import React, {Fragment} from 'react';
import AppComponentBase from '../../components/AppComponentBase';
import {inject, observer} from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import {ColumnProps} from 'antd/lib/table/interface';
import {L} from '../../lib/abpUtility';
import {Button, Card, Modal, Table, Col, Row, Divider} from 'antd';
/*import Filter from './components/filter';*/
import BaiVietStore from '../../stores/AppStore/BaiViet/BaiVietStore';
import {History, LocationState} from "history";
/*
import BaiVietCreatEdit from '../BaiVietChung/CreatEditBaiViet';
*/

const {confirm} = Modal;

interface IPayProps {
    baiVietStore: BaiVietStore
    history: History<LocationState>;
}

interface IState {
    isShow: boolean,
    id: any,
}

@inject(Stores.BaiVietStore)
@observer
class GiaovienCreen extends AppComponentBase<IPayProps, IState> {
    state = {
        isShow: true,
        id: 0,
    }

    componentDidMount(): void {
        this.getAllServerPaging();
    }

    async getAllServerPaging() {
        await this.props.baiVietStore.getAllServerPaging();
    }

    handleTableChange = async (pagination: any, filters: any, sorter: any) => {
        const {baiVietStore} = this.props!;
        const {filter} = baiVietStore;
        baiVietStore.currentPage = pagination.current;
        filter.maxResultCount = pagination.pageSize;
        filter.skipCount = (pagination.current - 1) * pagination.pageSize;
        await this.getAllServerPaging();
    };
    handleSearch = async () => {
        await this.getAllServerPaging();
    };

    async showEditModal(id?: any) {
        this.setState({
            isShow: false,
            id: id,
        })
    }

    async delete(id?: any) {
        const {baiVietStore} = this.props;
        const self = this;
        confirm({
            title: 'Do you want to delete these items?',
            okType: 'danger',
            onOk() {
                baiVietStore.delete(id).then(() => self.getAllServerPaging())
            },
            onCancel() {
            },
        })
    }


    render() {
        const {baiVietStore} = this.props;
        const {dataPaging, isLoading} = baiVietStore;
        const {isShow} = this.state;
        const columns: ColumnProps<any>[] = [
                {
                    title: '#',
                    key: 'STT',
                    width: 50,

                    render: (text: string, item: any, index: any) => (
                        <Fragment>
                            {index + 1 + baiVietStore.filter.skipCount}
                        </Fragment>
                    ),
                },
                {

                    title: 'Actions',
                    key: 'action',
                    width: 100,
                    render: (text: string, item: any) => (
                        <Fragment>
                            <Button type="primary" icon="edit" title={L('Edit')}
                                    onClick={() => this.showEditModal(item.id)}></Button>
                            <Button type="danger" icon="delete" title={L('Delete')}
                                    onClick={() => this.delete(item.id)}></Button>
                        </Fragment>
                    ),
                },
                {
                    title: 'Tóm Tắt',
                    dataIndex: 'tomTat',
                    key: 'tomTat',
                }
                ,
                {
                    title: 'Thứ tự bài',
                    dataIndex: 'ngayViet',
                    key: 'ngayViet',
                }
                ,
                {
                    title: 'Bộ Sưu tập',
                    dataIndex: 'boSuuTap',
                    key: 'boSuuTap',
                }
                ,
                {
                    title: 'Chủ đề',
                    dataIndex: 'chuDe',
                    key: 'chuDe',
                }
                ,
                {
                    title: 'Tác giả',
                    dataIndex: 'nguoiViet',
                    key: 'nguoiViet',
                }
            ]
        ;
        return (
            <>
                {isShow ?
                    <>
                        <Card style={{marginBottom: '10px'}}>
                            <Row type="flex" justify="space-between">
                                <Col>
                                    <h2 style={{color: '#b3ba21'}}>Quản lí Bài Viết
                                        <Divider type="vertical" style={{height: '50px'}}/>
                                        <Divider dashed style={{margin: '5px'}}/>
                                    </h2>
                                </Col>
                                <Col>
                                    <Button type="primary" icon="plus"
                                            onClick={() => this.showEditModal()}>{L('AddRecord')} </Button>
                                </Col>
                            </Row>
                            {/*<Filter storeData={baiVietStore}/>*/}
                        </Card>
                        <Card>
                            <Table
                                columns={columns}
                                size={'default'}
                                bordered={true}
                                rowKey={record => record.id}
                                dataSource={dataPaging == undefined ? [] : dataPaging.items}
                                pagination={{
                                    pageSize: baiVietStore.filter.maxResultCount,
                                    total: dataPaging === undefined ? 0 : dataPaging.totalCount,
                                    defaultCurrent: 1,
                                    pageSizeOptions: ['5', '10', '20', '50'],
                                    showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} records\``,
                                    showSizeChanger: true,
                                }}
                                loading={isLoading}
                                onChange={this.handleTableChange}
                            />
                        </Card>
                    </> : null
                }
                {/*{!isShow ?
                    <Card style={{marginBottom: '10px', width:'500px', margin:'auto'}}>
                        <BaiVietCreatEdit id={this.state.id}/>
                    </Card>
                    : null
                }*/}
            </>
        )
    }
}

export default GiaovienCreen;
