export enum BangCap {
    mot="Thủ Khoa Đại Học",
    hai="Cử Nhân Tài Năng",
    ba="Thạc Sĩ",
    bon="Cử Nhân Đại Học Sư Phạm",
    nam="Sinh Viên Tài Năng",
    sau="Sinh Viên Đại Học Sư Phạm",
    bay="Cử Nhân Đại Học",
    tam="Sinh Viên Đại Học",
    chin="Cao Đẳng Sư Phạm",
}
export enum CongViec {
    mot="Giáo Viên",
    hai="Gia Sư",
    ba="Sinh Viên"
}
export enum TrangThaiTime {
    mot="Vẫn Còn Lịch Dạy",
    hai="Kín Lịch Dạy",
    ba="Sắp Hết Lịch Dạy"
}
