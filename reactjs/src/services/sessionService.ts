class sessionService{
    public getValue(key:any,page?:any){
        if(page){
            if(page!=sessionStorage.getItem(key+'page')){return null;}
            else {return sessionStorage.getItem(key)}
        }
        return sessionStorage.getItem(key);
    }
    public setValue(key:any,value:any,page?:any){
        sessionStorage.setItem(key,value);
        if(page){sessionStorage.setItem(key+'page',page);}
    }
}
export default new sessionService();
