import http from '../httpService';
import localService from '../localService';

class CommonComboService {
  public async getMonHoc(): Promise<any[]> {
    const data= localService.getValue('monhoc');
    if(data!=null){return data;}
    const result = await http.get('api/services/app/ComboboxService/MonHoc');
    localService.setValue('monhoc',result.data.result);
    return result.data.result;
  }
  public async getTruong(): Promise<any[]> {
    const data= localService.getValue('truong');
    if(data!=null){return data;}
    const result = await http.get('api/services/app/ComboboxService/Truong');
    localService.setValue('truong',result.data.result);
    return result.data.result;
  }
  public async getLoaiLop(): Promise<any[]> {
    const data= localService.getValue('loailop');
    if(data!=null){return data;}
    const result = await http.get('api/services/app/ComboboxService/LoaiLop');
    localService.setValue('loailop',result.data.result);
    return result.data.result;
  }
  public async getStatus(loai?:any): Promise<any[]> {
    const data= localService.getValue('status'+loai);
    if(data!=null){return data;}
    const result = await http.get('api/services/app/ComboboxService/Status',{
      params:{loai:loai}
    });
    localService.setValue('status'+loai,result.data.result);
    return result.data.result;
  }
}

export default new CommonComboService();
