class localService{
    public getValue(key:any,page?:any){
        if(page){
            if(page!=localStorage.getItem(key+'page')) return null;
            else {
                const data=localStorage.getItem(key);
                if(data) return JSON.parse(data);
                else return null;
            }
        }
        const dataAll= localStorage.getItem(key);
        if(dataAll) return JSON.parse(dataAll);
        else return null;
    }
    public setValue(key:any,value:any,page?:any){
        localStorage.setItem(key,JSON.stringify(value));
        if(page){localStorage.setItem(key+'page',page);}
    }
}
export default new localService();
