import http from '../httpService';
import { PagedResultDto } from '../dto/pagedResultDto';
import {message} from "antd";


class LopLonService {
  public async getAllServerPaging(input: any): Promise<PagedResultDto<any>> {
    const res = await http.get('api/services/app/LopHoatDongLonService/GetPage', {params:input});
    return res.data.result;
  }
  public async getById(id: any) {
    let result = await http.get('api/services/app/LopHoatDongLonService/getLopYeuCau',{params: {id:id}});
    return result.data.result;
  }
  public async createUpdate(input: any) {
    delete input.toado;
     const result=await http.post('api/services/app/LopHoatDongLonService/CreatLopYeuCau', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async delete(id: any) {
    let result = await http.delete('api/services/app/LopHoatDongLonService/DeleteLopYeuCau/',{params: id});
    if(result.status==204){
      message.success('Delete success');
    }else {
      message.error('Delete error');
    };
  }
}

export default new LopLonService();
