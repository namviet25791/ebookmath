import http from '../httpService';
import { PagedResultDto } from '../dto/pagedResultDto';
import {message} from "antd";


class GiaoVienService {
  public async getAllServerPaging(input: any): Promise<PagedResultDto<any>> {
    const res = await http.get('api/services/app/GiaoVienService/GetPage', {params:input});
    return res.data.result;
  }
  public async getInfo(id: any) {
    let result = await http.get('api/services/app/GiaoVienService/GetThongTin/',{params: {id:id}});
    return result.data.result;
  }
  public async getBC(id: any) {
    let result = await http.get('api/services/app/GiaoVienService/GetBangCap',{params: {id:id}});
    return result.data.result;
  }
  public async getQK(id: any) {
    let result = await http.get('api/services/app/GiaoVienService/GetKiNang',{params: {id:id}});
    return result.data.result;
  }
  public async getKN(id: any) {
    let result = await http.get('api/services/app/GiaoVienService/GetQuaKhu',{params: {id:id}});
    return result.data.result;
  }
  public async getTT(id: any):Promise<any> {
    let result = await http.get('api/services/app/GiaoVienService/GetThanhTich',{params: {id:id}});
    return result.data.result;
  }

  public async createUpdateGV(input: any) {
     const result=await http.post('api/services/app/GiaoVienService/CreatUpdateGiaoVien', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async createListQK(input: any) {
    const result=await http.post('api/services/app/GiaoVienService/CreatListQuaKhu', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async updateQK(input: any) {
    const result=await http.post('api/services/app/GiaoVienService/UpdateQuaKhu', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async createListBC(input: any) {
    const result=await http.post('api/services/app/GiaoVienService/CreatListBangCap', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async updateBC(input: any) {
    const result=await http.post('api/services/app/GiaoVienService/UpdateBangCap', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }

  public async createListTT(input: any) {
    const result=await http.post('api/services/app/GiaoVienService/CreatListThanhTich', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async updateTT(input: any) {
    const result=await http.post('api/services/app/GiaoVienService/UpdateThanhTich', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async createListKN(input: any) {
    const result=await http.post('api/services/app/GiaoVienService/CreatListKiNang', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async updateKN(input: any) {
    const result=await http.post('api/services/app/GiaoVienService/UpdateKiNang', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }

  public async delete(id: any) {
    let result = await http.delete('api/services/app/GiaoVienService/Delete/',{params: id});
    if(result.status==204){
      message.success('Delete success');
    }else {
      message.error('Delete error');
    };
  }
  public async deleteTT(id: any) {
    let result = await http.delete('api/services/app/GiaoVienService/DeleteTT/',{params: {id:id}});
    if(result.status==204){
      message.success('Delete success');
    }else {
      message.error('Delete error');
    };
  }
  public async deleteQK(id: any) {
    let result = await http.delete('api/services/app/GiaoVienService/DeleteQK/',{params: {id:id}});
    if(result.status==204){
      message.success('Delete success');
    }else {
      message.error('Delete error');
    };
  }
  public async deleteBC(id: any) {
    let result = await http.delete('api/services/app/GiaoVienService/DeleteBC/',{params: {id:id}});
    if(result.status==204){
      message.success('Delete success');
    }else {
      message.error('Delete error');
    };
  }
}

export default new GiaoVienService();
