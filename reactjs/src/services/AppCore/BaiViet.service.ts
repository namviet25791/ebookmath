import http from '../httpService';
import { PagedResultDto } from '../dto/pagedResultDto';
import {message} from "antd";


class BaiVietService {
  public async getAllServerPaging(input: any): Promise<PagedResultDto<any>> {
    const res = await http.get('api/services/app/BaiVietService/GetPage', {params:input});
    return res.data.result;
  }
  public async getById(id: any) {
    let result = await http.get('api/services/app/BaiVietService/GetThongTin',{params: {id:id}});
    return result.data.result;
  }
  public async createUpdate(input: any) {
     const result=await http.post('api/services/app/BaiVietService/CreatUpdate', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
  public async delete(id: any) {
    let result = await http.delete('api/services/app/BaiVietService/Delete/',{params: id});
    if(result.status==204){
      message.success('Delete success');
    }else {
      message.error('Delete error');
    };
  }
  public async CreatUpdateBoSuuTap(input: any) {
    const result=await http.post('api/services/app/BaiVietService/CreatUpdateBoSuuTap', input);
    if(result.status==200){
      message.success('Creat Or Update success');
    }else {
      message.error('Creat Or Update error');
    };
  }
}

export default new BaiVietService();
