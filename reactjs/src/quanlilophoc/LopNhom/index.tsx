import React, {Fragment} from 'react';
import AppComponentBase from '../../components/AppComponentBase';
import {inject, observer} from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import {ColumnProps} from 'antd/lib/table/interface';
import {L} from '../../lib/abpUtility';
import {Button, Card, Modal, Table, Col, Row, Divider} from 'antd';
import Filter from './components/filter';
import LopNhomStore from '../../stores/AppStore/LopHoc/lopNhomStore';
import {History, LocationState} from "history";
import moment from 'moment';
import CreatOrUpdate from './components/creat-update';

const {confirm} = Modal;

interface IPayProps {
    lopNhomStore: LopNhomStore
    history: History<LocationState>;
}

@inject(Stores.LopNhomStore)
@observer
class LopNhom extends AppComponentBase<IPayProps> {
    componentDidMount(): void {
        this.getAllServerPaging();
    }

    async getAllServerPaging() {
        await this.props.lopNhomStore.getAllServerPaging();
    }

    handleTableChange = async (pagination: any, filters: any, sorter: any) => {
        const {lopNhomStore} = this.props!;
        const {filter} = lopNhomStore;
        lopNhomStore.currentPage = pagination.current;
        filter.maxResultCount = pagination.pageSize;
        filter.skipCount = (pagination.current - 1) * pagination.pageSize;
        await this.getAllServerPaging();
    };
    handleSearch = async () => {
        await this.getAllServerPaging();
    };

    async showEditModal(item?: any) {
        await this.props.lopNhomStore.modalCreateOrUpdate.showModal(item);
    }

    async delete(id?: any) {
        const {lopNhomStore} = this.props;
        const self = this;
        confirm({
            title: 'Do you want to delete these items?',
            okType: 'danger',
            onOk() {
                lopNhomStore.delete(id).then(() => self.getAllServerPaging())
            },
            onCancel() {
            },
        })
    }


    render() {
        const {lopNhomStore} = this.props;
        const {dataPaging, isLoading} = lopNhomStore;
        const columns: ColumnProps<any>[] = [
                {
                    title: '#',
                    key: 'STT',
                    width: 50,

                    render: (text: string, item: any, index: any) => (
                        <Fragment>
                            {index + 1 + lopNhomStore.filter.skipCount}
                        </Fragment>
                    ),
                },
                {

                    title: 'Actions',
                    key: 'action',
                    width: 100,
                    render: (text: string, item: any) => (
                        <Fragment>
                            <Button type="primary" icon="edit" title={L('Edit')}
                                    onClick={() => this.showEditModal(item)}></Button>
                            <Button type="primary" icon="edit" title={'Xem thông tin học sinh'}
                                    onClick={() => this.showEditModal(item.id)}></Button>
                            <Button type="primary" icon="edit" title={'Xem địa chỉ trên bản đồ'}
                                    onClick={() => this.showEditModal(item.id)}></Button>
                            <Button type="primary" icon="edit" title={'Xem địa chỉ trên bản đồ'}
                                    onClick={() => this.showEditModal(item.id)}></Button>
                            <Button type="danger" icon="delete" title={L('Delete')}
                                    onClick={() => this.delete(item.id)}></Button>
                        </Fragment>
                    ),
                },
                {
                    title: 'Tên',
                    dataIndex: 'tenNguoiNhom',
                    key: 'tenNguoiNhom',
                },{
                    title: 'Loại Lơp',
                    dataIndex: 'loaiLop',
                    key: 'loaiLop',
                },{
                    title: 'Môn',
                    dataIndex: 'monHoc',
                    key: 'monHoc',
                },{
                title: 'Thời gian yêu câu',
                dataIndex: 'thoiGian_Nhom',
                key: 'thoiGian_Nhom',
                render: (value: any, item: any) => {
                    let date = value ? moment.utc(value).format('DD/MM/YYYY') : null;
                    return <div> {date} </div>;
                    }
                },
                {
                    title: 'Địa Chỉ',
                    dataIndex: 'address',
                    key: 'address',
                },
                {
                    title: 'Trạng Thái',
                    dataIndex: 'trangThai',
                    key: 'trangThai',
                },
                {
                    title: 'Giới tính',
                    dataIndex: 'soLuong',
                    key: 'soLuong',
                },
                {
                    title: 'Giáo viên',
                    dataIndex: 'giaovien',
                    key: 'giaovien',
                },
            ]
        ;


        return (
            <>
                <Card style={{marginBottom: '10px'}}>
                    <Row type="flex" justify="space-between">
                        <Col>
                            <h2 style={{color: '#b3ba21'}}>Quản lí Giáo viên
                                <Divider type="vertical" style={{height: '50px'}}/>
                                <Divider dashed style={{margin: '5px'}}/>
                            </h2>
                        </Col>
                        <Col>
                            <Button type="primary" icon="plus"
                                    onClick={() => this.showEditModal()}>{L('AddRecord')} </Button>
                        </Col>
                    </Row>
                    <Filter storeData={lopNhomStore}/>
                </Card>
                <Card>
                    <Table
                        columns={columns}
                        size={'default'}
                        bordered={true}
                        rowKey={record => record.id}
                        dataSource={dataPaging == undefined ? [] : dataPaging.items}
                        pagination={{
                            pageSize: lopNhomStore.filter.maxResultCount,
                            total: dataPaging === undefined ? 0 : dataPaging.totalCount,
                            defaultCurrent: 1,
                            pageSizeOptions: ['5', '10', '20', '50'],
                            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} records\``,
                            showSizeChanger: true,
                        }}
                        loading={isLoading}
                        onChange={this.handleTableChange}
                    />
                    {lopNhomStore.modalCreateOrUpdate.isRender ?
                        <CreatOrUpdate storeData={lopNhomStore}
                                       refreshGrid={() => this.getAllServerPaging()}/>
                        : null}
                </Card>
            </>
        );
    }
}

export default LopNhom;
