import React, {Component} from 'react';

import {Button, Col, Form, Row, Icon, Input, InputNumber,DatePicker} from 'antd';
import {FormComponentProps} from 'antd/lib/form';
import LopYeuCauStore from '../../../stores/AppStore/LopHoc/lopYeuCauStore';
import StatusSelect from "../../../components/Combobox/Status";
import LoaiLopSelect from "../../../components/Combobox/LoaiLop";

export interface IFilterProps extends FormComponentProps {
    storeData: LopYeuCauStore,
}


class Filter extends Component<IFilterProps> {
    constructor(props: IFilterProps) {
        super(props);
    }

    state = {
        expand: false,
    };
    handleReset = () => {
        this.props.form.resetFields();
        this.handleSearch();
    };
    handleSearch = () => {
        this.props.form.validateFields(async (err, values) => {
            this.props.storeData.setFilterValue({...this.props.storeData.filter, ...values})
            await this.props.storeData.getAllServerPaging();
        });
    };
    toggle = () => {
        this.setState({expand: !this.state.expand});
    };

    render() {
        const {form} = this.props;
        const {getFieldDecorator} = form;

        const formItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 24},
            style: {marginBottom: 0},
        };
        const buttonItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 24},
        };

        return (
            <Form>
                <Form.Item>
                    <a style={{marginLeft: 8, fontSize: 12}} onClick={this.toggle}>
                        {'Tìm kiếm'} <Icon type={this.state.expand ? 'up' : 'down'}/>
                    </a>
                </Form.Item>
                {this.state.expand ? (
                    <Row type="flex" gutter={24} align="top" style={{marginBottom: 10}}>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label='Môn Học'>
                                {getFieldDecorator('monHocId')(
                                    <LoaiLopSelect />,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label='Status'>
                                {getFieldDecorator('statusId')(
                                    <StatusSelect loai={'LopYC'}/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label='Status'>
                                {getFieldDecorator('ngay')(
                                    <InputNumber />,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={24} lg={6}>
                            <Form.Item>
                                <Form.Item  {...formItemLayout} label={'Thời gian bắt đầu từ'} style={{display:'inline-block',width: 'calc(50% - 12px)'}}>
                                    {getFieldDecorator('batDau_Min')(
                                        <DatePicker/>)}
                                </Form.Item>
                                <Form.Item  {...formItemLayout} label={'Thời gian bắt đầu đến'} style={{display:'inline-block',width: 'calc(50% - 12px)'}}>
                                    {getFieldDecorator('batDau_Max')(
                                        <DatePicker/>)}
                                </Form.Item>
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label={'Filter Code'}>
                                {getFieldDecorator('filterCode')(
                                    <Input/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label={'Filter Địa Chỉ'}>
                                {getFieldDecorator('filterDiaChi')(
                                    <Input/>,
                                )}
                            </Form.Item>
                        </Col>

                        <Col xs={12} lg={5}>
                            <Form.Item {...buttonItemLayout} {...formItemLayout} label={'Tìm'}>
                                <Button onClick={this.handleSearch} type="primary" htmlType="submit">
                                    <Icon type="search"/>
                                    {'Search'}
                                </Button>
                                <Button onClick={this.handleReset} style={{marginLeft: 8}}>
                                    {'Clear'}
                                </Button>
                            </Form.Item>

                        </Col>
                    </Row>) : null}
            </Form>
        );
    }
}

export default Form.create<IFilterProps>()(Filter);
