import React from 'react';
import {Form, Card, Row, Col, Input, Select, Button, Icon, DatePicker} from 'antd';
import {observer} from 'mobx-react';
import {FormComponentProps} from 'antd/lib/form';
import * as _ from 'lodash';
import {RowProps} from 'antd/es/row';
import GiaoVienEditUpdateStore, {MenuType} from '../../../stores/AppStore/GiaoVien/GiaoVienEditUpdateStore';
import AppComponentBase from '../../../components/AppComponentBase';
import moment from 'moment';
import MonHocSelect from "../../../components/Combobox/MonHoc";
import TruongSelect from "../../../components/Combobox/Truong";
import {History, LocationState} from "history";
import UploadImg from "../../../components/Shared/uploadImg";
import {BangCap, CongViec} from '../../../chung/enum/giaovien';

const {TextArea} = Input;

export interface IProps extends FormComponentProps {
    storeData: GiaoVienEditUpdateStore,
    history: History<LocationState>;
}

export interface IState {
    formVals: any,
}


@observer
class Information extends AppComponentBase<IProps, IState> {
    constructor(props: IProps) {
        super(props);
    }

    componentDidMount() {
    };

    save = async (nextMenuType?: MenuType) => {
        const {form} = this.props;
        let res = 0;
        await form.validateFields(async (err: any, fieldsValue: any) => {
            if (err) {
                return;
            } else {
                // const formVals:FnbCategoryDto = { ...oldValue, ...fieldsValue };
                const {storeData} = this.props;
                await storeData.CreatUpdateData({...fieldsValue});
            }
        });
        return res;
    };

    render() {
        const {form} = this.props;
        const {infoData} = this.props.storeData;
        const {getFieldDecorator} = form;
        const formItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 24},
            style: {marginBottom: 0},
        };
        const rowLayout: RowProps = {
            gutter: [16, 0],
            type: 'flex',
        };
        return (
            <div>
                <Form>
                    <Row type='flex' gutter={16} align={'top'}>
                        <Card title='Thông tin cơ bản' size='small'>
                            <Row  {...rowLayout}>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label="Tên">
                                        {getFieldDecorator('ten', {initialValue: infoData.ten})(
                                            <Input/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label="Email">
                                        {getFieldDecorator('email', {initialValue: infoData.email})(
                                            <Input/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label="Địa Chỉ Làm Việc">
                                        {getFieldDecorator('diaChiLamViec', {initialValue: infoData.diaChiLamViec})(
                                            <Input/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label="Địa Chỉ Nhà">
                                        {getFieldDecorator('diaChiNha', {initialValue: infoData.diaChiNha})(
                                            <Input/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={7}>
                                    <Form.Item  {...formItemLayout} label="Điện thoại">
                                        {getFieldDecorator('dienThoai', {initialValue: infoData.dienThoai})(
                                            <Input/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={5}>
                                    <Form.Item  {...formItemLayout} label='Giới tính'>
                                        {getFieldDecorator('gioiTinh', {initialValue: infoData.gioiTinh})(
                                            <Select>
                                                <Select.Option key='1' value={1}>Nam</Select.Option>
                                                <Select.Option key='2' value={2}>Nữ</Select.Option>
                                            </Select>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={5}>
                                    <Form.Item  {...formItemLayout} label="Ngày sinh">
                                        {getFieldDecorator('ngaySinh', {initialValue: infoData.ngaySinh ? moment(infoData.ngaySinh, 'YYYY/MM/DD') : null})
                                        (<DatePicker/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={7}>
                                    <Form.Item  {...formItemLayout} label="Môn học">
                                        {getFieldDecorator('monHocId', {initialValue: infoData.monHocId})(
                                            <MonHocSelect/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={5}>
                                    <Form.Item  {...formItemLayout} label="Ảnh Bằng">
                                        {getFieldDecorator('photoBang', {
                                            initialValue: infoData.photoBang,
                                        })(<UploadImg/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={19}>
                                    <Row {...rowLayout}>
                                        <Col xs={24} lg={12}>
                                            <Form.Item  {...formItemLayout} label="Trường Đại học">
                                                {getFieldDecorator('schoolId', {initialValue: infoData.schoolId})(
                                                    <TruongSelect/>)}
                                            </Form.Item>
                                        </Col>
                                        <Col xs={24} lg={12}>
                                            <Form.Item  {...formItemLayout} label="Bằng Cấp">
                                                {getFieldDecorator('bangCapId', {
                                                    initialValue: infoData.bangCapId,
                                                })(
                                                    <Select>
                                                        <Select.Option key='1' value={1}>{BangCap.mot}</Select.Option>
                                                        <Select.Option key='2' value={2}>{BangCap.hai}</Select.Option>
                                                        <Select.Option key='3' value={3}>{BangCap.ba}</Select.Option>
                                                        <Select.Option key='4' value={4}>{BangCap.bon}</Select.Option>
                                                        <Select.Option key='5' value={5}>{BangCap.nam}</Select.Option>
                                                        <Select.Option key='6' value={6}>{BangCap.sau}</Select.Option>
                                                        <Select.Option key='7' value={7}>{BangCap.bay}</Select.Option>
                                                        <Select.Option key='8' value={8}>{BangCap.tam}</Select.Option>
                                                        <Select.Option key='9' value={9}>{BangCap.chin}</Select.Option>
                                                    </Select>)}
                                            </Form.Item>
                                        </Col>
                                        <Col xs={24} lg={12}>
                                            <Form.Item  {...formItemLayout} label="Công Việc Hiện Tại">
                                                {getFieldDecorator('congViecId', {initialValue: infoData.congViecId})(
                                                    <Select>
                                                        <Select.Option key='1' value={1}>{CongViec.mot}</Select.Option>
                                                        <Select.Option key='2' value={2}>{CongViec.hai}</Select.Option>
                                                        <Select.Option key='3' value={3}>{CongViec.ba}</Select.Option>
                                                    </Select>)}
                                            </Form.Item>
                                        </Col>
                                        <Col span={12}>
                                            <Form.Item  {...formItemLayout} label="Tính Cách">
                                                {getFieldDecorator('tinhCach', {initialValue: infoData.tinhCach})(
                                                    <TextArea autoSize={{minRows: 2, maxRows: 2}}/>)}
                                            </Form.Item>
                                        </Col>
                                        <Col span={24}>
                                            <Form.Item  {...formItemLayout} label="Mô tả chi tiết">
                                                {getFieldDecorator('mota', {initialValue: infoData.mota})(
                                                    <TextArea autoSize={{minRows: 2, maxRows: 3}}/>)}
                                            </Form.Item>
                                        </Col>
                                        <Col span={24}>
                                            <Form.Item  {...formItemLayout} label="Thành Tích Nổi Bật">
                                                {getFieldDecorator('thanhTichNoiBat', {initialValue: infoData.thanhTichNoiBat})(
                                                    <TextArea autoSize={{minRows: 2, maxRows: 3}}/>)}
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                    </Row>
                    <Row type="flex" justify="end" style={{paddingTop: '10px'}}>
                        <Button key="cancel" onClick={() => this.props.history.goBack()}
                                style={{marginRight: '8px'}}>
                            Hủy bỏ
                        </Button>
                        <Button key="save" type="primary" onClick={() => this.save()}>
                            <Icon type="save"/>
                            Lưu Lại
                        </Button>
                    </Row>
                </Form>
            </div>
        )
            ;
    }
}

export default Form.create<IProps>()(Information);
