import React from 'react';
import AppComponentBase from '../../../components/AppComponentBase';
import {Avatar, Row, Col, message, Button, Upload} from 'antd';
import {observer} from 'mobx-react';
import GiaoVienEditUpdateStore from '../../../stores/AppStore/GiaoVien/GiaoVienEditUpdateStore';

export interface ICreateOrUpdateModalProps {
    storeData: GiaoVienEditUpdateStore,
}

export interface Istate {
    img: any,
}

@observer
class AvatarUpdate extends AppComponentBase<ICreateOrUpdateModalProps, Istate> {

    constructor(props: ICreateOrUpdateModalProps) {
        super(props);
        this.state = {
            img: ''
        }
    }

    componentDidMount(): void {
        const {storeData} = this.props;
        const {infoData} = storeData;
        if(infoData.avatar!=null){
            this.setState({img: infoData.avatar});
        }
    }

    getBase64(img: any, callback: any) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }

    handleChange = (info: any) => {
        const isJpgOrPng = info.file.type === 'image/jpeg' || info.file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
            return;
        }
        const isLt2M = info.file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
            return;
        }

        this.getBase64(info.file, (imgData: any) => {
                this.setState({img: imgData});
            },
        );
    };
    changeAvatar = () => {
        const {img} = this.state;
        if (img != '') {
            const {storeData} = this.props;
            storeData.CreatUpdateAvatar({avatar:img ? img.split(';base64,')[1] : ''});
        }
    }

    render() {
        return (
            <>
                <Row style={{marginBottom:5}}>
                    <Avatar shape="square" size={64} icon="user" src={this.state.img}/>
                </Row>
                <Row gutter={16} align={'top'} type={'flex'}>
                    <Col span={6}>
                        <Upload showUploadList={false}
                                beforeUpload={() => false}
                                onChange={this.handleChange}>
                            <Button shape="circle" type="danger" icon="upload" size={'small'}></Button>
                        </Upload>
                    </Col>
                    <Col span={6}>
                        <Button shape="circle" type="primary" icon="check" onClick={this.changeAvatar} size={'small'}></Button>
                    </Col>
                </Row>
            </>
        );
    }
}

export default AvatarUpdate;
