import React from 'react';
import AppComponentBase from '../../components/AppComponentBase';
import {inject, observer} from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import {match} from 'react-router-dom';
import GiaoVienEditUpdateStoreStore, {MenuType} from '../../stores/AppStore/GiaoVien/GiaoVienEditUpdateStore';
import {Menu, Row, Col, PageHeader, Card, Icon} from 'antd';
import {History, LocationState} from "history";
import AvatarUpdate from './components/avatar';
import Information from './components/information';
import GiaoVien_ThanhTich from '../GiaoVien_Chung/GiaoVien_ThanhTich';
import GiaoVien_BangCap from '../GiaoVien_Chung/GiaoVien_BangCap';
import GiaoVien_QuaKhu from '../GiaoVien_Chung/GiaoVien_QuaKhu';


interface IProps {
    giaoVienEditUpdateStoreStore: GiaoVienEditUpdateStoreStore;
    match: match<any>;
    history: History<LocationState>;
}

interface IState {
    id: any;
}

@inject(Stores.GiaoVienEditUpdateStoreStore)
@observer
class GiaoVienCreatEdit extends AppComponentBase<IProps, IState> {
    state = {
        id: 0
    }

    componentDidMount(): void {
        if (this.props.match.params.id != 0) {
            const {giaoVienEditUpdateStoreStore} = this.props;
            giaoVienEditUpdateStoreStore.showModal(this.props.match.params.id);
            giaoVienEditUpdateStoreStore.menuType = MenuType.information;
            this.setState({id: this.props.match.params.id})
        }
    }

    renderChildren = () => {
        const {giaoVienEditUpdateStoreStore,history} = this.props;
        const {menuType} = giaoVienEditUpdateStoreStore;
        switch (menuType) {
            case 'information':return <Information storeData={giaoVienEditUpdateStoreStore} history={history}/>;break;
            case 'thanhtich':return <GiaoVien_ThanhTich giaoVienId={this.state.id} />;break;
            case 'bangcap':return <GiaoVien_BangCap giaoVienId={this.state.id}/>;break;
            case 'quakhu':return <GiaoVien_QuaKhu giaoVienId={this.state.id}/>;break;

        }
        return null;
    };
    selectKey = async (key: MenuType) => {
        const {giaoVienEditUpdateStoreStore} = this.props;
        giaoVienEditUpdateStoreStore.changeStatusSelectKey(key);
    };

    render() {
        const {giaoVienEditUpdateStoreStore, history} = this.props;
        const {menuType, infoData} = giaoVienEditUpdateStoreStore;
        const {id} = this.state;
        const renderMenu = () => {
            return [
                <Menu.Item key='information'>Thông tin cơ bản</Menu.Item>,
                <Menu.Item key='thanhtich'>Bảng Thành Tích</Menu.Item>,
                <Menu.Item key='bangcap'>Bằng Cấp</Menu.Item>,
                <Menu.Item key='quakhu'>Quá khứ làm việc</Menu.Item>,
            ];
        };
        return (
            <Row gutter={10} align={'top'} type={'flex'}>
                <Col span={24}>
                    <PageHeader
                        style={{paddingRight: 0, paddingLeft: 0, paddingTop: 0}}
                        onBack={() => history.goBack()}
                        title={id ? "Chỉnh Sửa" : "Tạo Mới"}
                    />
                </Col>
                <Col xs={24} lg={6}>
                    <Card className='Tab-Info'>
                        <Row gutter={10} align={'top'} type={'flex'}>
                            <Col xs={{span: 24}} lg={{span: 12}}>
                                <AvatarUpdate storeData={giaoVienEditUpdateStoreStore}/>
                            </Col>
                            <Col xs={{span: 24}} lg={{span: 12}}>
                                <span>{infoData.ten} </span>
                                <span>{infoData.gioiTinh === 1 ? <Icon style={{color: 'blue'}} type="man"/> :
                                    infoData.gioiTinh === 2 ? <Icon style={{color: 'pink'}} type="woman"/> : ''
                                } </span>
                            </Col>
                        </Row>
                        <Row align={'top'} type={'flex'} justify={'space-between'} className='Tab-Info-Sum'>
                            <Col>
                                <span style={{fontWeight: 'bold'}}>Số ĐT: </span>
                            </Col>
                            <Col>
                                <span style={{color: 'rgba(67,67,67,0.38)'}}>{infoData.dienThoai}</span>
                            </Col>
                        </Row>
                        <Row type={'flex'} justify={'space-between'} className='Tab-Info-Sum'>
                            <Col>
                                <span style={{fontWeight: 'bold'}}>Email: </span>
                            </Col>
                            <Col>
                                <span style={{color: 'rgba(67,67,67,0.38)'}}>{infoData.email}</span>
                            </Col>
                        </Row>
                    </Card>
                    <Card className='Tab-Menu'>
                        <Menu
                            mode="inline"
                            selectedKeys={[menuType ? menuType : '']}
                            onClick={({key}) => this.selectKey(key as MenuType)}

                        >
                            {renderMenu()}
                        </Menu>
                    </Card>
                </Col>
                <Col xs={24} lg={18}>
                    <Card className='Tab-Content'>
                        {this.renderChildren()}
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default GiaoVienCreatEdit;
