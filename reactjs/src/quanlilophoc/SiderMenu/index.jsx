import React from 'react';
import {Row, Col, Icon, Menu, Button, Popover, Dropdown, Badge, Avatar, Drawer} from 'antd';
import {quanlilophocRouter} from '../../components/Router/router.config';

import {enquireScreen} from 'enquire-js';
import {isGranted, L} from "../../lib/abpUtility";
import profilePicture from "../../chung/images/user.png";



const {SubMenu} = Menu;


class SiderMenu extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        menuVisible: false,
        typeIcon: 'menu-unfold',
        mobile:false,
    };

    componentDidMount() {
        enquireScreen((b) => {
            this.setState({mobile:true});
        });
    }

    renderSubMenus = (routes) =>
        routes.map(route => {
            if (route.permission && !isGranted(route.permission)) return null;
            if (route.children) {
                return (
                    <SubMenu key={JSON.stringify(route)}
                             title={<span>
                        <Icon type={route.icon}/>
                        <span>{L(route.title)}</span>
                      </span>}>
                        {this.renderSubMenus(route.children)}
                    </SubMenu>
                );
            }
            return (
                <Menu.Item key={route.path} onClick={() => this.props.history.push(route.path)}>
                    <Icon type={route.icon}/>
                    <span>{L(route.title)}</span>
                </Menu.Item>
            );
        });
    onMenuVisibleChange = (visible) => {
        this.setState({
            menuVisible: visible,
        });
    }
    handleShowMenu = () => {
        this.setState({
            menuVisible: true,
        });
    }
    render() {
        const {mobile, menuVisible} = this.state;
        const menu = (
            <Menu mode="inline" key="nav" theme={'dark'}>
                {this.renderSubMenus(quanlilophocRouter
                    .filter((item) => !item.isLayout && item.showInMenu))}
            </Menu>
        );
        if (mobile==false) {
            return (
                <>
                    {menu}
                </>
            )
        } else {
            return (
                <Popover
                    overlayClassName="popover-menu"
                    placement="bottomLeft"
                    content={menu}
                    trigger="click"
                    visible={menuVisible}
                    arrowPointAtCenter
                    onVisibleChange={this.onMenuVisibleChange}
                >
                    <Icon
                        className="nav-phone-icon"
                        type="menu"
                        onClick={this.handleShowMenu}
                    />
                </Popover>
            )
        }
    }
}

export default SiderMenu;
