import React from 'react';
import {Form, Modal, Row, Col, Input, InputNumber, DatePicker, Tabs, Select} from 'antd';
import {observer} from 'mobx-react';
import {FormComponentProps} from 'antd/lib/form';
import * as _ from 'lodash';
import LopYeuCauStore from '../../../stores/AppStore/LopHoc/lopYeuCauStore';
import {RowProps} from 'antd/es/row';
/*import rules from "./rule.validation";*/
import AppComponentBase from '../../../components/AppComponentBase';
import LoaiLopSelect from "../../../components/Combobox/LoaiLop";
import moment from 'moment';
import MonHocSelect from "../../../components/Combobox/MonHoc";
import StatusSelect from "../../../components/Combobox/Status";
import MapForm from '../../../components/Shared/mapForm';

interface IProps extends FormComponentProps {
    storeData: LopYeuCauStore,
    refreshGrid: () => void

}

interface IState {
    formVals: any,
}

@observer
class CreatOrUpdate extends AppComponentBase<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            formVals: props.storeData.modalCreateOrUpdate.dataModal,
        };
    }

    save = async () => {
        const {form} = this.props;
        await form.validateFields(async (err: any, fieldsValue: any) => {
            if (err) {
                return;
            } else {
                // const formVals:FnbCategoryDto = { ...oldValue, ...fieldsValue };
                const {modalCreateOrUpdate} = this.props.storeData;
                await modalCreateOrUpdate.updateData({...fieldsValue});
                await this.props.refreshGrid();
                modalCreateOrUpdate.closeModal();
            }
        });
    };

    render() {
        const {modalCreateOrUpdate} = this.props.storeData;
        const {formVals} = this.state;
        const {form} = this.props;
        const {getFieldDecorator} = form;
        const formItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 24},
            style: {marginBottom: 0},
        };
        const rowLayout: RowProps = {
            gutter: [10, 0],
            type: 'flex',
        };
        const {TabPane} = Tabs;
        const {TextArea} = Input;
        return (
            <Modal className={'Modal-Color'}
                   width={900}
                   title={modalCreateOrUpdate.dataModal.companyId ? 'Chỉnh Sửa Lớp' : 'Tạo Mới Lớp'}
                   visible={modalCreateOrUpdate.isShow}
                   onCancel={() => modalCreateOrUpdate.closeModal()}
                   onOk={() => this.save()}
            >

                <Form>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Thông tin lớp yêu cầu" key="1">
                            <Row  {...rowLayout}>
                                <Col xs={12} lg={6}>
                                    <Form.Item {...formItemLayout} label={'Mã Code'}>
                                        {getFieldDecorator('codeId', {initialValue: formVals.codeId})(
                                            <Input/>,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={6}>
                                    <Form.Item  {...formItemLayout} label={'Loại Lớp'}>
                                        {getFieldDecorator('loaiLopId', {
                                            initialValue: formVals.loaiLopId,
                                        })(
                                            <LoaiLopSelect/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={4}>
                                    <Form.Item  {...formItemLayout} label={'Lớp số'}>
                                        {getFieldDecorator('lopSo', {initialValue: formVals.lopSo})(
                                            <InputNumber width={'100%'}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={4}>
                                    <Form.Item  {...formItemLayout} label={'Môn học'}>
                                        {getFieldDecorator('monHocId', {initialValue: formVals.monHocId})(
                                            <MonHocSelect/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={4}>
                                    <Form.Item  {...formItemLayout} label="Ngày đăng kí">
                                        {getFieldDecorator('thoiGian_YeuCau', {initialValue: formVals.thoiGian_End ? moment(formVals.thoiGian_Start, 'YYYY/MM/DD') : null})
                                        (<DatePicker/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={6}>
                                    <Form.Item  {...formItemLayout} label={'Trạng Thái'}>
                                        {getFieldDecorator('statusId', {initialValue: formVals.statusId})(
                                            <StatusSelect loai={'LopYC'}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={6}>
                                    <Form.Item>
                                        <Form.Item  {...formItemLayout} label={'Phí Học Min'} style={{display:'inline-block',width: 'calc(50% - 12px)'}}>
                                            {getFieldDecorator('gia_Start', {initialValue: formVals.gia_Start})(
                                                <InputNumber/>)}
                                        </Form.Item>
                                        <Form.Item  {...formItemLayout} label={'Phí Học Max'} style={{display:'inline-block',width: 'calc(50% - 12px)'}}>
                                            {getFieldDecorator('gia_End', {initialValue: formVals.gia_End})(
                                                <InputNumber/>)}
                                        </Form.Item>
                                    </Form.Item>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tab="Thông tin người yêu cầu" key="2">
                            <Row  {...rowLayout}>
                                <Col xs={12} lg={8}>
                                    <Form.Item {...formItemLayout} label={'Tên Phụ Huynh'}>
                                        {getFieldDecorator('tenNguoiYeuCau', {initialValue: formVals.tenNguoiYeuCau})(
                                            <Input/>,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={8}>
                                    <Form.Item {...formItemLayout} label={'Địa Chỉ'}>
                                        {getFieldDecorator('address', {initialValue: formVals.address})(
                                            <Input/>,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={4}>
                                    <Form.Item {...formItemLayout} label={'Điện thoại'}>
                                        {getFieldDecorator('phone', {initialValue: formVals.phone})(
                                            <Input/>,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={4}>
                                    <Form.Item  {...formItemLayout} label='Giới tính'>
                                        {getFieldDecorator('gioiTinhNguoiYeuCau', {initialValue: formVals.gioiTinhNguoiYeuCau})(
                                            <Select>
                                                <Select.Option key='1' value={1}>Nam</Select.Option>
                                                <Select.Option key='2' value={2}>Nữ</Select.Option>
                                            </Select>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Số lượng trong lớp'>
                                        {getFieldDecorator('gioiTinhNguoiYeuCau', {initialValue: formVals.gioiTinhNguoiYeuCau})(
                                            <Select>
                                                <Select.Option key='1' value={1}>Nhóm nhỏ (3-5)</Select.Option>
                                                <Select.Option key='2' value={2}>Gia Sư (1-2)</Select.Option>
                                                <Select.Option key='3' value={3}>Lớp học tập
                                                    trung(10-15)</Select.Option>
                                            </Select>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Yêu Cầu Chung'>
                                        {getFieldDecorator('yeuCauChung', {initialValue: formVals.yeuCauChung})(
                                            <TextArea autoSize={{minRows: 2, maxRows: 3}}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Yêu Cầu Với Giáo Viên'>
                                        {getFieldDecorator('yeuCauGiaoVien', {initialValue: formVals.yeuCauGiaoVien})(
                                            <TextArea autoSize={{minRows: 2, maxRows: 3}}/>)}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tab="Địa chỉ người yêu cầu trên bản đồ" key="3">
                            <Form.Item  {...formItemLayout}>
                                {getFieldDecorator('toado', {initialValue: formVals.toado})(
                                    <MapForm />)}
                            </Form.Item>
                        </TabPane>
                        <TabPane tab="Thông tin học sinh" key="4">
                            <Row  {...rowLayout}>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Tên Học Sinh'>
                                        {getFieldDecorator('tenHocSinh', {initialValue: formVals.tenHocSinh})(
                                            <Input/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Học lực học sinh'>
                                        {getFieldDecorator('hocLucHocSinh', {initialValue: formVals.hocLucHocSinh})(
                                            <Input/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Trường'>
                                        {getFieldDecorator('truongHocSinh', {initialValue: formVals.truongHocSinh})(
                                            <Input/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={24}>
                                    <Form.Item  {...formItemLayout} label='Mô Tả Học Sinh'>
                                        {getFieldDecorator('moTaHocSinh', {initialValue: formVals.moTaHocSinh})(
                                            <TextArea autoSize={{minRows: 1, maxRows: 2}}/>)}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </TabPane>
                    </Tabs>
                </Form>
            </Modal>
        );
    }
}

export default Form.create<IProps>()(CreatOrUpdate);
