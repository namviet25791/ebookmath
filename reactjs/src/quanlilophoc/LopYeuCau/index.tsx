import React, {Fragment} from 'react';
import AppComponentBase from '../../components/AppComponentBase';
import {inject, observer} from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import {ColumnProps} from 'antd/lib/table/interface';
import {L} from '../../lib/abpUtility';
import {Button, Card, Modal, Table, Col, Row, Divider} from 'antd';
import Filter from './components/filter';
import LopYeuCauStore from '../../stores/AppStore/LopHoc/lopYeuCauStore';
import {History, LocationState} from "history";
import moment from 'moment';
import CreatOrUpdate from './components/creat-update';

const {confirm} = Modal;

interface IPayProps {
    lopYeuCauStore: LopYeuCauStore
    history: History<LocationState>;
}

@inject(Stores.LopYeuCauStore)
@observer
class LopYeuCau extends AppComponentBase<IPayProps> {
    componentDidMount(): void {
        this.getAllServerPaging();
    }

    async getAllServerPaging() {
        await this.props.lopYeuCauStore.getAllServerPaging();
    }

    handleTableChange = async (pagination: any, filters: any, sorter: any) => {
        const {lopYeuCauStore} = this.props!;
        const {filter} = lopYeuCauStore;
        lopYeuCauStore.currentPage = pagination.current;
        filter.maxResultCount = pagination.pageSize;
        filter.skipCount = (pagination.current - 1) * pagination.pageSize;
        await this.getAllServerPaging();
    };
    handleSearch = async () => {
        await this.getAllServerPaging();
    };

    async showEditModal(item?: any) {
        await this.props.lopYeuCauStore.modalCreateOrUpdate.showModal(item);
    }

    async delete(id?: any) {
        const {lopYeuCauStore} = this.props;
        const self = this;
        confirm({
            title: 'Do you want to delete these items?',
            okType: 'danger',
            onOk() {
                lopYeuCauStore.delete(id).then(() => self.getAllServerPaging())
            },
            onCancel() {
            },
        })
    }


    render() {
        const {lopYeuCauStore} = this.props;
        const {dataPaging, isLoading} = lopYeuCauStore;
        const columns: ColumnProps<any>[] = [
                {
                    title: '#',
                    key: 'STT',
                    width: 50,

                    render: (text: string, item: any, index: any) => (
                        <Fragment>
                            {index + 1 + lopYeuCauStore.filter.skipCount}
                        </Fragment>
                    ),
                },
                {

                    title: 'Actions',
                    key: 'action',
                    width: 100,
                    render: (text: string, item: any) => (
                        <Fragment>
                            <Button type="primary" icon="edit" title={'Xem địa chỉ trên bản đồ'}
                                    onClick={() => this.showEditModal(item.id)}></Button>
                            <Button type="danger" icon="delete" title={L('Delete')}
                                    onClick={() => this.delete(item.id)}></Button>
                        </Fragment>
                    ),
                },
                {
                    title: 'Tên',
                    dataIndex: 'tenNguoiYeuCau',
                    key: 'tenNguoiYeuCau',
                },{
                    title: 'Loại Lơp',
                    dataIndex: 'loaiLop',
                    key: 'loaiLop',
                },{
                    title: 'Môn',
                    dataIndex: 'monHoc',
                    key: 'monHoc',
                },{
                title: 'Thời gian yêu câu',
                dataIndex: 'thoiGian_YeuCau',
                key: 'thoiGian_YeuCau',
                render: (value: any, item: any) => {
                    let date = value ? moment.utc(value).format('DD/MM/YYYY') : null;
                    return <div> {date} </div>;
                    }
                },
                {
                    title: 'Địa Chỉ',
                    dataIndex: 'address',
                    key: 'address',
                },
                {
                    title: 'Phone',
                    dataIndex: 'phone',
                    key: 'phone',
                },
                {
                    title: 'Giới tính',
                    dataIndex: 'gioiTinhNguoiYeuCau',
                    key: 'gioiTinhNguoiYeuCau',
                    render:
                        (value: any, item: any) => {
                            if (value == '0') return <div>Giới tính ba</div>;
                            else if (value == '1') return <div>Nam</div>;
                            else return <div>Nữ</div>;
                        }
                },
                {
                    title: 'Tính cách người yêu cầu',
                    dataIndex: 'danhGiaNguoiYeuCau',
                    key: 'danhGiaNguoiYeuCau',
                },
            ]
        ;


        return (
            <>
                <Card style={{marginBottom: '10px'}}>
                    <Row type="flex" justify="space-between">
                        <Col>
                            <h2 style={{color: '#b3ba21'}}>Quản lí Giáo viên
                                <Divider type="vertical" style={{height: '50px'}}/>
                                <Divider dashed style={{margin: '5px'}}/>
                            </h2>
                        </Col>
                        <Col>
                            <Button type="primary" icon="plus"
                                    onClick={() => this.showEditModal()}>{L('AddRecord')} </Button>
                        </Col>
                    </Row>
                    <Filter storeData={lopYeuCauStore}/>
                </Card>
                <Card>
                    <Table
                        columns={columns}
                        size={'default'}
                        bordered={true}
                        rowKey={record => record.id}
                        dataSource={dataPaging == undefined ? [] : dataPaging.items}
                        pagination={{
                            pageSize: lopYeuCauStore.filter.maxResultCount,
                            total: dataPaging === undefined ? 0 : dataPaging.totalCount,
                            defaultCurrent: 1,
                            pageSizeOptions: ['5', '10', '20', '50'],
                            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} records\``,
                            showSizeChanger: true,
                        }}
                        loading={isLoading}
                        onChange={this.handleTableChange}
                    />
                    {lopYeuCauStore.modalCreateOrUpdate.isRender ?
                        <CreatOrUpdate storeData={lopYeuCauStore}
                                       refreshGrid={() => this.getAllServerPaging()}/>
                        : null}
                </Card>
            </>
        );
    }
}

export default LopYeuCau;
