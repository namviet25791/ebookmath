import React from 'react';
import AppComponentBase from '../../components/AppComponentBase';
import {inject, observer} from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import {L} from '../../lib/abpUtility';
import {Button, Card, Modal, Col, Row, Divider, List, Icon, Avatar, Tag} from 'antd';
import Filter from './components/filter';
import GiaoVienStore from '../../stores/AppStore/GiaoVien/GiaoVienStore';
import {History, LocationState} from "history";
import {BangCap} from '../../chung/enum/giaovien';

const {confirm} = Modal;

interface IPayProps {
    giaoVienStore: GiaoVienStore
    history: History<LocationState>;
}

@inject(Stores.GiaoVienStore)
@observer
class GiaoVienList extends AppComponentBase<IPayProps> {
    componentDidMount(): void {
        this.getAllServerPaging();
    }

    async getAllServerPaging() {
        await this.props.giaoVienStore.getAllServerPaging();
    }

    handleTableChange = async (page: any, pageSize: any) => {
        const {giaoVienStore} = this.props!;
        const {filter} = giaoVienStore;
        giaoVienStore.currentPage = page;
        filter.maxResultCount = pageSize;
        filter.skipCount = (page - 1) * pageSize;
        await this.getAllServerPaging();
    };
    handleSearch = async () => {
        await this.getAllServerPaging();
    };

    async showEditModal(id: any) {
        this.props.history.push(`/quanlilophoc/giaoviencreatedit/${id}`)
    }

    async delete(id?: any) {
        const {giaoVienStore} = this.props;
        const self = this;
        confirm({
            title: 'Do you want to delete these items?',
            okType: 'danger',
            onOk() {
                giaoVienStore.delete(id).then(() => self.getAllServerPaging())
            },
            onCancel() {
            },
        })
    }

    title = (item: any) => {
        return (
            <Row>
                <span>{item.ten} </span>
                <span>{item.gioiTinh === 1 ? <Icon style={{color: 'blue'}} type="man"/> :
                    item.gioiTinh === 2 ? <Icon style={{color: 'pink'}} type="woman"/> : ''
                }
                </span>
                <span> (Tuổi: {item.tuoi})</span>
                <Tag color="#87d068">Môn: {item.monHoc} </Tag>
            </Row>
        )
    }
    mota = (item: any) => {
        let bangCap = '';
        switch (item.bangCapId) {
            case 1:bangCap = BangCap.mot;break;
            case 2:bangCap = BangCap.hai;break;
            case 3:bangCap = BangCap.ba;break;
            case 4:bangCap = BangCap.bon;break;
            case 5:bangCap = BangCap.nam;break;
            case 6:bangCap = BangCap.sau;break;
            case 7:bangCap = BangCap.bay;break;
            case 8:bangCap = BangCap.tam;break;
            case 9:bangCap = BangCap.chin;break;
        }
        return (
            <Row>
                <span><Tag color="lime">Trường </Tag>{item.truong}  </span>
                <Tag color="green">{`Bằng Cấp: ${bangCap}`} </Tag>
            </Row>
        )
    }
    content = (item: any) => {
        return (
            <>
                <Row>
                    <span><Tag color="#1890ff">Thành Tích Nổi Bật</Tag> {item.thanhTichNoiBat}.</span>
                </Row>
            </>
        )
    }
    render() {
        const {giaoVienStore} = this.props;
        const {dataPaging, isLoading} = giaoVienStore;
        return (
            <>
                <Card style={{marginBottom: '10px'}}>
                    <Row type="flex" justify="space-between">
                        <Col>
                            <h2 style={{color: '#b3ba21'}}>Danh Sách Giáo Viên
                                <Divider type="vertical" style={{height: '50px'}}/>
                                <Divider dashed style={{margin: '5px'}}/>
                            </h2>
                        </Col>
                        <Col>
                            <Button type="primary" icon="plus"
                                    onClick={() => this.showEditModal(0)}>{L('AddRecord')} </Button>
                        </Col>
                    </Row>
                    <Filter storeData={giaoVienStore}/>
                </Card>
                <Card>
                    <List
                        itemLayout="vertical"
                        size="large"
                        dataSource={dataPaging == undefined ? [] : dataPaging.items}
                        pagination={{
                            pageSize: giaoVienStore.filter.maxResultCount,
                            total: dataPaging === undefined ? 0 : dataPaging.totalCount,
                            defaultCurrent: 1,
                            pageSizeOptions: ['12', '24', '36'],
                            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} records\``,
                            showSizeChanger: true,
                            onChange: (page: any, pageSize: any) => this.handleTableChange(page, pageSize)
                        }}
                        loading={isLoading}
                        renderItem={item => (
                            <List.Item
                                key={item.id}
                                actions={[
                                    <Button size={'small'} type="primary" icon="edit" title={L('Edit')} onClick={() => this.showEditModal(item.id)}></Button>,
                                ]}
                                extra={
                                    <img
                                        width={272}
                                        alt="logo"
                                        src={item.photoBang?`data:image/jpeg;base64${item.photoBang}`:"https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png" }
                                    />
                                }
                            >
                                <List.Item.Meta
                                    avatar={<Avatar shape="square" size={'large'} src={`data:image/jpeg;base64,${item.avatar}`}/>}
                                    title={this.title(item)}
                                    description={this.mota(item)}
                                />
                                {this.content(item)}
                            </List.Item>
                        )}
                    />
                </Card>
            </>
        );
    }
}

export default GiaoVienList;
