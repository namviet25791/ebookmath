import React, {Component} from 'react';

import {Button, Col, Form, Row, Icon, Select, Input, InputNumber} from 'antd';
import {FormComponentProps} from 'antd/lib/form';
import GiaoVienStore from '../../../stores/AppStore/GiaoVien/GiaoVienStore';
import MonHocSelect from "../../../components/Combobox/MonHoc";
import StatusSelect from "../../../components/Combobox/Status";

export interface IFilterProps extends FormComponentProps {
    storeData: GiaoVienStore,
}


class Filter extends Component<IFilterProps> {
    constructor(props: IFilterProps) {
        super(props);
    }

    state = {
        expand: false,
    };
    handleReset = () => {
        this.props.form.resetFields();
        this.handleSearch();
    };
    handleSearch = () => {
        this.props.form.validateFields(async (err, values) => {
            this.props.storeData.setFilterValue({...this.props.storeData.filter, ...values})
            await this.props.storeData.getAllServerPaging();
        });
    };
    toggle = () => {
        this.setState({expand: !this.state.expand});
    };

    render() {
        const {form} = this.props;
        const {getFieldDecorator} = form;

        const formItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 24},
            style: {marginBottom: 0},
        };
        const buttonItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 24},
        };

        return (
            <Form>
                <Form.Item>
                    <a style={{marginLeft: 8, fontSize: 12}} onClick={this.toggle}>
                        {'Tìm kiếm'} <Icon type={this.state.expand ? 'up' : 'down'}/>
                    </a>
                </Form.Item>
                {this.state.expand ? (
                    <Row type="flex" gutter={24} align="top" style={{marginBottom: 10}}>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label='Môn Học'>
                                {getFieldDecorator('monHocId')(
                                    <MonHocSelect/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label='Status'>
                                {getFieldDecorator('statusId')(
                                    <StatusSelect loai={'GiaoVien'}/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label='Giới Tính'>
                                {getFieldDecorator('gioiTinh')(
                                    <Select>
                                        <Select.Option key='0' value={''}>{'Tất cả'}</Select.Option>
                                        <Select.Option key='1' value={1}>{'Nam'}</Select.Option>
                                        <Select.Option key='2' value={0}>{'Nữ'}</Select.Option>
                                    </Select>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label='Bằng Cấp'>
                                {getFieldDecorator('bangCapId')(
                                    <Select>
                                        <Select.Option key='0' value={''}>{'Tất cả'}</Select.Option>
                                        <Select.Option key='1' value={1}>{'Đại học'}</Select.Option>
                                        <Select.Option key='2' value={2}>{'Thạc sĩ'}</Select.Option>
                                        <Select.Option key='3' value={3}>{'Đại Học Tài Năng'}</Select.Option>
                                        <Select.Option key='4' value={4}>{'Cao Đẳng'}</Select.Option>
                                    </Select>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label='Tuổi'>
                                <Form.Item style={{display: 'inline-block', width: 'calc(50% - 12px)'}}>
                                    {getFieldDecorator('tuoi_Start')(
                                        <InputNumber/>
                                    )}
                                </Form.Item>
                                <span style={{display: 'inline-block', width: '24px', textAlign: 'center'}}>-</span>
                                <Form.Item style={{display: 'inline-block', width: 'calc(50% - 12px)'}}>
                                    {getFieldDecorator('tuoi_End')(
                                        <InputNumber/>
                                    )}
                                </Form.Item>
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...formItemLayout} label={'Tên'}>
                                {getFieldDecorator('ten')(
                                    <Input/>,
                                )}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={5}>
                            <Form.Item {...buttonItemLayout} {...formItemLayout} label={'Tìm'}>
                                <Button onClick={this.handleSearch} type="primary" htmlType="submit">
                                    <Icon type="search"/>
                                    {'Search'}
                                </Button>
                                <Button onClick={this.handleReset} style={{marginLeft: 8}}>
                                    {'Clear'}
                                </Button>
                            </Form.Item>

                        </Col>
                    </Row>) : null}
            </Form>
        );
    }
}

export default Form.create<IFilterProps>()(Filter);
