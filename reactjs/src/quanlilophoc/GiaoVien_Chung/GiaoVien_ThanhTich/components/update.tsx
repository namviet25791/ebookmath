import React from 'react';
import {Form, Modal, Row, Col, Input, /*DatePicker,*/ InputNumber, DatePicker} from 'antd';
import {observer} from 'mobx-react';
import {FormComponentProps} from 'antd/lib/form';
import * as _ from 'lodash';
import AppComponentBase from '../../../../components/AppComponentBase';
import {RowProps} from 'antd/es/row';
/*import moment from "moment";*/
import moment from 'moment';
import UploadImg from '../../../../components/Shared/uploadImg';
import GiaoVienThanhTich from "../../../../stores/AppStore/GiaoVien/GiaoVienThanhTichStore";

const {TextArea} = Input;

interface IProps extends FormComponentProps {
    storeData: GiaoVienThanhTich,
    refreshGrid: () => void

}

interface IState {
    formVals: any,
}

@observer
class UpdateCreat extends AppComponentBase<IProps, IState> {
    constructor(props: IProps) {
        super(props);
    }

    async componentDidMount() {
    }

    save = async () => {
        const {form} = this.props;
        await form.validateFields(async (err: any, fieldsValue: any) => {
            if (err) {
                return;
            } else {
                const {storeData} = this.props;
                await storeData.CreatUpdateData({...fieldsValue});
                await storeData.getAllServerPaging();
            }
        });
    };

    render() {
        const {storeData} = this.props;
        const {DataItem} = storeData;
        const {form} = this.props;
        const {getFieldDecorator} = form;
        const formItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 24},
            style: {marginBottom: 0},
        };
        const rowLayout: RowProps = {
            gutter: [16, 0],
            type: 'flex',
        };
        return (
            <Modal className={'Modal-Color'}
                   width={800}
                   title={DataItem.id ? 'Chỉnh sửa' : 'Tạo mới'}
                   visible={storeData.isShow}
                   onCancel={() => storeData.closeModal()}
                   onOk={() => this.save()}
            >
                <Form>
                    <Row  {...rowLayout}>
                        <Col xs={24} lg={24}>
                            <Form.Item  {...formItemLayout} label='Trường học'>
                                {getFieldDecorator('truongHoc', {initialValue: DataItem.truongHoc})(
                                    <Input/>)}
                            </Form.Item>
                        </Col>
                        <Col xs={12} lg={6}>
                            <Form.Item  {...formItemLayout} label='Ảnh Thành Tích'>
                                {getFieldDecorator('photo', {initialValue: DataItem.photo})(
                                    <UploadImg/>)}
                            </Form.Item>
                        </Col>
                        <Col xs={24} lg={18}>
                            <Row {...rowLayout}>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label="Lớp">
                                        {getFieldDecorator('lop', {initialValue: DataItem.lop})(
                                            <InputNumber style={{width: '100%'}}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Loại lớp'>
                                        {getFieldDecorator('loaiLopId', {initialValue: DataItem.loaiLopId})(
                                            <InputNumber style={{width: '100%'}}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label="Ngày bắt đầu">
                                        {getFieldDecorator('thoiGian_Start', {initialValue: DataItem.thoiGian_Start ? moment(DataItem.thoiGian_Start, 'YYYY/MM/DD') : null})
                                        (<DatePicker/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label="Ngày kết thúc">
                                        {getFieldDecorator('thoiGian_End', {initialValue: DataItem.thoiGian_Start ? moment(DataItem.thoiGian_End, 'YYYY/MM/DD') : null})
                                        (<DatePicker/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Thành Tích'>
                                        {getFieldDecorator('storeData', {initialValue: DataItem.storeData})(
                                            <TextArea autoSize={{minRows: 2, maxRows: 5}}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Mô tả'>
                                        {getFieldDecorator('moTa', {initialValue: DataItem.moTa})(
                                            <TextArea autoSize={{minRows: 2, maxRows: 5}}/>)}
                                    </Form.Item>
                                </Col>

                            </Row>
                        </Col>

                    </Row>
                </Form>

            </Modal>
        );
    }
}

export default Form.create<IProps>()(UpdateCreat);
