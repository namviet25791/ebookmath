import React  from 'react';
import {Form, Modal, Row, Col, Input, DatePicker, Select} from 'antd';
import { observer } from 'mobx-react';
import { FormComponentProps } from 'antd/lib/form';
import * as _ from 'lodash';
import AppComponentBase from '../../../../components/AppComponentBase';
import { RowProps } from 'antd/es/row';
/*import moment from "moment";*/
import moment from 'moment';
import UploadImg from '../../../../components/Shared/uploadImg';
import TruongSelect from "../../../../components/Combobox/Truong";
import GiaoVienBangCap from "../../../../stores/AppStore/GiaoVien/GiaoVienBangCapStore";

const { TextArea } = Input;
interface IProps extends FormComponentProps {
    storeData: GiaoVienBangCap,
    refreshGrid: () => void

}

interface IState {
    formVals: any,
}

@observer
class UpdateCreat extends AppComponentBase<IProps, IState> {
    constructor(props: IProps) {
        super(props);
    }

    async componentDidMount() {
    }

    save = async () => {
        const { form } = this.props;
        await form.validateFields(async (err: any, fieldsValue: any) => {
            if (err) {
                return;
            } else {
                const { storeData } = this.props;
                await storeData.CreatUpdateData({ ...fieldsValue });
                await storeData.getAllServerPaging();
            }
        });
    };

    render() {
        const { storeData } = this.props;
        const { DataItem } = storeData;
        const { form } = this.props;
        const { getFieldDecorator } = form;
        const formItemLayout = {
            labelCol: { span: 24 },
            wrapperCol: { span: 24 },
            style: { marginBottom: 0 },
        };
        const rowLayout: RowProps = {
            gutter: [16, 0],
            type: 'flex',
        };
        return (
            <Modal className={'Modal-Color'}
                width={800}
                title={DataItem.id?'Chỉnh sửa':'Tạo mới'}
                visible={storeData.isShow}
                onCancel={() => storeData.closeModal()}
                onOk={() => this.save()}
            >
                <Form>
                    <Row  {...rowLayout}>
                        <Col xs={24} lg={6}>
                            <Form.Item  {...formItemLayout} label='Ảnh'>
                                {getFieldDecorator('photo', {initialValue:DataItem.photo})(
                                    <UploadImg />)}
                            </Form.Item>
                        </Col>
                        <Col xs={24} lg={18}>
                            <Row {...rowLayout}>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label='Trường học'>
                                        {getFieldDecorator('truongHocId', {initialValue:DataItem.truongHocId})(
                                            <TruongSelect />)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                    <Form.Item  {...formItemLayout} label="Bằng Cấp">
                                        {getFieldDecorator('bangCapId', {initialValue:DataItem.bangCapId})(
                                            <Select>
                                                <Select.Option key='1' value={1}>Đại Học</Select.Option>
                                                <Select.Option key='2' value={2}>Thạc sĩ</Select.Option>
                                                <Select.Option key='3' value={3}>Đại Học Tài Năng</Select.Option>
                                                <Select.Option key='4' value={4}>Cao Đẳng</Select.Option>
                                            </Select>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                <Form.Item  {...formItemLayout} label="Ngày bắt đầu">
                                    {getFieldDecorator('thoiGian_Start', {initialValue: DataItem.thoiGian_Start ? moment(DataItem.thoiGian_Start, 'YYYY/MM/DD') : null})
                                    (<DatePicker/>)}
                                </Form.Item>
                                </Col>
                                <Col xs={24} lg={12}>
                                <Form.Item  {...formItemLayout} label="Ngày kết thúc">
                                    {getFieldDecorator('thoiGian_End', {initialValue: DataItem.thoiGian_End ? moment(DataItem.thoiGian_Start, 'YYYY/MM/DD') : null})
                                    (<DatePicker/>)}
                                </Form.Item>
                                </Col>
                                <Col xs={24} lg={24}>
                                    <Form.Item  {...formItemLayout} label='Mô tả'>
                                        {getFieldDecorator('moTa', {initialValue:DataItem.moTa})(
                                            <TextArea autoSize={{minRows: 2, maxRows: 3}}/>)}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>

                    </Row>
                </Form>

            </Modal>
        );
    }
}
export default Form.create<IProps>()(UpdateCreat);
