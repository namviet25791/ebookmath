import React, {Fragment} from 'react';
import {Button, Col, Modal, Row, Table, Card} from 'antd';
import {ColumnProps} from 'antd/lib/table/interface';
import AppComponentBase from "../../../components/AppComponentBase";
import moment from 'moment';
import GiaoVienBangCap from '../../../stores/AppStore/GiaoVien/GiaoVienBangCapStore';
import {inject, observer} from "mobx-react";
import Stores from "../../../stores/storeIdentifier";
import UpdateCreat from "./components/update";

const {confirm} = Modal;

interface IProps {
    giaoVienBangCap?: GiaoVienBangCap,
    giaoVienId?: any,
}
@inject(Stores.GiaoVienBangCap)
@observer
class GiaoVien_BangCap extends AppComponentBase<IProps> {
    componentDidMount(): void {
        this.getAllServerPaging();
    }

    async getAllServerPaging() {
        const {giaoVienBangCap, giaoVienId} = this.props;
        giaoVienBangCap!.giaoVienId=giaoVienId;
        giaoVienBangCap!.getAllServerPaging();
    }
    async showEditModal(item?: any) {
        const {giaoVienBangCap} = this.props;
        await giaoVienBangCap!.showModal(item);
    }

    async delete(item: any) {
        const {giaoVienBangCap} = this.props;
        const self = this;
        confirm({
            title: 'Do you want to delete these items?',
            okType: 'danger',
            onOk() {
                if (item.companyId) {
                    giaoVienBangCap!.delete(item.id).then(() => self.getAllServerPaging());
                }
            },
            onCancel() {
            },
        });
    }

    render() {
        const {giaoVienBangCap} = this.props;
        const {Data,truonghocs} = giaoVienBangCap!;
        const columns: ColumnProps<any>[] = [
            {
                title: '#',
                key: 'STT',
                width: 50,
                render: (text: string, item: any, index: any) => (
                    <Fragment>
                        {index + 1}
                    </Fragment>
                ),
            },
            {

                title: 'Hành Động',
                key: 'action',
                width: 100,
                render: (text: string, item: any) => (
                    <Fragment>
                        <Button type="primary" icon="edit" title='EditUpdate'
                                onClick={() => this.showEditModal(item)}></Button>
                        <Button type="danger" icon="delete" title='Delete'
                                onClick={() => this.delete(item)}></Button>
                    </Fragment>
                ),
            },
            {
                title: 'Trường học',
                dataIndex: 'truongHocId',
                key: 'truongHocId',
                render: (value: any, item: any) => (
                    truonghocs.filter((x:any)=>x.id==value).displayText
                ),
            },
            {
                title: 'Thời gian từ',
                dataIndex: 'thoiGian_Start',
                key: 'thoiGian_Start',
                render: (value: any, item: any) => {
                    let date =value?moment.utc(value).format('DD/MM/YYYY'):null;
                    return <div> {date} </div>;
                },
            },
            {
                title: 'Thời gian đến',
                dataIndex: 'thoiGian_End',
                key: 'thoiGian_End',
                render: (value: any, item: any) => {
                    let date =value?moment.utc(value).format('DD/MM/YYYY'):null;
                    return <div> {date} </div>;
                },

            },
            {
                title: 'Lớp',
                dataIndex: 'lopSo',
                key: 'lopSo',
            },
            {
                title: 'Năm',
                dataIndex: 'year',
                key: 'year',
            },
            {
                title: 'Loại Bằng',
                dataIndex: 'bangCapId',
                key: 'bangCapId',
                render: (value: any, item: any) => {
                    switch (value) {
                        case 1:return <div> "Đại Học"</div>;break;
                        case 2:return <div> "Thạc Sĩ"</div>;break;
                        case 3:return <div> "Đại Học Chất Lượng Cao"</div>;break;
                        case 4:return <div> "Cao Đẳng"</div>;break;
                        default :return <div></div>;break
                    }
                },
            },
            {
                title: 'Mô Tả',
                dataIndex: 'moTa',
                key: 'moTa',
            },
        ];
        return (
            <Card title={'Anniversary'} size='small'>
                <Row type="flex" justify="end">
                    <Col>
                        <Button type="primary" icon="plus"
                                onClick={() => giaoVienBangCap!.showModal()}>{'addRecord'} </Button>
                    </Col>
                </Row>
                <Table
                    scroll={{x: true}}
                    columns={columns}
                    size={'default'}
                    bordered={true}
                    rowKey={record => record.id}
                    dataSource={Data == undefined ? [] : Data}
                    loading={giaoVienBangCap!.isLoading}
                />
                {giaoVienBangCap!.isRender ?
                    <UpdateCreat storeData={giaoVienBangCap!}
                                   refreshGrid={() => this.getAllServerPaging()}/>
                    : null}
            </Card>

        );
    }
}

export default GiaoVien_BangCap;
