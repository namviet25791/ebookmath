import React, {Fragment} from 'react';
import {Button, Col, Modal, Row, Table, Card} from 'antd';
import {ColumnProps} from 'antd/lib/table/interface';
import AppComponentBase from "../../../components/AppComponentBase";
import moment from 'moment';
import UpdateCreat from './components/update';
import GiaoVienQuaKhu from "../../../stores/AppStore/GiaoVien/GiaoVienQuaKhuStore";
import {inject, observer} from "mobx-react";
import Stores from "../../../stores/storeIdentifier";

const {confirm} = Modal;

interface IProps {
    giaoVienQuaKhu?: GiaoVienQuaKhu,
    giaoVienId?: any,
}
@inject(Stores.GiaoVienQuaKhu)
@observer
class GiaoVien_giaoVienQuaKhu extends AppComponentBase<IProps> {
    componentDidMount(): void {
        this.getAllServerPaging();
    }

    async getAllServerPaging() {
        const {giaoVienQuaKhu,giaoVienId} = this.props;
        giaoVienQuaKhu!.giaoVienId=giaoVienId;
        giaoVienQuaKhu!.getAllServerPaging();
    }
    async showEditModal(item?: any) {
        const {giaoVienQuaKhu} = this.props;
        await giaoVienQuaKhu!.showModal(item);
    }

    async delete(item: any) {
        const {giaoVienQuaKhu} = this.props;
        const self = this;
        confirm({
            title: 'Do you want to delete these items?',
            okType: 'danger',
            onOk() {
                if (item.companyId) {
                    giaoVienQuaKhu!.delete(item.id).then(() => self.getAllServerPaging());
                }
            },
            onCancel() {
            },
        });
    }

    render() {
        const {giaoVienQuaKhu} = this.props;
        const {Data} = giaoVienQuaKhu!;
        const columns: ColumnProps<any>[] = [
            {
                title: '#',
                key: 'STT',
                width: 50,
                render: (text: string, item: any, index: any) => (
                    <Fragment>
                        {index + 1}
                    </Fragment>
                ),
            },
            {

                title: 'Hành Động',
                key: 'action',
                width: 100,
                render: (text: string, item: any) => (
                    <Fragment>
                        <Button type="primary" icon="edit" title='EditUpdate'
                                onClick={() => this.showEditModal(item)}></Button>
                        <Button type="danger" icon="delete" title='Delete'
                                onClick={() => this.delete(item)}></Button>
                    </Fragment>
                ),
            },
            {
                title: 'Địa chỉ 1',
                dataIndex: 'diaChi_LamViec',
                key: 'diaChi_LamViec',
            },
            {
                title: 'Địa chỉ 2',
                dataIndex: 'diaChi_LamViec2',
                key: 'diaChi_LamViec2',
            },
            {
                title: 'Địa chỉ 3',
                dataIndex: 'diaChi_LamViec3',
                key: 'diaChi_LamViec3',
            },
            {
                title: 'Thời gian từ',
                dataIndex: 'thoiGian_Start',
                key: 'thoiGian_Start',
                render: (value: any, item: any) => {
                    let date =value?moment.utc(value).format('DD/MM/YYYY'):null;
                    return <div> {date} </div>;
                },
            },
            {
                title: 'Thời gian đến',
                dataIndex: 'thoiGian_End',
                key: 'thoiGian_End',
                render: (value: any, item: any) => {
                    let date =value?moment.utc(value).format('DD/MM/YYYY'):null;
                    return <div> {date} </div>;
                },
            },
            {
                title: 'Mô Tả',
                dataIndex: 'moTa',
                key: 'moTa',
            },
        ];
        return (
            <Card title={'Anniversary'} size='small'>
                <Row type="flex" justify="end">
                    <Col>
                        <Button type="primary" icon="plus"
                                onClick={() => giaoVienQuaKhu!.showModal()}>{'addRecord'} </Button>
                    </Col>
                </Row>
                <Table
                    scroll={{x: true}}
                    columns={columns}
                    size={'default'}
                    bordered={true}
                    rowKey={record => record.id}
                    dataSource={Data == undefined ? [] : Data}
                    loading={giaoVienQuaKhu!.isLoading}
                />
                {giaoVienQuaKhu!.isRender ?
                    <UpdateCreat storeData={giaoVienQuaKhu!}
                                   refreshGrid={() => this.getAllServerPaging()}/>
                    : null}
            </Card>

        );
    }
}

export default GiaoVien_giaoVienQuaKhu;
