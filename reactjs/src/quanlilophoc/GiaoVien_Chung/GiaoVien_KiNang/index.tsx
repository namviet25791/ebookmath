import React, {Fragment} from 'react';
import {Button, Col, Modal, Row, Table, Card} from 'antd';
import {ColumnProps} from 'antd/lib/table/interface';
import AppComponentBase from "../../../components/AppComponentBase";
import moment from 'moment';
import UpdateCreat from './components/update';
import {inject, observer} from "mobx-react";
import Stores from "../../../stores/storeIdentifier";
import GiaoVienKyNang from '../../../stores/AppStore/GiaoVien/GiaoVienKyNangStore';

const {confirm} = Modal;

interface IProps {
    giaoVienKyNang?: GiaoVienKyNang,
    giaoVienId?: any,
}

@inject(Stores.GiaoVienKyNang)
@observer
class GiaoVien_KiNang extends AppComponentBase<IProps> {
    componentDidMount(): void {
        this.getAllServerPaging();
    }

    async getAllServerPaging() {
        const {giaoVienKyNang, giaoVienId} = this.props;
        giaoVienKyNang!.giaoVienId=giaoVienId;
        giaoVienKyNang!.getAllServerPaging();
    }
    async showEditModal(item?: any) {
        const {giaoVienKyNang} = this.props;
        await giaoVienKyNang!.showModal(item);
    }

    async delete(item: any) {
        const {giaoVienKyNang} = this.props;
        const self = this;
        confirm({
            title: 'Do you want to delete these items?',
            okType: 'danger',
            onOk() {
                if (item.companyId) {
                    giaoVienKyNang!.delete(item.id).then(() => self.getAllServerPaging());
                }
            },
            onCancel() {
            },
        });
    }

    render() {
        const {giaoVienKyNang} = this.props;
        const {Data} = giaoVienKyNang!;
        const columns: ColumnProps<any>[] = [
            {
                title: '#',
                key: 'STT',
                width: 50,
                render: (text: string, item: any, index: any) => (
                    <Fragment>
                        {index + 1}
                    </Fragment>
                ),
            },
            {
                title: 'Hành Động',
                key: 'action',
                width: 100,
                render: (text: string, item: any) => (
                    <Fragment>
                        <Button type="primary" icon="edit" title='EditUpdate'
                                onClick={() => this.showEditModal(item)}></Button>
                        <Button type="danger" icon="delete" title='Delete'
                                onClick={() => this.delete(item)}></Button>
                    </Fragment>
                ),
            },
            {
                title: 'Trường học',
                dataIndex: 'truongHoc',
                key: 'truongHoc',
            },
            {
                title: 'Thời gian từ',
                dataIndex: 'thoiGian_Start',
                key: 'thoiGian_Start',
                render: (value: any, item: any) => {
                    let date =value?moment.utc(value).format('DD/MM/YYYY'):null;
                    return <div> {date} </div>;
                },
            },
            {
                title: 'Thời gian đến',
                dataIndex: 'thoiGian_End',
                key: 'thoiGian_End',
                render: (value: any, item: any) => {
                    let date =value?moment.utc(value).format('DD/MM/YYYY'):null;
                    return <div> {date} </div>;

                },
            },
            {
                title: 'Lớp',
                dataIndex: 'lopSo',
                key: 'lopSo',
            },
            {
                title: 'Năm',
                dataIndex: 'year',
                key: 'year',
            },
            {
                title: 'Thanh Tich',
                dataIndex: 'giaoVienKyNang',
                key: 'giaoVienKyNang',
            },
            {
                title: 'Mô Tả',
                dataIndex: 'moTa',
                key: 'moTa',
            },
        ];
        return (
            <Card title={'Anniversary'} size='small'>
                <Row type="flex" justify="end">
                    <Col>
                        <Button type="primary" icon="plus"
                                onClick={() => this.showEditModal()}>{'addRecord'} </Button>
                    </Col>
                </Row>
                <Table
                    scroll={{x: true}}
                    columns={columns}
                    size={'default'}
                    bordered={true}
                    rowKey={record => record.id}
                    dataSource={Data == undefined ? [] : Data}
                    loading={giaoVienKyNang!.isLoading}
                />
                {giaoVienKyNang!.isRender ?
                    <UpdateCreat storeData={giaoVienKyNang!}
                                   refreshGrid={() => this.getAllServerPaging()}/>
                    : null}
            </Card>

        );
    }
}

export default GiaoVien_KiNang;
