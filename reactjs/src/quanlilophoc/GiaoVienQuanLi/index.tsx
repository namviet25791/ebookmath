import React, {Fragment} from 'react';
import AppComponentBase from '../../components/AppComponentBase';
import {inject, observer} from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import {ColumnProps} from 'antd/lib/table/interface';
import {L} from '../../lib/abpUtility';
import {Button, Card, Modal, Table, Col, Row, Divider, Avatar} from 'antd';
import Filter from './components/filter';
import GiaoVienStore from '../../stores/AppStore/GiaoVien/GiaoVienStore';
import {History, LocationState} from "history";

const {confirm} = Modal;

interface IPayProps {
    giaoVienStore: GiaoVienStore
    history: History<LocationState>;
}

@inject(Stores.GiaoVienStore)
@observer
class GiaovienCreen extends AppComponentBase<IPayProps> {
    componentDidMount(): void {
        this.getAllServerPaging();
    }

    async getAllServerPaging() {
        await this.props.giaoVienStore.getAllServerPaging();
    }

    handleTableChange = async (pagination: any, filters: any, sorter: any) => {
        const {giaoVienStore} = this.props!;
        const {filter} = giaoVienStore;
        giaoVienStore.currentPage = pagination.current;
        filter.maxResultCount = pagination.pageSize;
        filter.skipCount = (pagination.current - 1) * pagination.pageSize;
        await this.getAllServerPaging();
    };
    handleSearch = async () => {
        await this.getAllServerPaging();
    };

    async showEditModal(id: any) {
        this.props.history.push(`/quanlilophoc/giaoviencreatedit/${id}`)
    }

    async delete(id?: any) {
        const {giaoVienStore} = this.props;
        const self = this;
        confirm({
            title: 'Do you want to delete these items?',
            okType: 'danger',
            onOk() {
                giaoVienStore.delete(id).then(() => self.getAllServerPaging())
            },
            onCancel() {
            },
        })
    }


    render() {
        const {giaoVienStore} = this.props;
        const {dataPaging, isLoading} = giaoVienStore;
        const columns: ColumnProps<any>[] = [
                {
                    title: '#',
                    key: 'STT',
                    width: 50,

                    render: (text: string, item: any, index: any) => (
                        <Fragment>
                            {index + 1 + giaoVienStore.filter.skipCount}
                        </Fragment>
                    ),
                },
                {

                    title: 'Actions',
                    key: 'action',
                    width: 100,
                    render: (text: string, item: any) => (
                        <Fragment>
                            <Button type="primary" icon="edit" title={L('Edit')} onClick={() => this.showEditModal(item.id)}></Button>
                            <Button type="danger" icon="delete" title={L('Delete')} onClick={() => this.delete(item.id)}></Button>
                        </Fragment>
                    ),
                },
                {
                    title: 'Tên',
                    dataIndex:'ten',
                    key:'ten',
                }
                ,
                {
                    title: 'Avatar',
                    dataIndex:'avatar',
                    key:'avatar',
                    render:(value: any, item: any) => (
                        <Avatar shape="square" src={`data:image/jpeg;base64,${value}`}/>
                    )
                }
                ,
                {
                    title: 'Tuổi',
                    dataIndex:'tuoi',
                    key: 'tuoi',
                }
                ,
                {
                    title: 'Giới tính',
                    dataIndex:'gioiTinh',
                    key: 'gioiTinh',
                    render:
                        (value: any, item: any) => {
                            if (value == '0') return <div>Giới tính ba</div>;
                            else if (value == '1') return <div>Nam</div>;
                            else return <div>Nữ</div>;
                        }
                }
                ,
                {
                    title: 'Môn học',
                    dataIndex:'monHoc',
                    key:'monHoc',
                }
                ,
                {
                    title: 'Trường',
                    dataIndex:'truong',
                    key:'truong',
                }
                ,
                {
                    title: 'Bằng cấp',
                    dataIndex:'bangCapId',
                    key:'bangCapId',
                }
                ,
                {
                    title: 'Trạng thái',
                    dataIndex:'trangThai',
                    key:'trangThai',
                }
                ,
            ]
        ;


        return (
            <>
                <Card style={{marginBottom: '10px'}}>
                    <Row type="flex" justify="space-between">
                        <Col>
                            <h2 style={{color: '#b3ba21'}}>Quản lí Giáo viên
                                <Divider type="vertical" style={{height: '50px'}}/>
                                <Divider dashed style={{margin: '5px'}}/>
                            </h2>
                        </Col>
                        <Col>
                            <Button type="primary" icon="plus"
                                    onClick={() => this.showEditModal(0)}>{L('AddRecord')} </Button>
                        </Col>
                    </Row>
                    <Filter storeData={giaoVienStore}/>
                </Card>
                <Card>
                    <Table
                        columns={columns}
                        size={'default'}
                        bordered={true}
                        rowKey={record => record.id}
                        dataSource={dataPaging == undefined ? [] : dataPaging.items}
                        pagination={{
                            pageSize: giaoVienStore.filter.maxResultCount,
                            total: dataPaging === undefined ? 0 : dataPaging.totalCount,
                            defaultCurrent: 1,
                            pageSizeOptions: ['5', '10', '20', '50'],
                            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} records\``,
                            showSizeChanger: true,
                        }}
                        loading={isLoading}
                        onChange={this.handleTableChange}
                    />
                </Card>
            </>
        );
    }
}

export default GiaovienCreen;
