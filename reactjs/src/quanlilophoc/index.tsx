import * as React from 'react';

import {Switch, Route} from 'react-router-dom';

import ProtectedRoute from '../components/Router/ProtectedRoute';
import SiderMenu from './SiderMenu';
import {quanlilophocRouter} from '../components/Router/router.config';
import {Row, Col} from 'antd';
import './index.less';


class QuanLiLopHoc extends React.Component<any> {

    render() {
        const {
            history,
        } = this.props;
        return (
            <Row gutter={5} align={'top'} type={'flex'}>
                <Col md={24} lg={4}>
                    <SiderMenu history={history}/>
                </Col>
                <Col md={24} lg={16}>
                    <Switch>
                        {quanlilophocRouter
                            .filter((item: any) => !item.isLayout)
                            .map((route: any, index: any) => (
                                <Route
                                    key={index}
                                    path={route.path}
                                    render={props => <ProtectedRoute component={route.component}
                                                                     permission={route.permission}/>}
                                />
                            ))}
                    </Switch>
                </Col>
            </Row>
        );
    }
}

export default QuanLiLopHoc;
