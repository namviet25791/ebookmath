import React from 'react';
import {Form, Modal, Row, Col, Input, InputNumber, DatePicker, Tabs,TimePicker} from 'antd';
import {observer} from 'mobx-react';
import {FormComponentProps} from 'antd/lib/form';
import * as _ from 'lodash';
import LopLonStore from '../../../stores/AppStore/LopHoc/lopLonStore';
import {RowProps} from 'antd/es/row';
/*import rules from "./rule.validation";*/
import AppComponentBase from '../../../components/AppComponentBase';
import LoaiLopSelect from "../../../components/Combobox/LoaiLop";
import moment from 'moment';
import MonHocSelect from "../../../components/Combobox/MonHoc";
import StatusSelect from "../../../components/Combobox/Status";
import MapForm from '../../../components/Shared/mapForm';

interface IProps extends FormComponentProps {
    storeData: LopLonStore,
    refreshGrid: () => void

}

interface IState {
    formVals: any,
}

@observer
class CreatOrUpdate extends AppComponentBase<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            formVals: props.storeData.modalCreateOrUpdate.dataModal,
        };
    }

    save = async () => {
        const {form} = this.props;
        await form.validateFields(async (err: any, fieldsValue: any) => {
            if (err) {
                return;
            } else {
                // const formVals:FnbCategoryDto = { ...oldValue, ...fieldsValue };
                const {modalCreateOrUpdate} = this.props.storeData;
                await modalCreateOrUpdate.updateData({...fieldsValue});
                await this.props.refreshGrid();
                modalCreateOrUpdate.closeModal();
            }
        });
    };

    render() {
        const {modalCreateOrUpdate} = this.props.storeData;
        const {formVals} = this.state;
        const {form} = this.props;
        const {getFieldDecorator} = form;
        const formItemLayout = {
            labelCol: {span: 24},
            wrapperCol: {span: 24},
            style: {marginBottom: 0},
        };
        const rowLayout: RowProps = {
            gutter: [10, 0],
            type: 'flex',
        };
        const {TabPane} = Tabs;
        const {TextArea} = Input;
        return (
            <Modal className={'Modal-Color'}
                   width={900}
                   title={modalCreateOrUpdate.dataModal.companyId ? 'Chỉnh Sửa Lớp' : 'Tạo Mới Lớp'}
                   visible={modalCreateOrUpdate.isShow}
                   onCancel={() => modalCreateOrUpdate.closeModal()}
                   onOk={() => this.save()}
            >

                <Form>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Thông tin lớp" key="1">
                            <Row  {...rowLayout}>
                                <Col xs={12} lg={6}>
                                    <Form.Item {...formItemLayout} label={'Mã Code'}>
                                        {getFieldDecorator('codeId', {initialValue: formVals.codeId})(
                                            <Input/>,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={6}>
                                    <Form.Item {...formItemLayout} label={'Tên'}>
                                        {getFieldDecorator('ten', {initialValue: formVals.ten})(
                                            <Input/>,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={6}>
                                    <Form.Item  {...formItemLayout} label={'Loại Lớp'}>
                                        {getFieldDecorator('loaiLopId', {
                                            initialValue: formVals.loaiLopId,
                                        })(
                                            <LoaiLopSelect/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={2}>
                                    <Form.Item  {...formItemLayout} label={'Lớp số'}>
                                        {getFieldDecorator('lopSo', {initialValue: formVals.lopSo})(
                                            <InputNumber width={'100%'}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={2}>
                                    <Form.Item  {...formItemLayout} label={'Ngày'}>
                                        {getFieldDecorator('ngay', {initialValue: formVals.ngay})(
                                            <InputNumber width={'100%'}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} lg={4}>
                                    <Form.Item  {...formItemLayout} label={'Môn học'}>
                                        {getFieldDecorator('monHocId', {initialValue: formVals.monHocId})(
                                            <MonHocSelect/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={6}>
                                    <Form.Item>
                                        <Form.Item  {...formItemLayout} label={'Thời gian từ'} style={{display:'inline-block',width: 'calc(50% - 12px)'}}>
                                            {getFieldDecorator('thoigian_Start')(
                                                <TimePicker/>)}
                                        </Form.Item>
                                        <Form.Item  {...formItemLayout} label={'Thời gian đến'} style={{display:'inline-block',width: 'calc(50% - 12px)'}}>
                                            {getFieldDecorator('thoigian_End')(
                                                <TimePicker/>)}
                                        </Form.Item>
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={4}>
                                    <Form.Item  {...formItemLayout} label="Ngày đăng kí">
                                        {getFieldDecorator('thoiGian_Lon', {initialValue: formVals.thoiGian_End ? moment(formVals.thoiGian_Start, 'YYYY/MM/DD') : null})
                                        (<DatePicker/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={6}>
                                    <Form.Item  {...formItemLayout} label={'Trạng Thái'}>
                                        {getFieldDecorator('statusId', {initialValue: formVals.statusId})(
                                            <StatusSelect loai={'LopGS'}/>)}
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={3}>
                                        <Form.Item  {...formItemLayout} label={'Phí học/1HS'}>
                                            {getFieldDecorator('giaovienId', {initialValue: formVals.giaovienId})(
                                                <InputNumber/>)}
                                        </Form.Item>
                                </Col>
                                <Col xs={24} lg={3}>
                                        <Form.Item  {...formItemLayout} label={'Số lượng'}>
                                            {getFieldDecorator('soLuong', {initialValue: formVals.soLuong})(
                                                <InputNumber/>)}
                                        </Form.Item>
                                </Col>
                                <Col xs={24} lg={3}>
                                        <Form.Item  {...formItemLayout} label={'Số lượng Max'}>
                                            {getFieldDecorator('soLuong_Max', {initialValue: formVals.soLuong_Max})(
                                                <InputNumber/>)}
                                        </Form.Item>
                                </Col>
                                <Col xs={24} lg={24}>
                                    <Form.Item  {...formItemLayout} label='Mô Tả'>
                                        {getFieldDecorator('moTa', {initialValue: formVals.moTa})(
                                            <TextArea autoSize={{minRows: 1, maxRows: 2}}/>)}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tab="Địa chỉ lớp trên bản đồ" key="3">
                            <Form.Item  {...formItemLayout}>
                                {getFieldDecorator('toado', {initialValue: formVals.toado})(
                                    <MapForm />)}
                            </Form.Item>
                        </TabPane>
                    </Tabs>
                </Form>
            </Modal>
        );
    }
}

export default Form.create<IProps>()(CreatOrUpdate);
