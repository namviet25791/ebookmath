import React from 'react';
import reactCSS from 'reactcss';
import { ChromePicker } from 'react-color';

interface IColorPickerProps {
  value?: any;
  onChange?: (args: any) => void;
}

class ColorPicker extends React.Component<IColorPickerProps> {
  state = {
    displayColorPicker: false,
  };
  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker });
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false });
  };

  handleChange = (color: any) => {
    if (this.props.onChange)
      this.props.onChange(color.hex);
  };

  render() {
    const styles = reactCSS({
      'default': {
        wapper:{height:"40px",width:"100%",display:'flex',alignItems:'center'},
        color: {
          width: '46px',
          height: '22px',
          borderRadius: '1px',
          background: `${ this.props.value}`,
        },
        swatch: {
          padding: '4px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        // popover: { position: 'absolute', zIndex: 2 },
        // cover: {
        //   position: 'fixed',
        //   top: '0px',
        //   right: '0px',
        //   bottom: '0px',
        //   left: '0px',
        // },
      },
    });

    return (
      <div style={styles.wapper}>
        <div style={styles.swatch} onClick={this.handleClick}>
          <div style={styles.color}/>
        </div>
        {this.state.displayColorPicker ? <div style={{ position: 'absolute', zIndex: 2 }}>
          <div style={{ position: 'fixed', top: 0, bottom: 0, right: 0, left: 0 }} onClick={this.handleClose}/>
          <ChromePicker   disableAlpha={true} color={this.props.value} onChange={this.handleChange}/>
        </div> : null}

      </div>
    );
  }
}

export default ColorPicker;