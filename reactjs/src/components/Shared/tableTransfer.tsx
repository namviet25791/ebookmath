import React, { Component } from 'react';
import { Table, Transfer } from 'antd';
import * as _ from 'lodash';

interface ITableTransferProps {
  dataSource: any[],
  value?: any[],//targetKeys
  onChange?: (args: any) => void;
  listDisabled?: boolean,
  leftColumns: any,
  rightColumns: any,
  title?: string[]
}

class TableTransfer extends Component<ITableTransferProps> {
  handleChange = (value: any) => {
    console.log(value,"valueTabletranF")
    if (this.props.onChange)
      this.props.onChange(value);
  };

  render() {
    return (
      <Transfer
        titles={this.props.title}
        rowKey={(record: any) => record.id.toString()}
        {...this.props} targetKeys={this.props.value}
        showSelectAll={false}
        onChange={this.handleChange}>
        {({
            direction,
            filteredItems,
            onItemSelectAll,
            onItemSelect,
            selectedKeys: listSelectedKeys,
            disabled: listDisabled,
          }) => {
          const columns = direction === 'left' ? this.props.leftColumns : this.props.rightColumns;

          const rowSelection = {
            getCheckboxProps: (item: any) => ({ disabled: listDisabled }),
            onSelectAll(selected: any, selectedRows: any) {
              const treeSelectedKeys = selectedRows
                .map((item: any, index: any) => item.id.toString());
              const diffKeys = selected
                ? _.difference(treeSelectedKeys, listSelectedKeys)
                : _.difference(listSelectedKeys, treeSelectedKeys);
              onItemSelectAll(diffKeys, selected);
            },
            onSelect({ id }: any, selected: any) {
              onItemSelect(id, selected);
            },
            selectedRowKeys: listSelectedKeys,
          };

          return (
            <Table
              rowSelection={rowSelection}
              columns={columns}
              rowKey={record => record.id.toString()}
              dataSource={filteredItems}
              size="small"
              onRow={({ id }) => ({
                onClick: () => {
                  if (listDisabled) return;
                  onItemSelect(id, !listSelectedKeys.includes(id));
                },
              })}
            />
          );
        }}
      </Transfer>
    );
  }
}

export default TableTransfer;