import React, { Component } from 'react';
import { Icon, message, Upload, Button } from 'antd';
import './uploadImg.css';
import { L } from '../../lib/abpUtility';

interface IUploadImgProps {
  value?: any;
  onChange?: (args: any) => void;
  width?: string,
  height?: string,
}

interface IUploadImgState {
  loading: boolean,
  // imgData: any,
}

class UploadImg extends Component<IUploadImgProps, IUploadImgState> {
  constructor(props: IUploadImgProps) {
    super(props);
    this.state = {
      loading: false,
      // imgData: props.value||''
    };
  }

  //#region Upload File
  beforeUpload(file: any) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  }

  getBase64(img: any, callback: any) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange = (info: any) => {
    const isJpgOrPng = info.file.type === 'image/jpeg' || info.file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
      return;
    }
    const isLt2M = info.file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
      return;
    }

    this.getBase64(info.file, (imgData: any) => {
        if (this.props.onChange)
          this.props.onChange(imgData);
      },
    );
  };

  //#endregion
  renderImg = () => {
    if (!this.props.value)
      return <div className='div-button-up'>
        <Icon type={this.state.loading ? 'loading' : 'plus'}/>
        <div className="ant-upload-text">Upload</div>
      </div>;
    else return <>
      <img className='content-box-imgupload' src={this.props.value} alt="avatar"/>
    </>;
  };

  deleteImg() {
    if (this.props.onChange)
      this.props.onChange('');
  }

  renderIconDelete = () => {
    if (this.props.value)
      return <Button type='danger' title={L('Delete')} onClick={() => this.deleteImg()}><Icon
        type="delete"></Icon></Button>;
    else
      return <></>;
  };

  render() {
    return (
      <>
        <div style={{ textAlign: 'center'}} className='box-imgupload'>
          <Upload
            name="avatar"
            listType="picture-card"
            className="content-box-imgupload"
            showUploadList={false}
            beforeUpload={() => false}
            onChange={this.handleChange}
          >
            {this.renderImg()}
          </Upload>
          {this.renderIconDelete()}
        </div>

      </>

    );
  }
}

export default UploadImg;
