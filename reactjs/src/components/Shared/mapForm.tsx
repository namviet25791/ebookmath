import React, {Component} from 'react';
import './mapForm.less';
import {GoogleMap, Marker, LoadScript} from '@react-google-maps/api'

interface IUploadImgProps {
    value?: any;
    onChange?: (args: any) => void;
}

interface IUploadImgState {
    toaDoX: any,
    toaDoY: any,
    // imgData: any,
}

class MapForm extends Component<IUploadImgProps, IUploadImgState> {
    constructor(props: IUploadImgProps) {
        super(props);
        this.state = {
            toaDoX: 21.0679188,
            toaDoY: 105.7904325,
        };
    }

    async componentDidMount() {
        if (this.props.value) {
                const item = this.props.value.split(',');
                console.log(item);
                this.setState({
                    toaDoX: item[0]!=null?Number(item[0]):21.0679188,
                    toaDoY: item[1]!=null?Number(item[1]):105.7904325
                })
        }
    };

    //#region Upload File
    onChangeValue = (value: any) => {
        console.log(value);
        const {onChange} = this.props;
        if (onChange)
            onChange(value);
    };
    onCursorChanged = () => {

    }
    onMarkerDragEnd = (e:any) => {
        console.log(e);
        const { latLng } = e;
        const lat = latLng.lat();
        const lng = latLng.lng();
        this.setState({
            toaDoX: lat,
            toaDoY: lng
        })
        const {onChange} = this.props;
        if (onChange)
            onChange(lat+","+lng);
    };
    render() {
        const {toaDoX, toaDoY} = this.state;
        return (
            <div style={{height: '100%', width: '100%'}}>
                <LoadScript
                    id="script-loader"
                    googleMapsApiKey="AIzaSyCWzKvVqjp3TgY347xdu1cScyGOLo-C_zM"
                >
                    <GoogleMap
                        id='data-example'
                        zoom={13}
                        center={{lat: toaDoX, lng: toaDoY}}
                        onClick={this.onMarkerDragEnd}
                    >
                        <Marker position={{lat: toaDoX, lng: toaDoY}} draggable onDragEnd={(e) => this.onMarkerDragEnd(e)}/>
                    </GoogleMap>
                </LoadScript>
            </div>

        );
    }
}

export default MapForm;
