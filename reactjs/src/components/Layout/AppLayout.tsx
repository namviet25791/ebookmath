import './AppLayout.less';

import * as React from 'react';

import {Redirect, Switch} from 'react-router-dom';

import DocumentTitle from 'react-document-title';
import ProtectedRoute from '../../components/Router/ProtectedRoute';
import {appRouters} from '../Router/router.config';
import utils from '../../chung/utils/utils';
import {isGranted} from '../../lib/abpUtility';
import Footer from '../Home/Footer';
import Header from '../Home/Header';
import {Affix} from 'antd';

class AppLayout extends React.Component<any> {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  onCollapse = (collapsed: any) => {
    this.setState({ collapsed });
  };
  onEnterChange = (mode: any) => {
    this.setState({
      isFirstScreen: mode === 'enter',
    });
  }
  renderAppRouters = (routes: any[]): any => {
    return routes.map(route => {
      if (route.permission && !isGranted(route.permission)) return null;
      if (route.children) {
        return this.renderAppRouters(route.children);
      }
      if (route.component) {
        return (
            <ProtectedRoute key={JSON.stringify(route)} path={route.path} component={route.component}
                            permission={route.permission}/>
        );
        /*else return (
            <Row   justify="center" align="top" style={{marginTop: '64px'}}>
              <Col xs={{span: 24}} lg={{span: 16}}>
                <ProtectedRoute key={JSON.stringify(route)} path={route.path} component={route.component}
                                permission={route.permission}/>
              </Col>
            </Row>
        );*/
      }
    });
  };
  render() {
    const {
      history,
      location: { pathname },
    } = this.props;

    /*const { path } = this.props.match;
    const { collapsed } = this.state;*/

    const layout = (
     /* <Layout style={{ minHeight: '100vh' }}>
        <SiderMenu path={path} onCollapse={this.onCollapse} history={history} collapsed={collapsed} />
        <Layout>
          <Layout.Header style={{ background: '#fff', minHeight: 52, padding: 0 }}>
            <Header collapsed={this.state.collapsed} toggle={this.toggle} />
          </Layout.Header>
          <Content style={{ margin: 16 }}>
            <Switch>
              {pathname === '/' && <Redirect from="/" to="/dashboard" />}
              {appRouters
                .filter((item: any) => !item.isLayout)
                .map((route: any, index: any) => (
                  <Route
                    exact
                    key={index}
                    path={route.path}
                    render={props => <ProtectedRoute component={route.component} permission={route.permission} />}
                  />
                ))}
              {pathname !== '/' && <NotFoundRoute />}
            </Switch>
          </Content>
          <Layout.Footer style={{ textAlign: 'center' }}>
            <Footer />
          </Layout.Footer>
        </Layout>
      </Layout>*/
        <div style={{background:'#f0f2f5'}}>
          <Affix offsetTop={0}>
            <Header history={history}/>
          </Affix>
          <div style={{maxWidth:'1400px', margin:'auto',minHeight:'800px'}}>
                  <Switch>
                    {this.renderAppRouters(appRouters.filter((item: any) => !item.isLayout))}
                    <Redirect from="/" to="/dashboard"/>
                  </Switch>
          </div>
          < Footer/>
        </div>
    );

    return <DocumentTitle title={utils.getPageTitle(pathname)}>{layout}</DocumentTitle>;
  }
}

export default AppLayout;
