import './index.less';

import * as React from 'react';

import {Avatar, Badge, Dropdown, Menu, Row} from 'antd';
import {LogoutOutlined,LoginOutlined} from '@ant-design/icons'
import {Link} from 'react-router-dom';

import profilePicture from '../../../chung/images/user.png';
import {inject, observer} from "mobx-react";
import Stores from "../../../stores/storeIdentifier";
import SessionStore from '../../../stores/sessionStore';

declare var abp: any;



const userDropdownMenu = () => {
    if (!abp.session.userId) {
        return (<Menu>
                <Menu.Item key="2">
                    <Link to="/logout">
                        <LogoutOutlined  />
                        <span>Đăng xuất</span>
                    </Link>
                </Menu.Item>
            </Menu>
        )
    } else {
        return (<Menu>
                <Menu.Item key="2">
                    <Link to="/logout">
                        <LoginOutlined />
                        <span> Đăng xuất</span>
                    </Link>
                </Menu.Item>
            </Menu>
        )
    }
};
export interface IProps {
    collapsed?: any;
    toggle?: any;
    sessionStore?:SessionStore;
}
@inject(Stores.SessionStore)
@observer
export class RightHeader extends React.Component<IProps> {
    render() {
        const {sessionStore} =this.props;
        const {currentLogin}=sessionStore!;
        const {user}=currentLogin;
        return (
            <Row gutter={16}>
                    {user?user.name:"test"}
                    <Dropdown overlay={userDropdownMenu} trigger={['click']}>
                        <Badge style={{}} count={3}>
                            <Avatar style={{height: 24, width: 24}} shape="circle" alt={'profile'}
                                    src={profilePicture}/>
                        </Badge>
                    </Dropdown>
            </Row>
        );
    }
}

export default RightHeader;
