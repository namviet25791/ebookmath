import React from 'react';
import DocumentTitle from 'react-document-title';
import { enquireScreen } from 'enquire-js';

import Header from './Header';
import Banner from './Components/Banner';
import ChuyenHinh from './Carousel';
import Page1 from './Components/Page1';
import Footer from './Footer';
import Content8 from './Components/Content8';
import {Content80DataSource} from './Components/data';
import Content9 from './Components/Content9';
import {Content90DataSource} from './Components/data';
import './Less/style';

let isMobile;

enquireScreen((b) => {
  isMobile = b;
});

class Home extends React.PureComponent {
  state = {
    isMobile:false,
  }
  componentDidMount() {
    enquireScreen((b) => {
      this.setState({
        isMobile: !!b,
      });
    });
  }
  onEnterChange = (mode) => {
    this.setState({
      isFirstScreen: mode === 'enter',
    });
  }
  render() {
    return (
      <DocumentTitle title="Math Club - Pro">
          <div className="home-wrapper">
            <ChuyenHinh isMobile={this.state.isMobile} />
            <Page1 isMobile={this.state.isMobile} />
            <Content9
                id="Content9_0"
                key="Content9_0"
                dataSource={Content90DataSource}
                isMobile={this.state.isMobile}
            />
            <Content8
                id="Content8_0"
                key="Content8_0"
                dataSource={Content80DataSource}
                isMobile={this.state.isMobile}
            />
            {/*<Page2 />*/}
          </div>
      </DocumentTitle>
    );
  }
}

export default Home;
