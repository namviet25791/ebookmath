import {Carousel} from 'antd';
import QueueAnim from "rc-queue-anim";
import PropTypes from "prop-types";
import React from "react";
import './index.less';
import Banner from "../Components/Banner";


function ChuyenHinh(props) {
    return (
        <Carousel>
            <div>
                <Banner isMobile={props.isMobile}/>
            </div>
            <div>
                <h3>2</h3>
            </div>
            <div>
                <h3>3</h3>
            </div>
            <div>
                <h3>4</h3>
            </div>
        </Carousel>
    );
}

Banner.propTypes = {
    isMobile: PropTypes.bool.isRequired,
};
export default ChuyenHinh;
