import React from 'react';
import {Row, Col, Icon, Menu, Button, Popover, Dropdown, Badge, Avatar, Drawer} from 'antd';
import {appRouters} from '../../components/Router/router.config';

import {enquireScreen} from 'enquire-js';
import {isGranted, L} from "../../lib/abpUtility";
import "./Less/header.less";
import "./Less/responsive.less";
import {Link} from "react-router-dom";
import profilePicture from "../../chung/images/user.png";
import RightHeader from "./RightHeader";

const LOGO_URL = 'https://gw.alipayobjects.com/zos/rmsportal/gVAKqIsuJCepKNbgbSwE.svg';
const {SubMenu} = Menu;


class Header extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        menuVisible: false,
        menuMode: 'horizontal',
        visible: false,
        typeIcon: 'menu-unfold',
    };

    componentDidMount() {
        enquireScreen((b) => {
            this.setState({menuMode: b ? 'inline' : 'horizontal'});
        });
    }

    renderSubMenus = (routes) =>
        routes.map(route => {
            if (route.permission && !isGranted(route.permission)) return null;
            if (route.children) {
                return (
                    <SubMenu key={JSON.stringify(route)}
                             title={<span>
                        <Icon type={route.icon}/>
                        <span>{L(route.title)}</span>
                      </span>}>
                        {this.renderSubMenus(route.children)}
                    </SubMenu>
                );
            }
            return (
                <Menu.Item key={route.path} onClick={() => this.props.history.push(route.path)}>
                    <Icon type={route.icon}/>
                    <span>{L(route.title)}</span>
                </Menu.Item>
            );
        });
    showDrawer = () => {
        this.setState({
            visible: true,
            typeIcon: 'menu-fold',
        });
    };
    onClose = () => {
        this.setState({
            visible: false,
            typeIcon: 'menu-unfold',
        });
    };

    render() {
        const {menuMode, menuVisible} = this.state;
        const {history} = this.props;
        const menu = (
            <Menu mode={menuMode} id="nav" key="nav">
                {this.renderSubMenus(appRouters
                    .filter((item) => !item.isLayout && item.showInMenu))}
            </Menu>
        );
        if (menuMode === 'horizontal') {
            return (
                <div id="header">
                    <div id="headerSub">
                        <Row align={'middle'} justify="space-between" type={'flex'} gutter={10}>
                            <Col lg={5} xl={3}>
                                <div id="logo">
                                    <img src={LOGO_URL} alt="logo"/>
                                    <span>MathClub</span>
                                </div>
                            </Col>
                            <Col lg={14} xl={16} id={'menu'}>
                                {menu}
                            </Col>
                            <Col lg={5} xl={4}>
                                <RightHeader/>
                            </Col>
                        </Row>
                    </div>
                </div>
            )
        } else {
            return (
                <div id="header">
                    <div id="headerLittle">
                        <Row type="flex" justify="space-between" align={'middle '}>
                            <Col span={6}>
                                <img src={LOGO_URL} alt="logo" style={{paddingLeft:'10px'}}/>
                            </Col>
                            <Col span={15} id={'avatar'}>
                                <RightHeader/>
                            </Col>
                            <Col span={3}>
                                <Icon type={this.state.typeIcon} onClick={this.showDrawer} style={{fontSize: '25px'}}/>
                            </Col>
                        </Row>
                        <Drawer
                            placement="left"
                            closable={false}
                            onClose={this.onClose}
                            visible={this.state.visible}
                        >
                            {menu}
                        </Drawer>
                    </div>
                </div>
            )
        }
        /*return (
            <div id="header">
                {menuMode === 'inline' ? (
                    <div>

                    </div>
                ) : null}
                <Row>
                    <Col lg={4} sm={24}>
                        <Row align={'middle'} justify="space-between">
                            <Col span={16}>

                            </Col>
                            <Col span={8}>
                                {menuMode === 'inline' ?
                                    <>


                                    </>
                                    : null}
                            </Col>
                        </Row>

                    </Col>
                    {menuMode === 'horizontal' ?
                        <Col xxl={20} xl={19} lg={16} md={16} sm={0} xs={0}>
                            <div className="header-meta">
                                <div id="preview">

                                </div>
                                <div id="menu">
                                    {menu}
                                </div>
                            </div>
                        </Col>
                        : null}

                </Row>
            </div>
        );*/
    }
}

export default Header;
