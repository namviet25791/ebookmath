import LoadableComponent from './../Loadable/index';

export const userRouter: any = [
  {
    path: '/user',
    name: 'user',
    title: 'User',
    component: LoadableComponent(() => import('../../components/Layout/UserLayout')),
    isLayout: true,
    showInMenu: false,
  },
  {
    path: '/user/login',
    name: 'login',
    title: 'LogIn',
    component: LoadableComponent(() => import('../../chung/scenes/Login')),
    showInMenu: false,
  },
];

export const appRouters: any = [
  {
    path: '/',
    exact: true,
    name: 'home',
    permission: '',
    title: 'Home',
    icon: 'home',
    component: LoadableComponent(() => import('../../components/Layout/AppLayout')),
    isLayout: true,
    showInMenu: false,
  },
  {
    path: '/dashboard',
    name: 'Trang Chủ',
    permission: '',
    title: 'Trang Chủ',
    icon: 'home',
    showInMenu: true,
    component: LoadableComponent(() => import('../../components/Home')),
  },
  {
    path: '/quanlilophoc',
    name: 'Quản lí lớp học',
    permission: '',
    title: 'Quản lí lớp học',
    icon: 'home',
    component: LoadableComponent(() => import('../../quanlilophoc')),
    showInMenu: true,
  },
  {
    path: '/quanlibaiviet',
    name: 'Quản lí bài viet',
    permission: '',
    title: 'Quản lí bài viết',
    icon: 'home',
    component: LoadableComponent(() => import('../../baiviet/QuanLiBaiViet')),
    showInMenu: true,
  },
  {
    path: '/quanlitailieu',
    name: 'Tài Liệu Toán',
    permission: '',
    title: 'Tài Liệu Toán',
    icon: 'home',
/*
    component: LoadableComponent(() => import('../../quanlilophoc')),
*/
    showInMenu: true,
  },
  {
    path: '/Cauhinh',
    name: 'Cấu Hình',
    permission: '',
    title: 'Cấu hình',
    icon: 'home',
    showInMenu: true,
    children: [
      {
        path: '/users',
        permission: 'Pages.Users',
        title: 'Users',
        name: 'user',
        icon: 'user',
        showInMenu: true,
        component: LoadableComponent(() => import('../../chung/scenes/Users')),
      },
      {
        path: '/roles',
        permission: 'Pages.Roles',
        title: 'Roles',
        name: 'role',
        icon: 'tags',
        showInMenu: true,
        component: LoadableComponent(() => import('../../chung/scenes/Roles')),
      },
      {
        path: '/tenants',
        permission: 'Pages.Tenants',
        title: 'Tenants',
        name: 'tenant',
        icon: 'appstore',
        showInMenu: true,
        component: LoadableComponent(() => import('../../chung/scenes/Tenants')),
      },
    ],
  },
  {
    path: '/logout',
    permission: '',
    title: 'Logout',
    name: 'logout',
    icon: 'info-circle',
    showInMenu: false,
    component: LoadableComponent(() => import('../../components/Logout')),
  },
  {
    path: '/exception?:type',
    permission: '',
    title: 'exception',
    name: 'exception',
    icon: 'info-circle',
    showInMenu: false,
    component: LoadableComponent(() => import('../../chung/scenes/Exception')),
  },
];
export const quanlilophocRouter: any = [
  {
    path: '/quanlilophoc/quanligiaovien',
    name: 'Quản Lí GV',
    title: 'Quản Lí GV',
    icon: 'info-circle',
    component: LoadableComponent(() => import('../../quanlilophoc/GiaoVienQuanLi')),
    showInMenu: true,
  },
  {
    path: '/quanlilophoc/giaoviencreatedit/:id',
    name: 'Thêm mới, chỉnh sửa Giáo viên',
    component: LoadableComponent(() => import('../../quanlilophoc/CreatEditGiaoVien')),
    showInMenu: false,
  },
  {
    path: '/quanlilophoc/giaovienView',
    name: 'Danh Sách GV',
    title: 'Danh Sách GV',
    icon: 'info-circle',
    component: LoadableComponent(() => import('../../quanlilophoc/GiaoVienList')),
    showInMenu: true,
  },
  {
    path: '/quanlilophoc/lopyeucau',
    name: 'Lớp yêu cầu',
    title: 'Lớp yêu cầu',
    icon: 'info-circle',
    component: LoadableComponent(() => import('../../quanlilophoc/LopYeuCau')),
    showInMenu: true,
  },
  {
    path: '/quanlilophoc/lopgiasu',
    name: 'Lớp gia sư',
    title: 'Lớp gia sư',
    icon: 'info-circle',
    component: LoadableComponent(() => import('../../quanlilophoc/LopGiaSu')),
    showInMenu: true,
  },
  {
    path: '/quanlilophoc/lopnhom',
    name: 'Lớp nhóm',
    title: 'Lớp nhóm',
    icon: 'info-circle',
    component: LoadableComponent(() => import('../../quanlilophoc/LopNhom')),
    showInMenu: true,
  },
  {
    path: '/quanlilophoc/loplon',
    name: 'Lớp tập trung',
    title: 'Lớp tập trung',
    icon: 'info-circle',
    component: LoadableComponent(() => import('../../quanlilophoc/LopLon')),
    showInMenu: true,
  },
];
export const giaovienRouter: any = [

];

export const routers = [...userRouter, ...appRouters,...quanlilophocRouter,...giaovienRouter];
