import React, { Component } from 'react';
import { Select } from 'antd';
import commonService from '../../services/commonCombo/commonComboService';

interface ISelectProp {
  value?: any[];
  loai?:any;
  onChange?: (args: any) => void;

}

class StatusSelect extends Component<ISelectProp> {
  state = {
    data: [],
  };

  async loadData(checkRestValue:boolean=true) {
    const data = await commonService.getStatus(this.props.loai);
    this.setState({
      data,
    });
  }

  async componentDidMount() {
    this.loadData(false)
  };
  onChangeValue = (value: any) => {
    const { onChange } = this.props;
    if (onChange)
      onChange(value);
  };
  render() {
    const { value } = this.props;
    const options = this.state.data.map((item: any, index:any) =>
      <Select.Option key={item} value={item.value}>{item.displayText}</Select.Option>,
    );
    return (
      <Select style={{ width: '100%' }}
              onChange={(value: any) => this.onChangeValue(value)}
              value={value||''}
              showSearch
              filterOption={(input, option:any) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
      >
        <Select.Option key='all' value=''>Select...</Select.Option>
        {options}
      </Select>
    );
  }
}

export default StatusSelect;
