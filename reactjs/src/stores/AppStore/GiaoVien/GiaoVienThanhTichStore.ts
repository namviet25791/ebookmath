import {action, observable} from "mobx";
import GiaoVienService from "../../../services/AppCore/GiaoVien.service";

class GiaoVienThanhTich{
    @observable isLoading: boolean = false;
    @observable Data: any=[];
    @observable DataItem: any;
    @observable giaoVienId: any;
    @observable isShow: boolean = false;
    @observable isRender: boolean = false;
    @action
    async getAllServerPaging() {
        this.isLoading = true;
        this.Data = await GiaoVienService.getTT(this.giaoVienId);
        this.isLoading = false;
    }
    @action
    async showModal(item?: any) {
        if(item) {
            this.DataItem=item;
        }
        else {this.DataItem={}}
        this.isRender = true;
        this.isShow = true;
    }
    @action
    closeModal() {
        this.isShow = false;
        setTimeout(() => {
            this.isRender = false;
        }, 300);
    }

    @action
    async CreatUpdateData(formVals: any) {
        this.DataItem={...this.DataItem,...formVals,giaovienId:this.giaoVienId};
        if(!this.DataItem.id) {
            const list = [this.DataItem];
            await GiaoVienService.createListTT(list);
        }
        else {
            await GiaoVienService.updateTT(this.DataItem);
        }
        this.closeModal();
        this.getAllServerPaging();
    }
    @action
    async delete(id: any) {
        GiaoVienService.deleteQK(id);
    }
}
export default GiaoVienThanhTich;
