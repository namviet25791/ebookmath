import { observable, action } from 'mobx';
import GiaoVienService from '../../../services/AppCore/GiaoVien.service';

const prefixImg = 'data:image/jpeg;base64,';

export enum MenuType {
  information = 'information',
  thanhtich = 'thanhtich',
  bangcap = 'bangcap',
  quakhu = 'quakhu',
  otherinformation = 'otherinformation',
  comment = 'comment',
  guestidentification = 'guestidentification',
  bir = 'bir',
  cashcarh = 'cashcarh',
  accountsetting = 'accountsetting',
}




class GiaoVienEditUpdateStoreStore {
  @observable isShow: boolean = false;
  @observable isRender: boolean = false;
  @observable view: boolean = false;
  @observable item: any;
  @observable infoData: any = {};
  @observable menuType?: MenuType;

  @action
  changeStatusSelectKey = async (key: MenuType) => {
    this.menuType = key;
  };
  @action
  async showModal(id?: any) {
    if(id!=0) {
      const data = await GiaoVienService.getInfo(id);
      this.infoData = data ? {
        ...data,
        avatar: data.avatar ? prefixImg + data.avatar : '',
      } : {};
    }
    else {this.infoData={id:0}}
    this.isRender = true;
    this.isShow = true;
    console.log(this.infoData)
  }

  @action
  closeModal() {
    this.isShow = false;
    setTimeout(() => {
      this.isRender = false;
    }, 300);
  }

  @action
  async CreatUpdateAvatar(formVals: any) {
    formVals.avatar = formVals.avatar;
    this.infoData={...this.infoData,...formVals}
    await GiaoVienService.createUpdateGV(this.infoData);
  }
  @action
  async CreatUpdateData(formVals: any) {
    formVals.photoBang = formVals.photoBang?formVals.photoBang.split(';base64,')[1] : '';
    this.infoData={...this.infoData,...formVals}
    await GiaoVienService.createUpdateGV(this.infoData);
  }
}


export default GiaoVienEditUpdateStoreStore;
