import {action, observable} from "mobx";
import GiaoVienService from "../../../services/AppCore/GiaoVien.service";

class GiaoVienQuaKhu{
    @observable isLoading: boolean = false;
    @observable Data: any=[];
    @observable DataItem: any;
    @observable giaoVienId: any;
    @observable isShow: boolean = false;
    @observable isRender: boolean = false;
    @observable truonghocs: any = [];

    constructor() {
        this.getAllServerPaging();
    }

    @action
    async getAllServerPaging() {
        this.isLoading = true;
        this.Data = await GiaoVienService.getQK(this.giaoVienId);
        this.isLoading = false;
    }
    @action
    async showModal(item?: any) {
        if(item) {
            this.DataItem=item;
        }
        else {this.DataItem={}}
        this.isRender = true;
        this.isShow = true;
    }
    @action
    closeModal() {
        this.isShow = false;
        setTimeout(() => {
            this.isRender = false;
        }, 300);
    }

    @action
    async CreatUpdateData(formVals: any) {
        this.DataItem={...this.DataItem,...formVals,giaovienId:this.giaoVienId};
        if(!this.DataItem.id) {
            const list = [this.DataItem];
            await GiaoVienService.createListQK(list);
        }
        else {
            await GiaoVienService.updateQK(this.DataItem);
        }
    }
    @action
    async delete(id: any) {
        GiaoVienService.deleteQK(id);
    }
}
export default GiaoVienQuaKhu;
