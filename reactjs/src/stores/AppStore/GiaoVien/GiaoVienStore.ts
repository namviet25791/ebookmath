import { observable, action } from 'mobx';
import { PagedResultDto } from '../../../services/dto/pagedResultDto';
import GiaoVienService from '../../../services/AppCore/GiaoVien.service';

class GiaoVienStore {
  @observable isLoading: boolean = false;
  @observable dataPaging!: PagedResultDto<any>;
  @observable currentPage: number = 1;
  @observable filter: any = {};
  constructor() {
    this.restartPaging();
  }

  @action
  async getAllServerPaging(input: any = this.filter) {
    this.isLoading = true;
    this.dataPaging = await GiaoVienService.getAllServerPaging(input);
    if (this.dataPaging)
      this.isLoading = false;
  }

  @action
  async delete(id: any) {
    await GiaoVienService.delete(id);
  }

  @action
  setFilterValue(value: any) {
    this.filter = value;
  }

  @action
  restartPaging() {
    this.currentPage = 1;
    this.filter.skipCount = 0;
    this.filter.maxResultCount = 10;
  }
}

export default GiaoVienStore;
