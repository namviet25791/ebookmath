import {action, observable} from "mobx";
import GiaoVienService from "../../../services/AppCore/GiaoVien.service";
import commonComboService from "../../../services/commonCombo/commonComboService";

class GiaoVienBangCap{
    @observable isLoading: boolean = false;
    @observable Data: any=[];
    @observable DataItem: any;
    @observable giaoVienId: any;
    @observable isShow: boolean = false;
    @observable isRender: boolean = false;
    @observable truonghocs: any = [];

    constructor() {
        this.getAllServerPaging();
    }

    @action
    async getAllServerPaging() {
        this.isLoading = true;
        this.Data = await GiaoVienService.getBC(this.giaoVienId);
        this.truonghocs = await commonComboService.getTruong();
        this.isLoading = false;
    }
    @action
    async showModal(item?: any) {
        if(item) {
            this.DataItem=item;
        }
        else {this.DataItem={}}
        this.isRender = true;
        this.isShow = true;
    }
    @action
    closeModal() {
        this.isShow = false;
        setTimeout(() => {
            this.isRender = false;
        }, 300);
    }

    @action
    async CreatUpdateData(formVals: any) {
        this.DataItem={...this.DataItem,...formVals,giaovienId:this.giaoVienId};
        if(!this.DataItem.id) {
            const list = [this.DataItem];
            await GiaoVienService.createListBC(list);
        }
        else{
            await GiaoVienService.updateBC(this.DataItem);
        }
    }
    @action
    async delete(id: any) {
        GiaoVienService.deleteTT(id);
    }
}
export default GiaoVienBangCap;
