import { observable, action } from 'mobx';
import LopGiaSuService from '../../../services/AppCore/LopGiaSu.service';


class ModalCreateOrUpdateDto {
  @observable isShow: boolean = false;
  @observable isRender: boolean = false;
  @observable dataModal: any = {};

  @action
  async showModal(item?: any) {
    if(item) {
      const data = await LopGiaSuService.getById(item.id);
      const toado=item.toaDoX+','+item.toaDoY;
      this.dataModal = {...data,toado:toado};
    }
    else {this.dataModal={}}
    this.isRender = true;
    this.isShow = true;
  }

  @action
  closeModal() {
    this.isShow = false;
    setTimeout(() => {
      this.isRender = false;
    }, 300);
  }

  @action
  async updateData(formVals: any) {
    const item = formVals.toado.split(',');
    formVals.toaDoX=item[0];
    formVals.toaDoY=item[1];
    this.dataModal={...this.dataModal,...formVals}
    await LopGiaSuService.createUpdate(this.dataModal);
  }
}


class LopGiaSuStore {
  @observable isLoading: boolean = false;
  @observable dataPaging!: any;
  @observable currentPage: number = 1;
  @observable filter: any = {};
  @observable modalCreateOrUpdate: ModalCreateOrUpdateDto = new ModalCreateOrUpdateDto();


  constructor() {
    this.restartPaging();
  }

  @action
  async getAllServerPaging(input: any = this.filter) {
    this.isLoading = true;
    this.dataPaging = await LopGiaSuService.getAllServerPaging(input);
    if (this.dataPaging)
      this.isLoading = false;
  }

  @action
  async delete(id: any) {
    await LopGiaSuService.delete(id);
  }

  @action
  setFilterValue(value: any) {
    this.filter = value;

  }

  @action
  restartPaging() {
    this.currentPage = 1;
    this.filter.skipCount = 0;
    this.filter.maxResultCount = 10;
  }
}

export default LopGiaSuStore;
