import RoleStore from './roleStore';
import TenantStore from './tenantStore';
import UserStore from './userStore';
import SessionStore from './sessionStore';
import AuthenticationStore from './authenticationStore';
import AccountStore from './accountStore';
import GiaoVienStore from './AppStore/GiaoVien/GiaoVienStore';
import GiaoVienEditUpdateStoreStore from './AppStore/GiaoVien/GiaoVienEditUpdateStore';
import GiaoVienThanhTich from './AppStore/GiaoVien/GiaoVienThanhTichStore';
import GiaoVienQuaKhu from './AppStore/GiaoVien/GiaoVienQuaKhuStore';
import GiaoVienBangCap from "./AppStore/GiaoVien/GiaoVienBangCapStore";
import GiaoVienKyNang from './AppStore/GiaoVien/GiaoVienKyNangStore';
import LopYeuCauStore from "./AppStore/LopHoc/lopYeuCauStore";
import LopGiaSuStore from "./AppStore/LopHoc/lopGiaSuStore";
import LopNhomStore from './AppStore/LopHoc/lopNhomStore';
import LopLonStore from './AppStore/LopHoc/lopLonStore';
import BaiVietStore from './AppStore/BaiViet/BaiVietStore';

export default function initializeStores() {
  return {
    authenticationStore: new AuthenticationStore(),
    roleStore: new RoleStore(),
    tenantStore: new TenantStore(),
    userStore: new UserStore(),
    sessionStore: new SessionStore(),
    accountStore: new AccountStore(),
    giaoVienStore: new GiaoVienStore(),
    giaoVienEditUpdateStoreStore: new GiaoVienEditUpdateStoreStore(),
    giaoVienThanhTich: new GiaoVienThanhTich(),
    giaoVienQuaKhu: new GiaoVienQuaKhu(),
    giaoVienBangCap: new GiaoVienBangCap(),
    giaoVienKyNang: new GiaoVienKyNang(),
    lopYeuCauStore: new LopYeuCauStore(),
    lopGiaSuStore: new LopGiaSuStore(),
    lopNhomStore: new LopNhomStore(),
    lopLonStore: new LopLonStore(),
    baiVietStore: new BaiVietStore(),
  };
}
