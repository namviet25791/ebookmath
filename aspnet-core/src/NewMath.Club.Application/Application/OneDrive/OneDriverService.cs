﻿using System.Threading.Tasks;
using System.Collections.Generic;
using newPSG.PMS.Helper;
using KoenZomers.OneDrive.Api;
using Microsoft.Extensions.Configuration;
using System.IO;
using NewMath.Club.Configuration;
using KoenZomers.OneDrive.Api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace NewMath.Club.GiaoVienStore
{
    public class OneDriveService : ClubAppServiceBase/*, ILopHocService*/
    {
        private readonly IConfigurationRoot _appConfiguration;
        private readonly IApplicationServiceFactory _factory;
        private readonly OneDriveApi OneDriveApi;
        public OneDriveService(IApplicationServiceFactory factory)
        {
            _factory = factory;
            _appConfiguration = AppConfigurations.Get(Directory.GetCurrentDirectory());
            OneDriveApi = new OneDriveGraphApi(_appConfiguration.GetValue<string>("TokenOnedrive:AppId"));
            OneDriveApi.AuthenticateUsingRefreshToken(_appConfiguration.GetValue<string>("TokenOnedrive:Token"));
        }
        [HttpPost]
        public async Task Upload(List<IFormFile> files, List<string> listName)
        {
            long size = files.Sum(f => f.Length);
            // full path to file in temp location
            var filePath = Path.GetTempFileName();
            int i = 0;
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                    OneDriveItem item = new OneDriveItem();
                    item = await OneDriveApi.UploadFileViaResumableUpload(filePath, formFile.FileName, item);
                }
                i++;
            }
        }
        //Server get GiaoVienInformation
    }
}
