﻿namespace NewMath.Club.TeacherStore.Dto
{
    public class FileThongTin
    {
        public virtual string PathPdf { get; set; }
        public virtual string PathWord { get; set; }
        public virtual int? NumberPage { get; set; }
    }
}
