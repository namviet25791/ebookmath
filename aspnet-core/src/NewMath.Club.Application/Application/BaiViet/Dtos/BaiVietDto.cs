﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    [AutoMap(typeof(BaiViet))]
    public class BaiVietDto : EntityDto<int>
    {
        public virtual string TieuDe { get; set; }
        public virtual string TomTat { get; set; }
        public virtual string NoiDung { get; set; }
        public virtual string KetLuan { get; set; }
        public virtual int BaiThu { get; set; }
        public virtual DateTime NgayViet { get; set; }
    }
   
    [AutoMap(typeof(BoSuuTapBaiViet))]
    public class BoSuuTapBaiVietDto : EntityDto<int>
    {
        public virtual int? ChuDeId { get; set; }
        public virtual string Ten { get; set; }
        public virtual string TomTat { get; set; }
    }
    [AutoMap(typeof(ChuDeBaiViet))]
    public class ChuDeBaiVietDto : EntityDto<int>
    {
        public virtual string Code { get; set; }
        public virtual string Ten { get; set; }
        public virtual string MoTa { get; set; }
    }

    public class BaiVietInput: PagedAndSortedResultRequestDto
    {
        public virtual int? ChuDeId { get; set; }
        public virtual int? BoSuuTapId { get; set; }
    }
    public class BaiVietView:EntityDto<long>
    {
        public virtual string TomTat { get; set; }
        public virtual string NguoiViet { get; set; }
        public virtual string BoSuuTap { get; set; }
        public virtual string ChuDe { get; set; }
        public virtual DateTime NgayViet { get; set; }
        public virtual int BaiThu { get; set; }
    }
}
