﻿using Abp.Application.Services.Dto;
using Dapper;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using newPSG.PMS.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewMath.Club.GiaoVienStore
{
    public class BaiVietService: ClubAppServiceBase
    {
        private readonly IApplicationServiceFactory _factory;

        public BaiVietService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        public async Task<ListResultDto<BaiVietView>> GetPage(BaiVietInput input)
        {
            var sql = $@"select b.Id,b.TomTat,b.NgayViet,b.BaiThu,bs.Ten BoSuuTap,cd.Ten ChuDe,u.Name NguoiViet
                            from dbbaiviet b
                            join abpusers u on b.UserId=u.Id
		                    join dbbosuutapbaiviet bs on b.BoSuuTapId=bs.Id
		                    join dbchude_baiviet cd on bs.ChuDeId=cd.Id
                        ";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<string> list = new List<string>();
            int i = 1;
            if (input.ChuDeId.HasValue) { list.Add("bs.Id = @" + i); dic.Add("@" + i, input.ChuDeId); i++; }
            if (input.BoSuuTapId.HasValue) { list.Add("cd.Id = @" + i); dic.Add("@" + i, input.BoSuuTapId); i++; }
            if (list.Count > 0)
            {
                sql += " WHERE " + string.Join(" AND ", list.ToArray());
            }
            var parameters = new DynamicParameters(dic);
            var sqlCount = "select count(1) total from(" + sql + ")A";
            var sorting = input.Sorting ?? "Id";
            var sqlItems = sql + " order by " + sorting + " limit " + input.MaxResultCount + " OFFSET " + input.SkipCount;
            var Counts = await _factory.UnitOfWordDapper.QueryAsync<int>(sqlCount, parameters);
            var total = Counts.ElementAt(0);
            var Items = await _factory.UnitOfWordDapper.QueryAsync<BaiVietView>(sqlItems, parameters);
            return new PagedResultDto<BaiVietView>(total, Items.ToList());
        }
        public async Task<string> GetThongTin(int Id)
        {
            var _Repos = _factory.Repository<BaiViet, int>();
            var filter = await _Repos.FirstOrDefaultAsync(e => e.Id == Id);
            return filter.NoiDung;
        }
        public async Task CreatUpdate(BaiVietDto input)
        {
            input.NgayViet=DateTime.Now;
            var _Repos = _factory.Repository<BaiViet, int>();
            await _Repos.InsertOrUpdateAsync(ObjectMapper.Map<BaiViet>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task Delete(int Id)
        {
            var _Repos = _factory.Repository<GiaoVien, int>();
            await _Repos.DeleteAsync(e => e.Id == Id);
            await CurrentUnitOfWork.SaveChangesAsync();
        }

        public async Task CreatUpdateBoSuuTap(ChuDeBaiVietDto input)
        {
            var _Repos = _factory.Repository<ChuDeBaiViet, int>();
            await _Repos.InsertOrUpdateAsync(ObjectMapper.Map<ChuDeBaiViet>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        //Server get GiaoVienInformation
    }
}
