﻿using Microsoft.Office.Interop.Word;
using NewMath.Club.TeacherStore.Dto;

namespace NewMath.Club.GiaoVienStore
{
    public class OfficeService
    {
        private Application _app;
        public OfficeService(Application app)
        {
            _app = app;
        }
        public FileThongTin WordToPdf(string path)
        {
            Document doc = _app.Documents.Open(FileName:path,ReadOnly:true);
            var numberOfPages = doc.ComputeStatistics(WdStatistic.wdStatisticPages, false);
            doc.SaveAs2(path.Replace(".doc",".pdf").Replace(".docx",".pdf"), WdSaveFormat.wdFormatPDF);
            return new FileThongTin
            {
                NumberPage = numberOfPages,
                PathPdf = path.Replace(".doc", ".pdf").Replace(".docx", ".pdf"),
                PathWord = path
            };
        }
        //Server get GiaoVienInformation
    }
}
