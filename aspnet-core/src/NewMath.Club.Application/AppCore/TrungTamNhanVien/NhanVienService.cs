﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using NewMath.Club.Authorization.Users;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using Abp.Application.Services;
using newPSG.PMS.Helper;
using Dapper;
using System;

namespace NewMath.Club.GiaoVienStore
{
    public class NhanVienService : ClubAppServiceBase, INhanVienService
    {
        private readonly IApplicationServiceFactory _factory;

        public NhanVienService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        public async Task<ListResultDto<NhanVienView>> GetPage(NhanVienInput input)
        {
            var sql = $@"select u.Id UserId,
		                        g.Ten,g.Id,g.Avatar,g.CongViecId,g.CodeId,g.Avatar,g.DiaChi,g.Email,g.Tuoi,g.GioiTinh,g.NgaySinh,g.Luong,g.DiaChi,
                                t.Mota Truong,
                                s.MoTa TrangThai 
                                from dbnhanvien g Join abpusers u on g.UserId=u.Id
                                join dbtruong t on g.TruongId=t.Id
                                join dbstatus s on s.Id=g.StatusId
                        ";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<string> list = new List<string>();
            int i = 1;
            if (!string.IsNullOrEmpty(input.Ten)) { list.Add("g.Ten LIKE @" + i); dic.Add(@"@" + i, "%" + input.Ten + "%"); i++; }
            if (input.Tuoi_Start != null) { list.Add("g.Tuoi > @" + i); dic.Add("@" + i, input.Tuoi_Start); i++; }
            if (input.Tuoi_End != null) { list.Add("g.Tuoi < @" + i); dic.Add("@" + i, input.Tuoi_End); i++; }
            if (input.SchoolId != null) { list.Add("g.TruongId = @" + i); dic.Add("@" + i, input.SchoolId); i++; }
            if (input.CongViecId != null) { list.Add("g.CongViecId = @" + i); dic.Add("@" + i, input.CongViecId); i++; }
            if (input.GioiTinh != null) { list.Add("g.GioiTinh = @" + i); dic.Add("@" + i, input.GioiTinh); i++; }
            if (input.StatusId != null) { list.Add("g.StatusId = @" + i); dic.Add("@" + i, input.StatusId); i++; }
            if (list.Count > 0)
            {
                sql += " WHERE " + string.Join(" AND ", list.ToArray());
            }
            var parameters = new DynamicParameters(dic);
            var sqlCount = "select count(1) total from(" + sql + ")A";
            var sorting = input.Sorting ?? "Id";
            var sqlItems = sql + " order by " + sorting + " limit " + input.MaxResultCount + " OFFSET " + input.SkipCount;
            var Counts = await _factory.UnitOfWordDapper.QueryAsync<int>(sqlCount, parameters);
            var total = Counts.ElementAt(0);
            var Items = await _factory.UnitOfWordDapper.QueryAsync<NhanVienView>(sqlItems, parameters);
            return new PagedResultDto<NhanVienView>(total, Items.ToList());
        }
        public async Task<NhanVien> GetById(int Id)
        {
            var _Repos = _factory.Repository<NhanVien, int>();
            var filter = _Repos.FirstOrDefaultAsync(e => e.Id == Id);
            return await filter;
        }
        public async Task CreatUpdate(AppDto input)
        {
            input.Tuoi = DateTime.Now.Year - input.NgaySinh.Value.Year + 1;
            var _GiaoVien = _factory.Repository<NhanVien, int>();
            await _GiaoVien.InsertOrUpdateAsync(ObjectMapper.Map<NhanVien>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task Delete(int Id)
        {
            var _Repos = _factory.Repository<NhanVien, int>();
            await _Repos.DeleteAsync(e => e.Id == Id);
        }
        //Server get GiaoVienInformation
    }
}
