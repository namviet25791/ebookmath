﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using NewMath.Club.Authorization.Users;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using Abp.Application.Services;
using newPSG.PMS.Helper;
using Dapper;
using System;

namespace NewMath.Club.GiaoVienStore
{
    #region Interface
    public interface INhanVienService : IApplicationService
    {
        Task<ListResultDto<NhanVienView>> GetPage(NhanVienInput input);
        Task<NhanVien> GetById(int Id);
        Task CreatUpdate(AppDto input);
        Task Delete(int Id);

    }
    #endregion
        //Server get GiaoVienInformation
}
