﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    public class TrungTamCuocGapInput : PagedAndSortedResultRequestDto
    {
        public virtual int? StatusId { get; set; }
    }
    [AutoMap(typeof(TrungTamCuocGap))]
    public class TrungTamCuocGapDto : EntityDto<int>
    {
        public virtual int? GiaoVienId { get; set; }
        public virtual int? NhanVienId { get; set; }
        public virtual int? PhuHuynhId { get; set; }
        public virtual int? HocSinhId { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int Loai_DiaChiId { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual DateTime? ThoiGian_KeHoach { get; set; }
        public virtual DateTime? ThoiGian_Gap { get; set; }
        public virtual string Mota { get; set; }
        public virtual bool? ResultStatusId { get; set; }
        public virtual string MoTaKetQua { get; set; }
    }
    public class TrungTamCuocGapView : EntityDto<int>
    {
        public virtual string GiaoVien { get; set; }
        public virtual string NhanVien { get; set; }
        public virtual string PhuHuynh { get; set; }
        public virtual string HocSinh { get; set; }
        public virtual string TrangThai { get; set; }
        public virtual bool? ResultStatusId { get; set; }
        public virtual int Loai_DiaChiId { get; set; }
        public virtual DateTime? ThoiGian_KeHoach { get; set; }
        public virtual DateTime? ThoiGian_Gap { get; set; }
        public virtual string MoTa { get; set; }
        public virtual string MoTaKetQua { get; set; }
    }
}
