﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    public class TrungTamHoiThoaiInput : PagedAndSortedResultRequestDto
    {
        public virtual int? StatusId { get; set; }
    }
    [AutoMap(typeof(TrungTamHoiThoai))]
    public class TrungTamHoiThoainDto : EntityDto<int>
    {
        public virtual string CodeId { get; set; }
        public virtual int GiaoVienId { get; set; }
        public virtual int NhanVienId { get; set; }
        public virtual int? PHuHuynhId { get; set; }
        public virtual int? HocSinhId { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual bool? ResultStatusId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoIGian_Ent { get; set; }
        public virtual string MoTa { get; set; }
        public virtual string MoTaKetQua { get; set; }
        public virtual string CodeZalo { get; set; }
    }
    public class TrungTamHoiThoaiView : EntityDto<int>
    {
        public virtual string CodeId { get; set; }
        public virtual string GiaoVien { get; set; }
        public virtual string NhanVien { get; set; }
        public virtual string PhuHuynh { get; set; }
        public virtual string HocSinh { get; set; }
        public virtual string TrangThai { get; set; }
        public virtual bool? ResultStatusId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoIGian_Ent { get; set; }
        public virtual string MoTa { get; set; }
        public virtual string MoTaKetQua { get; set; }
        public virtual string CodeZalo { get; set; }
    }
}
