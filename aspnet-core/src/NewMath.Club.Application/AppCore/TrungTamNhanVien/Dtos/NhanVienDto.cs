﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    public class NhanVienInput : PagedAndSortedResultRequestDto
    {
        public virtual string Ten { get; set; }
        public virtual int? Tuoi_Start { get; set; }
        public virtual int? Tuoi_End { get; set; }
        public virtual int? SchoolId { get; set; }
        public virtual int? CongViecId { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual int? StatusId { get; set; }
    }
    [AutoMap(typeof(NhanVien))]
    public class AppDto : EntityDto<int>
    {
        public virtual string CodeId { get; set; }
        public virtual int CongViecId { get; set; }
        public virtual string Ten { get; set; }
        public virtual int UserId { get; set; }
        public virtual int? TruongId { get; set; }
        public virtual byte[] Avatar { get; set; }
        public virtual string Email { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual DateTime? NgaySinh { get; set; }
        public virtual int? Tuoi { get; set; }
        public virtual string TinhCach { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual int? Luong { get; set; }
        public virtual int StatusId { get; set; }
        public virtual string Mota { get; set; }
        public virtual string DiaChi { get; set; }
    }
    public class NhanVienView : EntityDto<int>
    {
        public virtual int UserId { get; set; }
        public virtual string Ten { get; set; }
        public virtual string CodeId { get; set; }
        public virtual int? CongViecId { get; set; }
        public virtual string Truong { get; set; }
        public virtual byte[] Avatar { get; set; }
        public virtual string Email { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual DateTime? NgaySinh { get; set; }
        public virtual int? Tuoi { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual int? Luong { get; set; }
        public virtual string TrangThai { get; set; }
        public virtual string DiaChi { get; set; }
    }
}
