﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using NewMath.Club.Authorization.Users;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using Abp.Application.Services;
using newPSG.PMS.Helper;
using Dapper;
using System;

namespace NewMath.Club.GiaoVienStore
{
    public class TrungTamCuocGapService : ClubAppServiceBase, ITrungTamCuocGapService
    {
        private readonly IApplicationServiceFactory _factory;

        public TrungTamCuocGapService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        public async Task<ListResultDto<TrungTamCuocGapView>> GetPage(TrungTamCuocGapInput input)
        {
            var sql = $@"select t.ResultStatusId,t.ThoiGian_KeHoach,t.ThoiGian_Gap,t.MoTa,t.MoTaKetQua,t.Loai_DiaChiId,
                                g.Ten,n.Ten,p.Ten,h.Ten,
                                s.MoTa TrangThai 
                                from dbtrungtam_cuochen t
                                Join dbgiaovien g on g.Id=t.GiaoVienId
                                join dbnhanvien n on n.Id=t.NhanVienId
                                left join dbphuhuynh p on p.Id=t.PHuHuynhId
                                left join dbhocsinh h on h.Id=t.HocSinhId
                                join dbstatus s on s.Id=g.StatusId
                        ";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<string> list = new List<string>();
            int i = 1;
            if (input.StatusId != null) { list.Add("g.StatusId = @" + i); dic.Add("@" + i, input.StatusId); i++; }
            if (list.Count > 0)
            {
                sql += " WHERE " + string.Join(" AND ", list.ToArray());
            }
            var parameters = new DynamicParameters(dic);
            var sqlCount = "select count(1) total from(" + sql + ")A";
            var sorting = input.Sorting ?? "Id";
            var sqlItems = sql + " order by " + sorting + " limit " + input.MaxResultCount + " OFFSET " + input.SkipCount;
            var Counts = await _factory.UnitOfWordDapper.QueryAsync<int>(sqlCount, parameters);
            var total = Counts.ElementAt(0);
            var Items = await _factory.UnitOfWordDapper.QueryAsync<TrungTamCuocGapView>(sqlItems, parameters);
            return new PagedResultDto<TrungTamCuocGapView>(total, Items.ToList());
        }
        public async Task<TrungTamCuocGap> GetById(int Id)
        {
            var _Repos = _factory.Repository<TrungTamCuocGap, int>();
            var filter = _Repos.FirstOrDefaultAsync(e => e.Id == Id);
            return await filter;
        }
        public async Task CreatUpdate(TrungTamCuocGapDto input)
        {
            var _GiaoVien = _factory.Repository<TrungTamCuocGap, int>();
            await _GiaoVien.InsertOrUpdateAsync(ObjectMapper.Map<TrungTamCuocGap>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task Delete(int Id)
        {
            var _Repos = _factory.Repository<TrungTamCuocGap, int>();
            await _Repos.DeleteAsync(e => e.Id == Id);
        }
        //Server get GiaoVienInformation
    }
}
