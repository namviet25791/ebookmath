﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using newPSG.PMS.Helper;
using Dapper;
using System;

namespace NewMath.Club.GiaoVienStore
{
    public class LopHocService : ClubAppServiceBase, ILopHocService
    {
        private readonly IApplicationServiceFactory _factory;

        public LopHocService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        public async Task<ListResultDto<LopYeuCauView>> GetPage(LopYeuCauInput input)
        {
            var sql = $@"select l.Id,
                                l.CodeId,l.Gia_Start,l.Gia_End,l.ThoiGian_YeuCau,l.LopSo,
                                l.TenNguoiYeuCau,l.Address,l.Phone,l.GioiTinhNguoiYeuCau,l.SoLuongId,l.YeuCauChung,l.YeuCauGiaoVien,l.MoTaHocSinh,l.GioiTinhHocSinh,l.HocLucHocSinh,l.TruongHocSinh,l.DanhGiaNguoiYeuCau,l.DanhSachGiaoVienId,l.TenHocSinh,
                                u.Name,
                                m.MoTa Monhoc, ll.MoTa LoaiLop,
                                s.Id StatusId,s.MoTa TrangThai,
                                a.ToaDoX,a.ToaDoY
                                from dblop_yeucau_chuahoatdong l
                                join dbmonhoc m on m.Id=l.MonHocId
                                join dbstatus s on s.Id=l.StatusId
                                left join abpusers u on l.UserId=u.Id
                                left join dbloailop ll on ll.Id=l.LoaiLopId
                                left join dbdiachi a on a.Id=l.AddressId
                        ";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<string> list = new List<string>();
            int i = 1;
            if (!string.IsNullOrEmpty(input.FilterCode)) { list.Add("l.CodeId LIKE @" + i); dic.Add(@"@" + i, "%" + input.FilterCode + "%"); i++; }
            if (!string.IsNullOrEmpty(input.FilterDiaChi)) { list.Add("l.Address LIKE @" + i); dic.Add(@"@" + i, "%" + input.FilterDiaChi + "%"); i++; }
            if (input.LoaiLopId != null) { list.Add("l.LoaiLopId = @" + i); dic.Add("@" + i, input.LoaiLopId); i++; }
            if (input.StatusId != null) { list.Add("l.StatusId= @" + i); dic.Add("@" + i, input.StatusId); i++; }
            if (list.Count > 0)
            {
                sql += " WHERE " + string.Join(" AND ", list.ToArray());
            }
            var parameters = new DynamicParameters(dic);
            var sqlCount = "select count(1) total from(" + sql + ")A";
            var sorting = "l.ThoiGian_YeuCau";
            var sqlItems = sql + " order by " + sorting + " limit " + input.MaxResultCount + " OFFSET " + input.SkipCount;
            var Counts = await _factory.UnitOfWordDapper.QueryAsync<int>(sqlCount, parameters);
            var total = Counts.ElementAt(0);
            var Items = await _factory.UnitOfWordDapper.QueryAsync<LopYeuCauView>(sqlItems, parameters);
            return new PagedResultDto<LopYeuCauView>(total, Items.ToList());
        }
        public async Task<LopYeuCau> GetLopYeuCau(int Id)
        {
            var _Repos = _factory.Repository<LopYeuCau, int>();
            var filter = _Repos.FirstOrDefaultAsync(e => e.Id == Id);
            return await filter;
        }
        public async Task CreatLopYeuCau(LopYeuCauCreatInput input)
        {
            if (input.Id == 0)
            {
                input.ThoiGian_YeuCau = DateTime.Now;
            }
            var _Repos = _factory.Repository<LopYeuCau, int>();
            if (!input.AddressId.HasValue)
            {
                input.AddressId = 0;
            }
            var _DiaChiRepos = _factory.Repository<DiaChi, int>();
            var DiaChiItem = new DiaChi
            {
                Id = input.AddressId.Value,
                ToaDoX = input.ToaDoX,
                ToaDoY = input.ToaDoY,
            };
            var AddressId =await _DiaChiRepos.InsertOrUpdateAndGetIdAsync(DiaChiItem);
            input.AddressId = AddressId;
            await _Repos.InsertOrUpdateAsync(ObjectMapper.Map<LopYeuCau>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public void DeleteLopYeuCau(int Id)
        {
            var _Repos = _factory.Repository<LopYeuCau, int>();
            _Repos.Delete(x => x.Id == Id);
        }
        //Server get GiaoVienInformation
    }
}
