﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using NewMath.Club.Authorization.Users;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using Abp.Application.Services;
using newPSG.PMS.Helper;
using Dapper;
using System;

namespace NewMath.Club.GiaoVienStore
{
    public class LopHoatDongNhomService : ClubAppServiceBase, ILopHoatDongNhomService
    {
        private readonly IApplicationServiceFactory _factory;

        public LopHoatDongNhomService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        public async Task<ListResultDto<LopHoatDongNhomView>> GetPage(LopHoatDongNhomInput input)
        {
            var sql = $@"select l.Id,l.Ten,
                                l.CodeId,l.Thoigian_Start,l.Thoigian_End,l.ThoiGian_BatDau,l.DiaChi,l.LopSo,l.Ngay,l.Gia,l.SoLuong,l.MoTa,l.DiaChiId,
                                m.MoTa Monhoc, ll.MoTa LoaiLop,
                                s.Id StatusId,s.MoTa TrangThai,
                                a.ToaDoX,a.ToaDoY,
                                g.Ten Giaovien
                                from dblop_hoatdong_Nhom l
                                join dbmonhoc m on m.Id=l.MonHocId
                                join dbstatus s on s.Id=l.StatusId
                                join dbgiaovien g on g.Id=l.GiaovienId
                                left join dbloailop ll on ll.Id=l.LoaiLopId
                                left join dbdiachi a on a.Id=l.DiaChiId
                        ";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<string> list = new List<string>();
            int i = 1;
            if (!string.IsNullOrEmpty(input.FilterCode)) { list.Add("l.CodeId LIKE @" + i); dic.Add(@"@" + i, "%" + input.FilterCode + "%"); i++; }
            if (!string.IsNullOrEmpty(input.FilterDiaChi)) { list.Add("l.Address LIKE @" + i); dic.Add(@"@" + i, "%" + input.FilterDiaChi + "%"); i++; }
            if (input.LoaiLopId != null) { list.Add("l.LoaiLopId = @" + i); dic.Add("@" + i, input.LoaiLopId); i++; }
            if (input.StatusId != null) { list.Add("l.StatusId= @" + i); dic.Add("@" + i, input.StatusId); i++; }
            if (input.BatDau_Min != null) { list.Add("l.ThoiGian_BatDau>= @" + i); dic.Add("@" + i, input.BatDau_Min); i++; }
            if (input.BatDau_Max != null) { list.Add("l.ThoiGian_BatDau<= @" + i); dic.Add("@" + i, input.BatDau_Max); i++; }
            if (input.Ngay != null) { list.Add("l.Ngay= @" + i); dic.Add("@" + i, input.StatusId); i++; }
            if (list.Count > 0)
            {
                sql += " WHERE " + string.Join(" AND ", list.ToArray());
            }
            var parameters = new DynamicParameters(dic);
            var sqlCount = "select count(1) total from(" + sql + ")A";
            var sorting = "l.ThoiGian_BatDau";
            var sqlItems = sql + " order by " + sorting + " limit " + input.MaxResultCount + " OFFSET " + input.SkipCount;
            var Counts = await _factory.UnitOfWordDapper.QueryAsync<int>(sqlCount, parameters);
            var total = Counts.ElementAt(0);
            var Items = await _factory.UnitOfWordDapper.QueryAsync<LopHoatDongNhomView>(sqlItems, parameters);
            return new PagedResultDto<LopHoatDongNhomView>(total, Items.ToList());
        }
        public async Task<LopHoatDongNhom> GetLop(int Id)
        {

            var _Repos = _factory.Repository<LopHoatDongNhom, int>();
            var filter = _Repos.FirstOrDefaultAsync(e => e.Id == Id);
            return await filter;
        }
        public async Task CreatUpdate(LopHoatDongCreatNhom input)
        {

            if (input.Id == 0)
            {
                input.ThoiGian_BatDau = DateTime.Now;
            }
            var _Repos = _factory.Repository<LopHoatDongNhom, int>();
            if (!input.DiaChiId.HasValue)
            {
                input.DiaChiId = 0;
            }
            var _DiaChiRepos = _factory.Repository<DiaChi, int>();
            var DiaChiItem = new DiaChi
            {
                Id = input.DiaChiId.Value,
                ToaDoX = input.ToaDoX,
                ToaDoY = input.ToaDoY,
            };
            var AddressId = await _DiaChiRepos.InsertOrUpdateAndGetIdAsync(DiaChiItem);
            input.DiaChiId = AddressId;
            await _Repos.InsertOrUpdateAsync(ObjectMapper.Map<LopHoatDongNhom>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public void Delete(int Id)
        {
            var _Repos = _factory.Repository<LopHoatDongNhom, int>();
            _Repos.Delete(x => x.Id == Id);
        }
        //Server get GiaoVienInformation
    }
}
