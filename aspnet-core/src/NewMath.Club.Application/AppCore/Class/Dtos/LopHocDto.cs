﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    [AutoMap(typeof(LopYeuCau))]
    public class LopYeuCauDto : EntityDto<int>
    {
        public virtual string CodeId { get; set; }
        public virtual int? UserId { get; set; }
        //ThongTinLopYeCau
        public virtual int? LoaiLopId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual DateTime? ThoiGian_YeuCau { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? Gia_Start { get; set; }
        public virtual int? Gia_End { get; set; }
        public virtual int? AddressId { get; set; }
        //NguoiYeuCau
        public virtual string TenNguoiYeuCau { get; set; }
        public virtual string Address { get; set; }
        public virtual string Phone { get; set; }
        public virtual int GioiTinhNguoiYeuCau { get; set; }
        public virtual int? SoLuongId { get; set; }
        public virtual string YeuCauChung { get; set; }
        public virtual string YeuCauGiaoVien { get; set; }
        public virtual string MoTaHocSinh { get; set; }
        public virtual string TenHocSinh { get; set; }
        public virtual int GioiTinhHocSinh { get; set; }
        public virtual string HocLucHocSinh { get; set; }
        public virtual string TruongHocSinh { get; set; }
        //NhanVien
        public virtual string DanhGiaNguoiYeuCau { get; set; }
        public virtual string DanhSachGiaoVienId { get; set; }
    }
    [AutoMap(typeof(LopYeuCau))]
    public class LopYeuCauCreatInput : EntityDto<int>
    {
        public virtual string CodeId { get; set; }
        public virtual int? UserId { get; set; }
        //ThongTinLopYeCau
        public virtual int? LoaiLopId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual DateTime? ThoiGian_YeuCau { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? Gia_Start { get; set; }
        public virtual int? Gia_End { get; set; }
        public virtual int? AddressId { get; set; }
        public virtual decimal? ToaDoX { get; set; }
        public virtual decimal? ToaDoY { get; set; }
        //NguoiYeuCau
        public virtual string TenNguoiYeuCau { get; set; }
        public virtual string Address { get; set; }
        public virtual string Phone { get; set; }
        public virtual int GioiTinhNguoiYeuCau { get; set; }
        public virtual int? SoLuongId { get; set; }
        public virtual string YeuCauChung { get; set; }
        public virtual string YeuCauGiaoVien { get; set; }
        public virtual string MoTaHocSinh { get; set; }
        public virtual string TenHocSinh { get; set; }
        public virtual int GioiTinhHocSinh { get; set; }
        public virtual string HocLucHocSinh { get; set; }
        public virtual string TruongHocSinh { get; set; }
        //NhanVien
        public virtual string DanhGiaNguoiYeuCau { get; set; }
        public virtual string DanhSachGiaoVienId { get; set; }
    }
    public class LopYeuCauInput : PagedAndSortedResultRequestDto
    {
        public virtual string FilterCode { get; set; }
        public virtual string FilterDiaChi { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? StatusId { get; set; }
    }
    public class LopYeuCauView : EntityDto<int>
    {
        public virtual string CodeId { get; set; }
        public virtual string Name { get; set; }
        //ThongTinLopYeCau
        public virtual string LoaiLop { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual string MonHoc { get; set; }
        public virtual DateTime? ThoiGian_YeuCau { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? Gia_Start { get; set; }
        public virtual int? Gia_End { get; set; }
        public virtual int? AddressId { get; set; }
        public virtual decimal? ToaDoX { get; set; }
        public virtual decimal? ToaDoY { get; set; }
        //NguoiYeuCau
        public virtual string TenNguoiYeuCau { get; set; }
        public virtual string Address { get; set; }
        public virtual string Phone { get; set; }
        public virtual int GioiTinhNguoiYeuCau { get; set; }
        public virtual int? SoLuongId { get; set; }
        public virtual string YeuCauChung { get; set; }
        public virtual string YeuCauGiaoVien { get; set; }
        public virtual string TenHocSinh { get; set; }
        public virtual string MoTaHocSinh { get; set; }
        public virtual int GioiTinhHocSinh { get; set; }
        public virtual string HocLucHocSinh { get; set; }
        public virtual string TruongHocSinh { get; set; }
        //NhanVien
        public virtual string DanhGiaNguoiYeuCau { get; set; }
        public virtual string DanhSachGiaoVienId { get; set; }
    }
}
