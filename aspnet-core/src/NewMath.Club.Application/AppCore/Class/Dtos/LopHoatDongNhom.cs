﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    public class LopHoatDongNhomInput : PagedAndSortedResultRequestDto
    {
        public virtual string FilterCode { get; set; }
        public virtual string FilterDiaChi { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? BatDau_Min { get; set; }
        public virtual DateTime? BatDau_Max { get; set; }
    }
    #region LopHoatDongNhom
    [AutoMap(typeof(LopHoatDongNhom))]
    public class LopHoatDongNhomDto : EntityDto<int>
    {
        public virtual string Ten { get; set; }
        public virtual string CodeId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? Thoigian_Start { get; set; }
        public virtual DateTime? Thoigian_End { get; set; }
        public virtual int? GiaovienId { get; set; }
        public virtual int? Gia { get; set; }
        public virtual int? DiaChiId { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? SoLuong { get; set; }
        public virtual int? SoLuong_Max { get; set; }
        public virtual DateTime? ThoiGian_BatDau { get; set; }
        public virtual string MoTa { get; set; }

    }
    [AutoMap(typeof(LopHoatDongNhom))]
    public class LopHoatDongCreatNhom : EntityDto<int>
    {
        public virtual string Ten { get; set; }
        public virtual string CodeId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? Thoigian_Start { get; set; }
        public virtual DateTime? Thoigian_End { get; set; }
        public virtual int? GiaovienId { get; set; }
        public virtual int? Gia { get; set; }
        public virtual int? DiaChiId { get; set; }
        public virtual decimal? ToaDoX { get; set; }
        public virtual decimal? ToaDoY { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? SoLuong { get; set; }
        public virtual int? SoLuong_Max { get; set; }
        public virtual DateTime? ThoiGian_BatDau { get; set; }
        public virtual string MoTa { get; set; }

    }
    public class LopHoatDongNhomView : EntityDto<int>
    {
        public virtual string Ten { get; set; }
        public virtual string CodeId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual string LoaiLop { get; set; }
        public virtual string MonHoc { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? Thoigian_Start { get; set; }
        public virtual DateTime? Thoigian_End { get; set; }
        public virtual string Giaovien { get; set; }
        public virtual string Gia { get; set; }
        public virtual string ToaDoX { get; set; }
        public virtual string ToaDoY { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual string TrangThai { get; set; }
        public virtual int? SoLuong { get; set; }
        public virtual int? SoLuongMax { get; set; }
        public virtual DateTime? ThoiGian_BatDau { get; set; }
        public virtual string MoTa { get; set; }
    }
    #endregion
}
