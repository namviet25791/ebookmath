﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using NewMath.Club.Authorization.Users;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using Abp.Application.Services;
using newPSG.PMS.Helper;
using Dapper;
using System;

namespace NewMath.Club.GiaoVienStore
{
    #region Interface
    public interface ILopHocService : IApplicationService
    {
        Task<ListResultDto<LopYeuCauView>> GetPage(LopYeuCauInput input);
        Task<LopYeuCau> GetLopYeuCau(int Id);
        Task CreatLopYeuCau(LopYeuCauCreatInput input);
        void DeleteLopYeuCau(int Id);
    }
    #endregion
        //Server get GiaoVienInformation
}
