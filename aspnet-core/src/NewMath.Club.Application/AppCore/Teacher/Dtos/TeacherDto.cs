﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    [AutoMap(typeof(GiaoVien))]
    public class GiaoVienDto : EntityDto<int>
    {
        public int? UserId { get; set; }
        public byte[] Avatar { get; set; }
        public string Ten { get; set; }
        public string Email { get; set; }
        public int? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public int? Tuoi { get; set; }
        public string TinhCach { get; set; }
        public int? MonHocId { get; set; }
        public string Mota { get; set; }
        public string ThanhTichNoiBat { get; set; }
        public string DiaChiLamViec { get; set; }
        public string DiaChiNha { get; set; }
        public int StatusId { get; set; }
        public int? SchoolId { get; set; }
        public int? BangCapId { get; set; }
        public byte[] PhotoBang { get; set; }
        public int? CongViecId { get; set; }
        public int? StatusTimeId { get; set; }
        public string DienThoai { get; set; }
    }
    [AutoMap(typeof(GiaoVienQuaKhu))]
    public class GiaoVienQuaKhuDto : EntityDto<int>
    {
        public virtual int GiaovienId { get; set; }
        public virtual string DiaChi_LamViec { get; set; }
        public virtual string DiaChi_LamViec2 { get; set; }
        public virtual string DiaChi_LamViec3 { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual string Mota { get; set; }
    }
    [AutoMap(typeof(GiaoVienThanhTich))]
    public class GiaoVienThanhTichDto : EntityDto<int>
    {
        public virtual int GiaoVienId { get; set; }
        public virtual string TruongHoc { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? Year { get; set; }
        public virtual int LoaiLopId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual string ThanhTich { get; set; }
        public virtual byte[] Photo { get; set; }
        public virtual string MoTa { get; set; }
    }
    [AutoMap(typeof(GiaoVienBangCap))]
    public class GiaoVienBangCapDto : EntityDto<int>
    {
        public virtual int GiaoVienId { get; set; }
        public virtual int TruongHocId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual int BangCapId { get; set; }
        public virtual byte[] Photo { get; set; }
        public virtual string MoTa { get; set; }
    }
    [AutoMap(typeof(GiaoVienKiNang))]
    public class GiaoVienKiNangDto : EntityDto<int>
    {
        public virtual int GiaoVienId { get; set; }
        public virtual int? LoaiLop { get; set; }
        public virtual int? LopId { get; set; }
        public virtual int? GioiTinh { get; set; }
    }
    [AutoMap(typeof(GiaoVienSoLuong))]

    public class GiaoVienSoLuongDto : EntityDto<int>
    {
        public virtual int GiaoVienId { get; set; }
        public virtual int? SoLuongId { get; set; }
    }
    [AutoMap(typeof(GiaoVienCoTheDay))]
    public class GiaoVienCoTheDayDto : EntityDto<int>
    {
        public virtual int GiaoVienId { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual int? ThanhPho { get; set; }
        public virtual int? Quan { get; set; }
    }
    [AutoMap(typeof(GiaoVienRanh))]
    public class GiaoVienRanhDto : EntityDto<int>
    {
        public virtual int GiaoVienId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
    }
    [AutoMap(typeof(GiaoVienLich))]
    public class GiaoVienLichDto : EntityDto<int>
    {
        public virtual int GiaoVienId { get; set; }
        public virtual int? LopId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual int? MonHocId { get; set; }
    }
    public class GiaoVienInput: PagedAndSortedResultRequestDto
    {
        public virtual string Ten { get; set; }
        public virtual int? Tuoi_Start { get; set; }
        public virtual int? Tuoi_End { get; set; }
        public virtual int? SchoolId { get; set; }
        public virtual int? BangCapId { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? StatusId { get; set; }
    }
    public class GiaoVienOutPut:EntityDto<int>
    {
        public virtual int? UserId { get; set; }
        public virtual string Ten { get; set; }
        public virtual byte[] Avatar { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual int? Tuoi { get; set; }
        public virtual string MonHoc { get; set; }
        public virtual string Truong { get; set; }
        public virtual string ThanhTichNoiBat { get; set; }
        public virtual string DiaChiNha { get; set; }
        public virtual int? BangCapId { get; set; }
        public virtual byte[] PhotoBang { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual string TrangThai { get; set; }
        public virtual int? TrangThaiTime { get; set; }
    }
}
