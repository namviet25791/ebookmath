﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using newPSG.PMS.Helper;
using Dapper;
using System;

namespace NewMath.Club.GiaoVienStore
{
    #region Main
    public class GiaoVienService : ClubAppServiceBase, IGiaoVienService
    {
        private readonly IApplicationServiceFactory _factory;

        public GiaoVienService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        #region GiaoVienInforMation
        public async Task<ListResultDto<GiaoVienOutPut>> GetPage(GiaoVienInput input)
        {
            var sql = $@"select u.Id UserId,g.Ten,g.Id,g.Avatar,g.GioiTinh,g.BangCapId,g.ThanhTichNoiBat,g.DiaChiNha,g.PhotoBang,g.StatusTimeId TrangThaiTime,g.Tuoi,
                                t.Mota Truong,m.MoTa Monhoc,
                                s.Id StatusId,s.MoTa TrangThai 
                                from dbgiaovien g Left Join abpusers u on g.UserId=u.Id
                                join dbmonhoc m on m.Id=g.MonHocId
                                join dbtruong t on g.SchoolId=t.Id
                                join dbstatus s on s.Id=g.StatusId
                        ";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<string> list = new List<string>();
            int i = 1;
            if (!string.IsNullOrEmpty(input.Ten)) { list.Add("g.Ten LIKE @" + i); dic.Add(@"@" + i, "%" + input.Ten + "%"); i++; }
            if (input.Tuoi_Start != null) { list.Add("g.Tuoi > @" + i); dic.Add("@" + i, input.Tuoi_Start); i++; }
            if (input.Tuoi_End != null) { list.Add("g.Tuoi < @" + i); dic.Add("@" + i, input.Tuoi_End); i++; }
            if (input.SchoolId != null) { list.Add("g.SchoolId = @" + i); dic.Add("@" + i, input.SchoolId); i++; }
            if (input.BangCapId != null) { list.Add("g.BangCapId = @" + i); dic.Add("@" + i, input.BangCapId); i++; }
            if (input.GioiTinh != null) { list.Add("g.GioiTinh = @" + i); dic.Add("@" + i, input.GioiTinh); i++; }
            if (input.MonHocId != null) { list.Add("g.MonHocId = @" + i); dic.Add("@" + i, input.MonHocId); i++; }
            if (input.StatusId != null) { list.Add("g.StatusId = @" + i); dic.Add("@" + i, input.StatusId); i++; }
            if (list.Count > 0)
            {
                sql += " WHERE " + string.Join(" AND ", list.ToArray());
            }
            var parameters = new DynamicParameters(dic);
            var sqlCount = "select count(1) total from(" + sql + ")A";
            var sorting = input.Sorting ?? "Id";
            var sqlItems = sql + " order by " + sorting + " limit " + input.MaxResultCount + " OFFSET " + input.SkipCount;
            var Counts = await _factory.UnitOfWordDapper.QueryAsync<int>(sqlCount, parameters);
            var total = Counts.ElementAt(0);
            var Items = await _factory.UnitOfWordDapper.QueryAsync<GiaoVienOutPut>(sqlItems, parameters);
            return new PagedResultDto<GiaoVienOutPut>(total, Items.ToList());
        }
        public async Task<GiaoVien> GetThongTin(int Id)
        {
            var _Repos = _factory.Repository<GiaoVien, int>();
            var filter = _Repos.FirstOrDefaultAsync(e => e.Id == Id);
            return await filter;
        }
        public async Task<List<GiaoVienBangCap>> GetBangCap(int Id)
        {
            var _BangCap = _factory.Repository<GiaoVienBangCap, int>();
            var filter = _BangCap.GetAll().Where(e => e.GiaoVienId == Id);
            return await filter.ToListAsync();
        }
        public async Task<List<GiaoVienQuaKhu>> GetQuaKhu(int Id)
        {
            var _QuaKhu = _factory.Repository<GiaoVienQuaKhu, int>();
            var filter = _QuaKhu.GetAll().Where(e => e.GiaovienId == Id);
            return await filter.ToListAsync();
        }
        public async Task<List<GiaoVienThanhTich>> GetThanhTich(int Id)
        {

            var _ThanhTich = _factory.Repository<GiaoVienThanhTich, int>();
            var filter = _ThanhTich.GetAll().Where(e => e.GiaoVienId == Id);
            return await filter.ToListAsync();
        }
        #endregion
        #region GiaoVienTaoMoiUpdate
        public async Task CreatUpdateGiaoVien(GiaoVienDto input)
        {
            input.Tuoi = DateTime.Now.Year - input.NgaySinh.Value.Year + 1;
            var _GiaoVien = _factory.Repository<GiaoVien, int>();
            await _GiaoVien.InsertOrUpdateAsync(ObjectMapper.Map<GiaoVien>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task CreatListQuaKhu(List<GiaoVienQuaKhuDto> input)
        {
            var _QuaKhu = _factory.Repository<GiaoVienQuaKhu, int>();
            int Id = 0;
            foreach (var item in input)
            {
                if (Id == 0)
                {
                    Id = await _QuaKhu.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienQuaKhu>(item));
                }
                else
                {
                    item.Id = ++Id;
                    await _QuaKhu.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienQuaKhu>(item));
                }
            }
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task UpdateQuaKhu(GiaoVienQuaKhuDto input)
        {
            var _QuaKhu = _factory.Repository<GiaoVienQuaKhu, int>();
            await _QuaKhu.UpdateAsync(ObjectMapper.Map<GiaoVienQuaKhu>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task CreatListThanhTich(List<GiaoVienThanhTichDto> input)
        {
            var _ThanhTich = _factory.Repository<GiaoVienThanhTich, int>();
            int Id = 0;
            foreach (var item in input)
            {
                if (Id == 0)
                {
                    Id = await _ThanhTich.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienThanhTich>(item));
                }
                else
                {
                    item.Id = ++Id;
                    await _ThanhTich.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienThanhTich>(item));
                }
            }
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task UpdateThanhTich(GiaoVienThanhTichDto input)
        {
            var _ThanhTich = _factory.Repository<GiaoVienThanhTich, int>();
            await _ThanhTich.UpdateAsync(ObjectMapper.Map<GiaoVienThanhTich>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task CreatListBangCap(List<GiaoVienBangCapDto> input)
        {
            var _ThanhTich = _factory.Repository<GiaoVienBangCap, int>();
            int Id = 0;
            foreach (var item in input)
            {
                if (Id == 0)
                {
                    Id = await _ThanhTich.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienBangCap>(item));
                }
                else
                {
                    item.Id = ++Id;
                    await _ThanhTich.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienBangCap>(item));
                }
            }
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task UpdateBangCap(GiaoVienBangCapDto input)
        {
            var _Repos = _factory.Repository<GiaoVienBangCap, int>();
            await _Repos.UpdateAsync(ObjectMapper.Map<GiaoVienBangCap>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task CreatListKiNang(List<GiaoVienKiNangDto> input)
        {
            var _KiNang = _factory.Repository<GiaoVienKiNang, int>();
            int Id = 0;
            foreach (var item in input)
            {
                if (Id == 0)
                {
                    Id = await _KiNang.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienKiNang>(item));
                }
                else
                {
                    item.Id = ++Id;
                    await _KiNang.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienKiNang>(item));
                }
            }
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task UpdateKiNang(GiaoVienKiNangDto input)
        {
            var _KiNang = _factory.Repository<GiaoVienKiNang, int>();
            await _KiNang.UpdateAsync(ObjectMapper.Map<GiaoVienKiNang>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task CreatListSoLuong(List<GiaoVienSoLuongDto> input)
        {
            var _SoLuong = _factory.Repository<GiaoVienSoLuong, int>();
            int Id = 0;
            foreach (var item in input)
            {
                if (Id == 0)
                {
                    Id = await _SoLuong.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienSoLuong>(item));
                }
                else
                {
                    item.Id = ++Id;
                    await _SoLuong.InsertAndGetIdAsync(ObjectMapper.Map<GiaoVienSoLuong>(item));
                }
            }
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        #endregion
        #region DeleteGiaoVien
        public async Task Delete(int Id)
        {
            var _Repos = _factory.Repository<GiaoVien, int>();
            await _Repos.DeleteAsync(e => e.Id == Id);
        }
        public async Task DeleteTT(int Id)
        {
            var _Repos = _factory.Repository<GiaoVienThanhTich, int>();
            await _Repos.DeleteAsync(e => e.Id == Id);
        }
        public async Task DeleteBC(int Id)
        {
            var _Repos = _factory.Repository<GiaoVienBangCap, int>();
            await _Repos.DeleteAsync(e => e.Id == Id);
        }
        public async Task DeleteQK(int Id)
        {
            var _Repos = _factory.Repository<GiaoVienQuaKhu, int>();
            await _Repos.DeleteAsync(e => e.Id == Id);
        }
        #endregion
        #endregion
        //Server get GiaoVienInformation
    }
}
