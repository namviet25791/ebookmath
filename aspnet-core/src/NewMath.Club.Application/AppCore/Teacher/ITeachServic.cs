﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using NewMath.Club.Authorization.Users;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using Abp.Application.Services;
using newPSG.PMS.Helper;
using Dapper;
using System;

namespace NewMath.Club.GiaoVienStore
{
    #region Interface
    public interface IGiaoVienService : IApplicationService
    {
        Task<ListResultDto<GiaoVienOutPut>> GetPage(GiaoVienInput input);
        Task<GiaoVien> GetThongTin(int Id);
        Task<List<GiaoVienBangCap>> GetBangCap(int Id);
        Task<List<GiaoVienQuaKhu>> GetQuaKhu(int Id);
        Task<List<GiaoVienThanhTich>> GetThanhTich(int Id);
        Task CreatUpdateGiaoVien(GiaoVienDto input);
        Task CreatListQuaKhu(List<GiaoVienQuaKhuDto> input);
        Task UpdateQuaKhu(GiaoVienQuaKhuDto input);
        Task CreatListBangCap(List<GiaoVienBangCapDto> input);
        Task UpdateBangCap(GiaoVienBangCapDto input);
        Task CreatListThanhTich(List<GiaoVienThanhTichDto> input);
        Task UpdateThanhTich(GiaoVienThanhTichDto input);
        Task CreatListKiNang(List<GiaoVienKiNangDto> input);
        Task UpdateKiNang(GiaoVienKiNangDto input);
        Task Delete(int Id);
        Task DeleteTT(int Id);
        Task DeleteBC(int Id);
        Task DeleteQK(int Id);

    }
    #endregion
        //Server get GiaoVienInformation
}
