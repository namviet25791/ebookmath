﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Application.Services;
using Abp.Linq.Extensions;
using newPSG.PMS.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.UI;
using NewMath.Club.DataTutor;
using NewMath.Club;

namespace newPSG.PMS.Services
{
    #region INTERFACE
    public interface IDanhMucHuyenAppService : IApplicationService
    {
        Task<PagedResultDto<DanhMucHuyenDto>> SearchServerPaging(DanhMucHuyenInputDto input);
        Task<int> CreateOrUpdate(DanhMucHuyenDto input);
        Task Delete(EntityDto input);
        Task<DanhMucHuyenDto> GetById(EntityDto<int> input);
        Task<IEnumerable<ComboboxItemDto>> ComboBoxData(string maTinh);
    }
    #endregion

    #region MAIN
    public class DanhMucHuyenAppService : ClubAppServiceBase, IDanhMucHuyenAppService
    {
        private readonly IRepository<DanhMucHuyenEntity> _huyenRepos;
        private readonly IRepository<DanhMucTinhEntity> _tinhRepos;
        public DanhMucHuyenAppService(IRepository<DanhMucHuyenEntity> huyenRepos,
                               IRepository<DanhMucTinhEntity> tinhRepos)
        {
            _huyenRepos = huyenRepos;
            _tinhRepos = tinhRepos;
        }

        public async Task<PagedResultDto<DanhMucHuyenDto>> SearchServerPaging(DanhMucHuyenInputDto input)
        {
            try
            {
                var query = (from huyen in _huyenRepos.GetAll()
                             join r_tinh in _tinhRepos.GetAll() on huyen.MaTinh equals r_tinh.MaTinh into tb_tinh //Left Join
                             from tinh in tb_tinh.DefaultIfEmpty()
                             select new DanhMucHuyenDto
                             {
                                 Id = huyen.Id,
                                 TenHuyen = huyen.TenHuyen,
                                 MaHuyen = huyen.MaHuyen,
                                 MaTinh=huyen.MaTinh,
                                 TenTinh= tinh.TenTinh,
                                Cap=huyen.Cap,
                             })
           .WhereIf(!string.IsNullOrEmpty(input.Filter), u => u.TenHuyen.Contains(input.Filter) || u.MaHuyen.Contains(input.Filter))
           .WhereIf(!string.IsNullOrEmpty(input.MaTinh), u => u.MaTinh == input.MaTinh);


                var huyenCount = await query.CountAsync();
                var dataGrids = await query
                     .OrderBy(input.Sorting ?? "id asc")
                    .PageBy(input)
                   .ToListAsync();
                return new PagedResultDto<DanhMucHuyenDto>(huyenCount, dataGrids);
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<int> CreateOrUpdate(DanhMucHuyenDto input)
        {
            var tinh=await _tinhRepos.FirstOrDefaultAsync(x => x.MaTinh == input.MaTinh);
            if (tinh != null)
                input.TenTinh = tinh.TenTinh;
            if (input.Id > 0)
            {
                // update
                var updateData = await _huyenRepos.GetAsync(input.Id.Value);
                input.MapTo(updateData);
                await _huyenRepos.UpdateAsync(updateData);
                return updateData.Id;
            }
            else
            {
                try
                {
                    var insertInput = input.MapTo<DanhMucHuyenEntity>();
                    await _huyenRepos.InsertAsync(insertInput);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return insertInput.Id;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task Delete(EntityDto input)
        {
            var huyen = await _huyenRepos.GetAsync(input.Id);
            await _huyenRepos.DeleteAsync(huyen);
        }
        public async Task<DanhMucHuyenDto> GetById(EntityDto<int> input)
        {
            var data = await _huyenRepos.FirstOrDefaultAsync(input.Id);
            var res = ObjectMapper.Map<DanhMucHuyenDto>(data);
            return res;
        }


        public async Task<IEnumerable<ComboboxItemDto>> ComboBoxData(string maTinh)
        {
            var query = _huyenRepos.GetAll().Where(x=>x.MaTinh==maTinh)
                .Select(x => new ComboboxItemDto
                {
                    Value = x.MaHuyen,
                    DisplayText = x.TenHuyen,
                });
            var data = await query.ToListAsync();
            return data;
        }

    }
    #endregion
}
