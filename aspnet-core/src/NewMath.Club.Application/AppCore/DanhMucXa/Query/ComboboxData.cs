﻿using Abp.Application.Services.Dto;
using MediatR;
using NewMath.Club.DataTutor;
using newPSG.PMS.Helper;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace newPSG.PMS.DanhMucXa.HandlerQuery
{
    public class GetComboBoxDataQuery : IRequest<IEnumerable<ComboboxItemDto>>
    {
        public string MaHuyen { get; set; }
    }
    public class GetComboBoxDataQueryHandler : IRequestHandler<GetComboBoxDataQuery, IEnumerable<ComboboxItemDto>>
    {
        private readonly IApplicationServiceFactory _factory;
        public GetComboBoxDataQueryHandler(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        public async Task<IEnumerable<ComboboxItemDto>> Handle(GetComboBoxDataQuery request, CancellationToken cancellationToken)
        {
            var keyCache = $"ComboBox_{request.MaHuyen}";
            var cache = _factory.CacheManager.GetCache("DanhMucXa");
            var dataCache = await cache.GetOrDefaultAsync(keyCache);
            if (dataCache != null)
            {
                return dataCache as IEnumerable<ComboboxItemDto>;
            }
            var xaRepos = _factory.UnitOfWordDapper.DapperRepository<DanhMucXaEntity>();
            var data = await xaRepos
                .SetSelect(x => new
                {
                    x.MaXa,
                    x.TenXa
                })
                .FindAllAsync(x=>x.MaHuyen == request.MaHuyen);
            var listDto = data.Select(x => new ComboboxItemDto()
            {
                Value =  x.MaXa,
                DisplayText =  x.TenXa
            });

            await cache.SetAsync(keyCache, listDto);
            return listDto;
        }
    }
}
