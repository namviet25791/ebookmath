﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NewMath.Club.DataTutor;
using newPSG.PMS.DanhMucXa.Dto;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace newPSG.PMS.DanhMucXa.HandlerQuery
{
    public class PagingListQuery : PagedAndSortedResultRequestDto, IRequest<PagedResultDto<DanhMucXaDto>>
    {
        public string Filter { get; set; }
        public string MaTinh { get; set; }
        public string MaHuyen { get; set; }
    }
    public class PagingListQueryHandler : IRequestHandler<PagingListQuery, PagedResultDto<DanhMucXaDto>>
    {
        private readonly IRepository<DanhMucXaEntity> _xaRepos;
        private readonly IMapper _mapper;
        public PagingListQueryHandler(IRepository<DanhMucXaEntity> xaRepos,
            IMapper mapper)
        {
            _mapper = mapper;
            _xaRepos = xaRepos;
        }
        public async Task<PagedResultDto<DanhMucXaDto>> Handle(PagingListQuery input, CancellationToken cancellationToken)
        {
            var query = _xaRepos.GetAll()
                .WhereIf(!string.IsNullOrEmpty(input.Filter),
                    x => x.TenXa.Contains(input.Filter) || x.MaXa == input.Filter)
                .WhereIf(!string.IsNullOrEmpty(input.MaTinh), x => x.MaTinh == input.MaTinh)
                .WhereIf(!string.IsNullOrEmpty(input.MaHuyen), x => x.MaHuyen == input.MaHuyen);
            var count = await query.CountAsync(cancellationToken: cancellationToken);
            var items = await query.OrderBy(x => x.Id).PageBy(input).ToListAsync(cancellationToken);
            var listDtos = _mapper.Map<List<DanhMucXaDto>>(items);
            return new PagedResultDto<DanhMucXaDto>(
                count,
                listDtos
            );
        }
    }
}
