﻿using Abp.Application.Services.Dto;

namespace newPSG.PMS.DanhMucTinh.Dto
{
    public class PagingListDanhMucTinhInput: PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
