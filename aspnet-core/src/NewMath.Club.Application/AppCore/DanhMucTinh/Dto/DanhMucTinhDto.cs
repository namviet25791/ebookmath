﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using NewMath.Club.DataTutor;

namespace newPSG.PMS.DanhMucTinh.Dto
{
    [AutoMap(typeof(DanhMucTinhEntity))]

    public class DanhMucTinhDto: EntityDto<int>
    {
        public string MaTinh { get; set; }
        public string TenTinh { get; set; }
        public string Cap { get; set; }
    }
}
