﻿using System;
using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using newPSG.PMS.DanhMucTinh.Dto;
using System.Threading.Tasks;

namespace newPSG.PMS.DanhMucTinh
{
    public interface IDanhMucTinhAppService : IApplicationService
    {
        Task<IEnumerable<ComboboxItemDto>> ComboBoxData();
        Task<PagedResultDto<DanhMucTinhDto>> PagingList(PagingListDanhMucTinhInput input);
        Task InsertOrUpdate(DanhMucTinhDto input);
        Task Delete(int id);
    }
}
