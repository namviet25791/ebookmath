﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Linq.Extensions;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NewMath.Club;
using NewMath.Club.DataTutor;
using newPSG.PMS.DanhMucTinh.Dto;
using newPSG.PMS.Helper;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace newPSG.PMS.DanhMucTinh
{
    [AbpAuthorize]
    public class ComboboxService : ClubAppServiceBase, IDanhMucTinhAppService
    {
        private readonly IApplicationServiceFactory _factory;
        public ComboboxService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> ComboBoxData()
        {
            
            var cache = _factory.CacheManager.GetCache("DanhMucTinh");
            var dataCache = await cache.GetOrDefaultAsync("ComboBox");
            if (dataCache != null)
            {
                return dataCache as IEnumerable<ComboboxItemDto>;
            }
            var tinhRepos = _factory.UnitOfWordDapper.DapperRepository<DanhMucTinhEntity>();
            var sql = $@"select MaTinh Value, TenTinh DisplayText from DanhMucTinh";
            /*WHERE IsDeleted = @isDelete*/
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            await cache.SetAsync("ComboBox", data);
            return data;
        }
        [HttpPost]
        public async Task<PagedResultDto<DanhMucTinhDto>> PagingList(PagingListDanhMucTinhInput input)
        {
            var tinhRepos = _factory.Repository<DanhMucTinhEntity, int>();
            var query = tinhRepos.GetAll()
                .WhereIf(!string.IsNullOrEmpty(input.Filter),x=>x.TenTinh.Contains(input.Filter));
            var count = await query.CountAsync();
            var items = await query
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input).ToListAsync();
            var listDtos = ObjectMapper.Map<List<DanhMucTinhDto>>(items);
            return new PagedResultDto<DanhMucTinhDto>(
               count,
               listDtos
               );
        }

        [HttpPost]
        public async Task InsertOrUpdate(DanhMucTinhDto input)
        {
            var tinhRepos = _factory.Repository<DanhMucTinhEntity, int>();
            if (input.Id > 0)
            {
                var @update = await tinhRepos.GetAsync(input.Id);
                ObjectMapper.Map(input, @update);
                await tinhRepos.UpdateAsync(@update);
            }
            else
            {
                var @insert = ObjectMapper.Map<DanhMucTinhEntity>(input);
                await tinhRepos.InsertAsync(@insert);
            }
            await CurrentUnitOfWork.SaveChangesAsync();
            await _factory.CacheManager.GetCache("DanhMucTinh").ClearAsync();
        }

        [HttpDelete]
        public async Task Delete(int id)
        {
            await _factory.Repository<DanhMucTinhEntity, int>().DeleteAsync(id);
            await CurrentUnitOfWork.SaveChangesAsync();
            await _factory.CacheManager.GetCache("DanhMucTinh").ClearAsync();
        }

    }
}
