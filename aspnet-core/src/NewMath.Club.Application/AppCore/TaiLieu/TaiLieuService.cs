﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using System.Linq;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using System.Collections.Generic;
using newPSG.PMS.Helper;
using Dapper;

namespace NewMath.Club.GiaoVienStore
{
    public class TaiLieuService : ClubAppServiceBase, ITaiLieuService
    {
        private readonly IApplicationServiceFactory _factory;

        public TaiLieuService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        public async Task<ListResultDto<TaiLieuView>> GetPage(TaiLieuInput input)
        {
            var sql = $@"select l.Id,l.CodeId,l.Ten,l.Lopso,l.Gia,l.DanhGia,l.SoTrang,l.SoLuongTai,l.HavePro,l.MucDo,l.CapHocId,
		                        u.Name UserName,m.MoTa Monhoc, ll.MoTa LoaiLop
                                from dbtailieu l
                                join dbmonhoc m on m.Id=l.MonHocId
                                join abpusers u on l.UserId=u.Id
                                left join dbloailop ll on ll.Id=l.LoaiLopId
                                left join dbloaitailieu lt on lt.Id=l.LoaiTaiLieuId
                        ";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<string> list = new List<string>();
            int i = 1;
            if (!string.IsNullOrEmpty(input.FilterTen)) { list.Add("l.Ten LIKE @" + i); dic.Add(@"@" + i, "%" + input.FilterTen + "%"); i++; }
            if (input.LoaiLopId != null) { list.Add("l.LoaiLopId = @" + i); dic.Add("@" + i, input.LoaiLopId); i++; }
            if (input.Nam != null) { list.Add("l.Nam= @" + i); dic.Add("@" + i, input.Nam); i++; }
            if (input.LoaiId != null) { list.Add("l.LoaiId= @" + i); dic.Add("@" + i, input.LoaiId); i++; }
            if (input.MonHocId != null) { list.Add("l.MonHocId= @" + i); dic.Add("@" + i, input.MonHocId); i++; }
            if (input.Lopso != null) { list.Add("l.Lopso= @" + i); dic.Add("@" + i, input.Lopso); i++; }
            if (list.Count > 0)
            {
                sql += " WHERE " + string.Join(" AND ", list.ToArray());
            }
            var parameters = new DynamicParameters(dic);
            var sqlCount = "select count(1) total from(" + sql + ")A";
            var sorting = "l.Nam";
            var sqlItems = sql + " order by " + sorting + " limit " + input.MaxResultCount + " OFFSET " + input.SkipCount;
            var Counts = await _factory.UnitOfWordDapper.QueryAsync<int>(sqlCount, parameters);
            var total = Counts.ElementAt(0);
            var Items = await _factory.UnitOfWordDapper.QueryAsync<TaiLieuView>(sqlItems, parameters);
            return new PagedResultDto<TaiLieuView>(total, Items.ToList());
        }
        public async Task<TaiLieu> GetById(int Id)
        {
            var _Repos = _factory.Repository<TaiLieu, int>();
            var filter = _Repos.FirstOrDefaultAsync(e => e.Id == Id);
            return await filter;
        }
        public async Task CreatUpdate(TaiLieuDto input)
        {
            var _Repos = _factory.Repository<TaiLieu, int>();
            await _Repos.InsertOrUpdateAsync(ObjectMapper.Map<TaiLieu>(input));
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public void Delete(int Id)
        {
            var _Repos = _factory.Repository<TaiLieu, int>();
            _Repos.Delete(x => x.Id == Id);
        }
        //Server get GiaoVienInformation
    }
}
