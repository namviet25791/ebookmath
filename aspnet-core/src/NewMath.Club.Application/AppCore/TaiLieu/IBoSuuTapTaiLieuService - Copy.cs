﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using Abp.Application.Services;

namespace NewMath.Club.GiaoVienStore
{
    #region Interface
    public interface IBoSuuTapTaiLieuService : IApplicationService
    {
        Task<ListResultDto<TaiLieuBoSuuTapView>> GetPage(TaiLieuBoSuuTapInput input);
        Task<TaiLieuBoSuuTap> GetById(int Id);
        Task CreatUpdate(TaiLieuBoSuuTapDto input);
        void Delete(int Id);
    }
    #endregion
        //Server get GiaoVienInformation
}
