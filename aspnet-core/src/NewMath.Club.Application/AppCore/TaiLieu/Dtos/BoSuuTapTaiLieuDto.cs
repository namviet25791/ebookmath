﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    [AutoMap(typeof(TaiLieuBoSuuTap))]
    public class TaiLieuBoSuuTapDto : EntityDto<int>
    {
        public virtual string CodeId { get; set; }
        public virtual int? Lopso { get; set; }
        public virtual int? CapHocId { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? SotaiLieu { get; set; }
        public virtual int? LoaiId { get; set; }
        public virtual int? SoNguoiTai { get; set; }
        public virtual int? SoNguoiMua { get; set; }
        public virtual int? Gia { get; set; }
        public virtual string MoTa { get; set; }
        public virtual int? DanhGia { get; set; }
        public virtual int? StatusId { get; set; }
    }
    public class TaiLieuBoSuuTapInput : PagedAndSortedResultRequestDto
    {
        public virtual string FilterTen { get; set; }
        public virtual int? Nam { get; set; }
        public virtual int? LoaiId { get; set; }
        public virtual int? Lopso { get; set; }
        public virtual int? CapHocId { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? StatusId { get; set; }
    }
    public class TaiLieuBoSuuTapView : EntityDto<int>
    {
        public virtual string CodeId { get; set; }
        public virtual string Ten { get; set; }
        public virtual string UserName { get; set; }
        public virtual int? Lopso { get; set; }
        public virtual int? CapHocId { get; set; }
        public virtual string LoaiLop { get; set; }
        public virtual string MonHoc { get; set; }
        public virtual int? SotaiLieu { get; set; }
        public virtual string LoaiSuuTap { get; set; }
        public virtual int? SoNguoiTai { get; set; }
        public virtual int? SoNguoiMua { get; set; }
        public virtual int? Gia { get; set; }
        public virtual string MoTa { get; set; }
        public virtual int? DanhGia { get; set; }
        public virtual string TrangThai { get; set; }
    }
}
