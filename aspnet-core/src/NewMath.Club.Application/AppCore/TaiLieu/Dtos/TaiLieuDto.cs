﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.DataTutor;
using System;

namespace NewMath.Club.TeacherStore.Dto
{
    [AutoMap(typeof(TaiLieu))]
    public class TaiLieuDto : EntityDto<int>
    {
        public virtual int? UserId { get; set; }
        public virtual string CodeId { get; set; }
        public virtual int? LoaiTaiLieu { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLop { get; set; }
        public virtual int? CapHocId { get; set; }
        public virtual int? MonHoc { get; set; }
        public virtual int? SoTrang { get; set; }
        public virtual string TacGia { get; set; }
        public virtual int? Nam { get; set; }
        public virtual int? Gia { get; set; }
        public virtual string Mota { get; set; }
        public virtual string LinkPdf { get; set; }
        public virtual int SoLuongTai { get; set; }
        public virtual int? DanhGia { get; set; }
        public virtual int? LoaiFile { get; set; }
    }
    public class TaiLieuInput : PagedAndSortedResultRequestDto
    {
        public virtual string FilterTen { get; set; }
        public virtual int? Nam { get; set; }
        public virtual int? LoaiId { get; set; }
        public virtual int? Lopso { get; set; }
        public virtual int? CapHocId { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
    }
    public class TaiLieuView : EntityDto<int>
    {
        public virtual string UserName { get; set; }
        public virtual string CodeId { get; set; }
        public virtual string LoaiTaiLieu { get; set; }
        public virtual int? MucDo { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual string LoaiLop { get; set; }
        public virtual int? CapHocId { get; set; }
        public virtual string MonHoc { get; set; }
        public virtual int? SoTrang { get; set; }
        public virtual string TacGia { get; set; }
        public virtual int? Nam { get; set; }
        public virtual int? Gia { get; set; }
        public virtual string Mota { get; set; }
        public virtual string LinkPdf { get; set; }
        public virtual int SoLuongTai { get; set; }
        public virtual int? DanhGia { get; set; }
        public virtual int? LoaiFile { get; set; }
        public virtual bool HavePro { get; set; }
    }
}
