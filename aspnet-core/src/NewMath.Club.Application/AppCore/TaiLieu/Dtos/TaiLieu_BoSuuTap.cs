﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace NewMath.Club.TeacherStore.Dto
{
    public class TaiLieu_BoSuuTapDto : EntityDto<int>
    {
        public TaiLieuBoSuuTapDto BoSuuTap { get; set; }
        public List<TaiLieuDto> ListTaiLieu { get; set; }
    }
}
