﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using Abp.Application.Services;

namespace NewMath.Club.GiaoVienStore
{
    #region Interface
    public interface ITaiLieuService : IApplicationService
    {
        Task<ListResultDto<TaiLieuView>> GetPage(TaiLieuInput input);
        Task<TaiLieu> GetById(int Id);
        Task CreatUpdate(TaiLieuDto input);
        void Delete(int Id);
    }
    #endregion
        //Server get GiaoVienInformation
}
