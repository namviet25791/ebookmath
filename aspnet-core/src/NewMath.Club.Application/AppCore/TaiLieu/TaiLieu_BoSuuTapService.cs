﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NewMath.Club.DataTutor;
using NewMath.Club.TeacherStore.Dto;
using newPSG.PMS.Helper;

namespace NewMath.Club.GiaoVienStore
{
    public class TaiLieu_BoSuuTapService : ClubAppServiceBase
    {
        private readonly IApplicationServiceFactory _factory;
        private readonly IBoSuuTapTaiLieuService _BoSuuTap;
        private readonly ITaiLieuService _TaiLieu;

        public TaiLieu_BoSuuTapService(IApplicationServiceFactory factory,
                IBoSuuTapTaiLieuService BoSuuTap,
                ITaiLieuService TaiLieu
            )
        {
            _factory = factory;
            _BoSuuTap = BoSuuTap;
            _TaiLieu = TaiLieu;
        }
        public async Task CreatUpDate(TaiLieu_BoSuuTapDto input)
        {
            await _BoSuuTap.CreatUpdate(input.BoSuuTap);
            foreach(var item in input.ListTaiLieu)
            {
                await _TaiLieu.CreatUpdate(item);
            }
            await CurrentUnitOfWork.SaveChangesAsync();
        }
        public async Task<List<TaiLieu>> GetTaiLieuByBoSuuTap(int Id)
        {
            var sql = $@"select t.* from dbtailieu t join dbtailieu_bosuutap_chitiet d where d.BoSuuTapId=1";
            var Items = await _factory.UnitOfWordDapper.QueryAsync<TaiLieu>(sql);
            return Items.ToList();
        }
        //Server get GiaoVienInformation
    }
}
