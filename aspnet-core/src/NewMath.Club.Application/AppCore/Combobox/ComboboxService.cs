﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using newPSG.PMS.Helper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewMath.Club.Combobox
{
    #region Interface
    public interface IComboboxService : IApplicationService
    {
        Task<IEnumerable<ComboboxItemDto>> LoaiStatus();
        Task<IEnumerable<ComboboxItemDto>> Status(string Loai);
        Task<IEnumerable<ComboboxItemDto>> LoaiNote();
        Task<IEnumerable<ComboboxItemDto>> MonHoc();
        Task<IEnumerable<ComboboxItemDto>> PhanMon(int? MonHocId);
        Task<IEnumerable<ComboboxItemDto>> CapHoc();
        Task<IEnumerable<ComboboxItemDto>> Truong();
        Task<IEnumerable<ComboboxItemDto>> LoaiTruong();
        Task<IEnumerable<ComboboxItemDto>> LoaiLop();
        Task<IEnumerable<ComboboxItemDto>> LoaiKiemTra();
    }
    #endregion
    #region Main
    public class ComboboxService : ClubAppServiceBase, IComboboxService
    {
        private readonly IApplicationServiceFactory _factory;
        public ComboboxService(IApplicationServiceFactory factory)
        {
            _factory = factory;
        }
        
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> LoaiStatus()
        {
            var sql = $@"select Id Value, Loai DisplayText from dboaistatus";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> Status(string Loai)
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbstatus";
            if (Loai != null)
            {
                sql += " Where LoaiId='" + Loai+"'";
            }
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> LoaiNote()
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbloai_notes";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> MonHoc()
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbmonhoc";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> PhanMon(int? MonId)
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbphanmon";
            if (MonId != null)
            {
                sql += " Where MonHocId='" + MonId+"'";
            }
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> CapHoc()
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbcaphoc";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> Truong()
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbtruong";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> LoaiTruong()
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbloaitruong";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> LoaiLop()
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbloailop";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> LoaiKiemTra()
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbtest";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
        [HttpGet]
        public async Task<IEnumerable<ComboboxItemDto>> LoaiTaiLieu()
        {
            var sql = $@"select Id Value, MoTa DisplayText from dbloaitailieu";
            var data = await _factory.UnitOfWordDapper.QueryAsync<ComboboxItemDto>(sql);
            return data;
        }
    }
    #endregion
}
