using System.ComponentModel.DataAnnotations;

namespace NewMath.Club.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}