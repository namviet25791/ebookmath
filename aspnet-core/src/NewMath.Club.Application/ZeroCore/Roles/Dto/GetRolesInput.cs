﻿namespace NewMath.Club.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}
