﻿using Abp.Application.Services.Dto;

namespace NewMath.Club.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

