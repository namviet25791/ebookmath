﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using NewMath.Club.Configuration.Dto;

namespace NewMath.Club.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ClubAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
