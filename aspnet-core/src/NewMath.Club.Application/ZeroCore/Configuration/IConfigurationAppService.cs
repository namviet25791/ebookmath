﻿using System.Threading.Tasks;
using NewMath.Club.Configuration.Dto;

namespace NewMath.Club.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
