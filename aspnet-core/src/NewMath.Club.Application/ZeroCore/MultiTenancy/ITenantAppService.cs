﻿using Abp.Application.Services;
using NewMath.Club.MultiTenancy.Dto;

namespace NewMath.Club.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

