﻿using System.Threading.Tasks;
using Abp.Application.Services;
using NewMath.Club.Sessions.Dto;

namespace NewMath.Club.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
