﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using NewMath.Club.MultiTenancy;

namespace NewMath.Club.Sessions.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantLoginInfoDto : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }
    }
}
