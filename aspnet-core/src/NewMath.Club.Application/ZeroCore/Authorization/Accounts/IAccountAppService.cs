﻿using System.Threading.Tasks;
using Abp.Application.Services;
using NewMath.Club.Authorization.Accounts.Dto;

namespace NewMath.Club.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
