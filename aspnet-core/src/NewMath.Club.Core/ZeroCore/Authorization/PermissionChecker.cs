﻿using Abp.Authorization;
using NewMath.Club.Authorization.Roles;
using NewMath.Club.Authorization.Users;

namespace NewMath.Club.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
