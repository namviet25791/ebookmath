﻿using Abp.MultiTenancy;
using NewMath.Club.Authorization.Users;

namespace NewMath.Club.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
