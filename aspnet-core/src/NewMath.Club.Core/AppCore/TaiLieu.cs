﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;


namespace NewMath.Club.DataTutor
{
    #region TaiLieu
    [Table("DbTaiLieu_BoSuuTap")]
    public class TaiLieuBoSuuTap : Entity<int>
    {
        public virtual string CodeId { get; set; }
        public virtual string Ten { get; set; }
        public virtual int UserId { get; set; }
        public virtual int? Lopso { get; set; }
        public virtual int? CapHocId { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? SotaiLieu { get; set; }
        public virtual int? LoaiSuuTapId { get; set; }
        public virtual int? SoNguoiTai { get; set; }
        public virtual int? SoNguoiMua { get; set; }
        public virtual int? Gia { get; set; }
        public virtual string MoTa { get; set; }
        public virtual int? DanhGia { get; set; }
        public virtual int? StatusId { get; set; }
    }
    
    [Table("DbTaiLieu_BoSuuTap_Chitiet")]
    public class TaiLieuBoSuuTapChiTiet : Entity<int>
    {
        public virtual int BoSuuTapId { get; set; }
        public virtual int TaiLieuId { get; set; }
    }
    [Table("DbTaiLieu")]
    public class TaiLieu : Entity<int>
    {
        public virtual int? UserId { get; set; }
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual string Ten { get; set; }
        public virtual int? LoaiTaiLieuId { get; set; }
        public virtual int? MucDo { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? CapHocId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? SoTrang { get; set; }
        public virtual string TacGia { get; set; }
        public virtual int? Nam { get; set; }
        public virtual int? Gia { get; set; }
        public virtual string Mota { get; set; }
        public virtual string LinkPdf { get; set; }
        public virtual int SoLuongTai { get; set; }
        public virtual int? DanhGia { get; set; }
        public virtual int? LoaiFile { get; set; }
        public virtual bool HavePro { get; set; }
    }
    [Table("DbTaiLieuPro")]
    public class TaiLieuPro : Entity<int>
    {
        public virtual int TaiLieuId { get; set; }
        public virtual string LinkPro { get; set; }
        public virtual string LinkPro2 { get; set; }
        public virtual string LinkPro3 { get; set; }
        public virtual int? SoLuongMua { get; set; }
        public virtual string MoTa { get; set; }
        public virtual int? LoaiFile { get; set; }
    }
    [Table("DbTaiLieu_NguoiMua")]
    public class TaiLieuNguoiMua : Entity<int>
    {
        public virtual int? BoSuuTapId { get; set; }
        public virtual int? TaiLieuId { get; set; }
        public virtual int? UserId { get; set; }
        [MaxLength(50)]
        public virtual string NguoiMua { get; set; }
        [MaxLength(20)]
        public virtual string NguoiMuaSoDT { get; set; }
        public virtual DateTime? ThoiGian_Mua { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual string Mota { get; set; }
    }
    #endregion

}
