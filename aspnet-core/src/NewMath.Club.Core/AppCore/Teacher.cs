﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;


namespace NewMath.Club.DataTutor
{
    /*Teacher*/
    [Table("DbGiaoVien")]
    public class GiaoVien:Entity<int>
    {
        public virtual int? UserId { get; set; }
        public virtual byte[] Avatar { get; set; }
        [MaxLength(50)]
        public virtual string Ten { get; set; }
        [MaxLength(50)]
        public virtual string Email { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual DateTime? NgaySinh { get; set; }
        public virtual int? Tuoi { get; set; }
        [MaxLength(200)]
        public virtual string TinhCach { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual string Mota { get; set; }
        [MaxLength(200)]
        public virtual string ThanhTichNoiBat { get; set; }
        [MaxLength(200)]
        public virtual string DiaChiLamViec { get; set; }
        [MaxLength(200)]
        public virtual string DiaChiNha { get; set; }
        [MaxLength(2)]
        public virtual int StatusId { get; set; }
        [MaxLength(3)]
        public virtual int? SchoolId { get; set; }
        [MaxLength(1)]
        public virtual int? BangCapId { get; set; }
        public virtual byte[] PhotoBang { get; set; }
        [MaxLength(1)]
        public virtual int? CongViecId { get; set; }
        [MaxLength(1)]
        public virtual int? StatusTimeId { get; set; }
        [MaxLength(20)]
        public virtual string DienThoai { get; set; }
    }
    [Table("DbGiaoVien_QuaKhu")]
    public class GiaoVienQuaKhu : Entity<int>
    {
        public virtual int GiaovienId { get; set; }
        [MaxLength(100)]
        public virtual string DiaChi_LamViec { get; set; }
        [MaxLength(100)]
        public virtual string DiaChi_LamViec2 { get; set; }
        [MaxLength(100)]
        public virtual string DiaChi_LamViec3 { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        [MaxLength(200)]
        public virtual string Mota { get; set; }
    }
    [Table("DbGiaoVien_ThanhTich")]
    public class GiaoVienThanhTich : Entity<int>
    {
        public virtual int GiaoVienId { get; set; }
        [MaxLength(50)]
        public virtual string TruongHoc { get; set; }
        [MaxLength(2)]
        public virtual int? LopSo { get; set; }
        public virtual int? Year { get; set; }
        [MaxLength(1)]
        public virtual int LoaiLopId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual string ThanhTich { get; set; }
        public virtual byte[] Photo { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbGiaoVien_BangCap")]
    public class GiaoVienBangCap : Entity<int>
    {
        public virtual int GiaoVienId { get; set; }
        [MaxLength(2)]
        public virtual int TruongHocId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        [MaxLength(1)]
        public virtual int BangCapId { get; set; }
        public virtual byte[] Photo { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbGiaoVien_KiNang")]
    public class GiaoVienKiNang : Entity<int>
    {
        public virtual int GiaoVienId { get; set; }
        [MaxLength(2)]
        public virtual int? LoaiLop { get; set; }
        public virtual int? LopId { get; set; }
        [MaxLength(1)]
        public virtual int? GioiTinh { get; set; }
    }
    [Table("DbGiaoVien_SoLuong")]
    public class GiaoVienSoLuong : Entity<int>
    {
        public virtual int GiaoVienId { get; set; }
        [MaxLength(1)]
        public virtual int? SoLuongId { get; set; }
    }
    [Table("DbGiaoVien_Can_Lop")]
    public class GiaoVienCoTheDay : Entity<int>
    {
        public virtual int GiaoVienId { get; set; }
        [MaxLength(100)]
        public virtual string DiaChi { get; set; }
        [MaxLength(3)]
        public virtual int? ThanhPho { get; set; }
        [MaxLength(3)]
        public virtual int? Quan { get; set; }
    }
    [Table("DbGiaoVien_FreeTime")]
    public class GiaoVienRanh : Entity<int>
    {
        public virtual int GiaoVienId { get; set; }
        [MaxLength(1)]
        public virtual int? Ngay { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
    }
    [Table("DbGiaoVien_Lich")]
    public class GiaoVienLich : Entity<int>
    {
        public virtual int GiaoVienId { get; set; }
        public virtual int? LopId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        [MaxLength(1)]
        public virtual int? Ngay { get; set; }
        [MaxLength(2)]
        public virtual int? MonHocId { get; set; }
    }
}
