﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;


namespace NewMath.Club.DataTutor
{
    /*Student*/



    [Table("DbHocSinh")]
    public class HocSinh:Entity<int>
    {
        public virtual int? UserId { get; set; }
        public virtual string Ten { get; set; }
        public virtual byte[] Avatar { get; set; }
        public virtual string TruongHoc { get; set; }
        public virtual string Email { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual DateTime? NgaySinh { get; set; }
        public virtual int? Tuoi { get; set; }
        public virtual string TinhCach { get; set; }
        public virtual string Mota { get; set; }
        public virtual string StatusId { get; set; }

    }
    [Table("DbHocSinh_QuaKhu")]
    public class HocSinhQuaKhu : Entity<int>
    {
        public virtual int HocSinhId { get; set; }
        public virtual string TruongHoc { get; set; }
        public virtual int LopSo { get; set; }
        public virtual string LoaiLopId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual string ThanhTich { get; set; }
        public virtual byte[] Photo { get; set; }
        public virtual string MoTa { get; set; }

    }
    [Table("DbHocSinh_QuaKhu_ChiTiet")]
    public class HocSinhQuaKhuChiTiet : Entity<int>
    {
        public virtual int QuaKhuId { get; set; }
        public virtual int MonHocId { get; set; }
        public virtual int Diem { get; set; }
        public virtual string MoTa { get; set; }

    }
    
    [Table("DbHocSinh_PhuHuynh")]
    public class HocSinhPhuHuynh : Entity<int>
    {
        public virtual int? HocSinhId { get; set; }
        public virtual int? PhuHuynhId { get; set; }
        public virtual int? MoiQuanHeId { get; set; }

    }

    [Table("DbHocSinh_LopHoc")]
    public class HocSinhLopHoc : Entity<int>
    {
        public virtual int? HocSinhId { get; set; }
        public virtual int? LopId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual int? StatusId { get; set; }
    }

    [Table("DbHocSinh_Diem")]
    public class HocSinhDiem : Entity<int>
    {
        public virtual int? HocSinhId { get; set; }
        public virtual int? LopId { get; set; }
        public virtual int? Diem { get; set; }
        public virtual int? TestId { get; set; }
        public virtual int? MoTa { get; set; }
    }
    [Table("DbHocSinh_LichHoc")]
    public class HocSinhLichHoc : Entity<int>
    {
        public virtual int HocSinhId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? ThoiGian_start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual int? LopId { get; set; }
        public virtual int? MonHoc { get; set; }
    }
}
