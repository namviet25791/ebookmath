﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;


namespace NewMath.Club.DataTutor
{
    /*Parent*/



    [Table("DbPhuHuynh")]
    public class PhuHuynh:Entity<int>
    {
        public virtual int? UserId { get; set; }
        public virtual string Ten { get; set; }
        public virtual byte[] Avatar { get; set; }
        public virtual string Email { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual DateTime? NgaySinh { get; set; }
        public virtual int? Tuoi { get; set; }
        public virtual string TinhCach { get; set; }
        public virtual string Mota { get; set; }
        public virtual string StatusId { get; set; }
    }
    [Table("DbPhuHynh_GiaoVien")]
    public class PhuHuynhGiaoVien:Entity<int>
    {
        public virtual int? PhuHuynhId { get; set; }
        public virtual string GiaoVienId { get; set; }
        public virtual string PhuHuynh_Notes { get; set; }
        public virtual int? Loai_NotesId { get; set; }
        public virtual DateTime? ThoiGian_Notes { get; set; }
    }
    [Table("DbPhuHynh_HocSinh")]
    public class PhuHuynhHocSinh : Entity<int>
    {
        public virtual int? PhuHuynhId { get; set; }
        public virtual string HocSinhId { get; set; }
        public virtual string PhuHuynh_Notes { get; set; }
        public virtual int? Loai_NotesId { get; set; }
        public virtual DateTime? ThoiGian_Notes { get; set; }
    }
}
