﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;


namespace NewMath.Club.DataTutor
{
    [Table("DbLoaiStatus")]
    public class LoaiStatus : Entity<int>
    {
        [Required]
        public virtual string Loai { get; set; }
    }
    [Table("DbLoaiTaiLieu")]
    public class LoaiTaiLieu : Entity<int>
    {
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbStatus")]
    public class Status : Entity<int>
    {
        [Required]
        public virtual string LoaiId { get; set; }
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbLoai_Notes")]
    public class LoaiNotes : Entity<int>
    {
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbMonHoc")]
    public class MonHoc:Entity<int>
    {
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbPhanMon")]
    public class PhanMon : Entity<int>
    {
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbCapHoc")]
    public class CapHoc:Entity<int>
    {
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbTruong")]
    public class Truong : Entity<int>
    {
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
        public virtual string DiaChi { get; set; }
    }
    [Table("DbLoaiTruong")]
    public class LoaiTruong : Entity<int>
    {
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbLoaiLop")]
    public class LoaiLop : Entity<int>
    {
        [Required]
        public virtual string CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbAnh")]
    public class Anh : Entity<int>
    {
        [Required]
        public virtual int UserId { get; set; }
        public virtual byte[] Photo { get; set; }
    }
    [Table("DbDiaChi")]
    public class DiaChi : Entity<int>
    {
        [Required]
        public virtual int UserId { get; set; }
        public virtual int? LoaiDiaChi { get; set; }

        public virtual string DiaChi_User { get; set; }
        public virtual decimal? ToaDoX { get; set; }
        public virtual decimal? ToaDoY { get; set; }

    }

    [Table("DbTest")]
    public class Test : Entity<int>
    {
        [Required]
        public virtual int CodeId { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbMobile")]
    public class Mobile : Entity<int>
    {
        [Required]
        public virtual int UserId { get; set; }
        public virtual int DienThoaiLoai { get; set; }
        public virtual string DienThoai { get; set; }
    }
    [Table("DanhMucHuyen")]
    public class DanhMucHuyenEntity : Entity<int>
    {
        [Required]
        [MaxLength(10)]
        public string MaHuyen { get; set; }
        [Required]
        [MaxLength(200)]
        public string TenHuyen { get; set; }
        [Required]
        [MaxLength(10)]
        public string MaTinh { get; set; }
        [Required]
        [MaxLength(200)]
        public string TenTinh { get; set; }
        [MaxLength(200)]
        public string Cap { get; set; }
    }
    [Table("DanhMucTinh")]
    public class DanhMucTinhEntity : Entity<int>
    {
        [Required]
        [MaxLength(10)]
        public string MaTinh { get; set; }
        [Required]
        [MaxLength(200)]
        public string TenTinh { get; set; }
        [MaxLength(200)]
        public string Cap { get; set; }
    }
    [Table("DanhMucXa")]
    public class DanhMucXaEntity : Entity<int>
    {
        [Required]
        [MaxLength(10)]
        public string MaXa { get; set; }
        [Required]
        [MaxLength(200)]
        public string TenXa { get; set; }
        [Required]
        [MaxLength(10)]
        public string MaTinh { get; set; }
        [Required]
        [MaxLength(200)]
        public string TenTinh { get; set; }
        [Required]
        [MaxLength(10)]
        public string MaHuyen { get; set; }
        [Required]
        [MaxLength(200)]
        public string TenHuyen { get; set; }
        [MaxLength(200)]
        public string Cap { get; set; }
    }
}
