﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;


namespace NewMath.Club.DataTutor
{
    #region TaiLieu
    [Table("DbBaiViet")]
    public class BaiViet : Entity<int>
    {
        public virtual string TieuDe { get; set; }
        public virtual int UserId { get; set; }
        public virtual string TomTat { get; set; }
        public virtual string NoiDung { get; set; }
        public virtual string KetLuan { get; set; }
        public virtual int BaiThu { get; set; }
        public virtual int BoSuuTapId { get; set; }
        public virtual DateTime NgayViet { get; set; }
    }
    [Table("DbBoSuuTapBaiViet")]
    public class BoSuuTapBaiViet : Entity<int>
    {
        public virtual int? ChuDeId { get; set; }
        [MaxLength(30)]
        public virtual string Ten { get; set; }
        [MaxLength(200)]
        public virtual string TomTat { get; set; }
    }
    [Table("DbChuDe_BaiViet")]
    public class ChuDeBaiViet : Entity<int>
    {
        [MaxLength(30)]
        public virtual string Code { get; set; }
        [MaxLength(100)]
        public virtual string Ten { get; set; }
        [MaxLength(200)]
        public virtual string MoTa { get; set; }
    }
    #endregion

}
