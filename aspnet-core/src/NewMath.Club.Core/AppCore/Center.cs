﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;


namespace NewMath.Club.DataTutor
{
    #region NhanVien

    [Table("DbNhanVien")]
    public class NhanVien : Entity<int>
    {
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual int CongViecId { get; set; }
        [MaxLength(50)]
        public virtual string Ten { get; set; }
        public virtual int UserId { get; set; }
        public virtual int? TruongId { get; set; }
        public virtual byte[] Avatar { get; set; }
       
        [MaxLength(50)]
        public virtual string Email { get; set; }
        public virtual int? GioiTinh { get; set; }
        public virtual DateTime? NgaySinh { get; set; }
        public virtual int? Tuoi { get; set; }
        public virtual string TinhCach { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoiGian_End { get; set; }
        public virtual int? Luong { get; set; }
        public virtual int StatusId { get; set; }
        public virtual string Mota { get; set; }
        public virtual string DiaChi { get; set; }

    }
    [Table("DbNhanVien_Luong")]
    public class NhanVienLuong : Entity<int>
    {
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        [MaxLength(50)]
        public virtual int NhanVienId { get; set; }
        public virtual int? Luong { get; set; }
        public virtual int? ThieuLuong { get; set; }
        public virtual int? Year { get; set; }
        public virtual int? Month { get; set; }
        public virtual DateTime? ThoiGian_Tra { get; set; }
        public virtual int? StatudId { get; set; }
        public virtual string Mota { get; set; }
    }
    [Table("DbNhanVien_GiaoVien_LopHoc")]
    public class NhanVienGiaoVienLopHocPhi : Entity<int>
    {
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual int NhanVienId { get; set; }
        public virtual int GiaoVienId { get; set; }
        public virtual int? LopGiaSu { get; set; }
        public virtual int? LopNhom { get; set; }
        public virtual int? LopLon { get; set; }
        public virtual int? Phi { get; set; }
        public virtual int? NoPhi { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? Month { get; set; }
        public virtual int? Year { get; set; }
        public virtual DateTime? ThoiGian_Tra { get; set; }
        public virtual DateTime? ThoiGian_HenTra { get; set; }
        public virtual DateTime? ThoiGian_KichHoat { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbTrungTam_HoiThoai")]
    public class TrungTamHoiThoai : Entity<int>
    {
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual int GiaoVienId { get; set; }
        public virtual int NhanVienId { get; set; }
        public virtual int? PHuHuynhId { get; set; }
        public virtual int? HocSinhId { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual bool? ResultStatusId { get; set; }
        public virtual DateTime? ThoiGian_Start { get; set; }
        public virtual DateTime? ThoIGian_Ent { get; set; }
        public virtual string MoTa { get; set; }
        public virtual string MoTaKetQua { get; set; }
        [MaxLength(100)]
        public virtual string CodeZalo { get; set; }
    }

    [Table("DbTrungTam_CuocHen")]
    public class TrungTamCuocGap : Entity<int>
    {
        public virtual int? GiaoVienId { get; set; }
        public virtual int? NhanVienId { get; set; }
        public virtual int? PhuHuynhId { get; set; }
        public virtual int? HocSinhId { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int Loai_DiaChiId { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual DateTime? ThoiGian_KeHoach { get; set; }
        public virtual DateTime? ThoiGian_Gap { get; set; }
        public virtual string Mota { get; set; }
        public virtual bool? ResultStatusId { get; set; }
        public virtual string MoTaKetQua { get; set; }

    }
    #endregion
    [Table("DbTrungTam_KeHoach_DayHoc")]
    public class TrungTamKeHoachDayHoc : Entity<int>
    {
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLop { get; set; }
        public virtual int? MonHoc { get; set; }
        public virtual int? PhanMon { get; set; }
        public virtual string Mota { get; set; }
    }
    [Table("DbTrungTam_KeHoach_ChiTiet")]
    public class KeHoachChiTiet : Entity<int>
    {
        public virtual int? KeHoachId { get; set; }
        public virtual int? BuoiSo { get; set; }
        public virtual string Mota { get; set; }
    }

}
