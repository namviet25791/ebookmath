﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;


namespace NewMath.Club.DataTutor
{
    /*Class*/


    [Table("DbLop_YeuCau_ChuaHoatDong")]
    public class LopYeuCau : Entity<int>
    {
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual int? UserId { get; set; }
        //ThongTinLopYeCau
        public virtual int? LoaiLopId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int MonHocId { get; set; }
        public virtual DateTime? ThoiGian_YeuCau { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? Gia_Start { get; set; }
        public virtual int? Gia_End { get; set; }
        public virtual int? AddressId { get; set; }
        //NguoiYeuCau
        [MaxLength(50)]
        public virtual string TenNguoiYeuCau { get; set; }
        [MaxLength(100)]
        public virtual string Address { get; set; }
        [MaxLength(20)]
        public virtual string Phone { get; set; }
        public virtual int GioiTinhNguoiYeuCau { get; set; }
        public virtual int? SoLuongId { get; set; }
        public virtual string YeuCauChung { get; set; }
        public virtual string YeuCauGiaoVien { get; set; }
        public virtual string MoTaHocSinh { get; set; }
        [MaxLength(50)]
        public virtual string TenHocSinh { get; set; }
        public virtual int GioiTinhHocSinh { get; set; }
        [MaxLength(100)]
        public virtual string HocLucHocSinh { get; set; }
        [MaxLength(100)]
        public virtual string TruongHocSinh { get; set; }
        //NhanVien
        public virtual string DanhGiaNguoiYeuCau { get; set; }
        [MaxLength(50)]
        public virtual string DanhSachGiaoVienId { get; set; }

    }
    [Table("DbLop_HoatDong_GiaSu")]
    public class LopHoatDongGiaSu : Entity<int>
    {
        [MaxLength(50)]
        public virtual string Ten { get; set; }
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? Thoigian_Start { get; set; }
        public virtual DateTime? Thoigian_End { get; set; }
        public virtual int? GiaovienId { get; set; }
        public virtual int? Gia { get; set; }
        public virtual int? DiaChiId { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? SoLuong { get; set; }
        public virtual DateTime? ThoiGian_BatDau { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbLop_HoatDong_Nhom")]
    public class LopHoatDongNhomDto : Entity<int>
    {
        [MaxLength(50)]
        public virtual string Ten { get; set; }
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? Thoigian_Start { get; set; }
        public virtual DateTime? Thoigian_End { get; set; }
        public virtual int? GiaovienId { get; set; }
        public virtual int? Gia { get; set; }
        public virtual int? DiaChiId { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? SoLuong { get; set; }
        public virtual int? SoLuong_Max { get; set; }
        public virtual DateTime? ThoiGian_BatDau { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbLop_HoatDong_Nhom")]
    public class LopHoatDongNhom : Entity<int>
    {
        [MaxLength(50)]
        public virtual string Ten { get; set; }
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? Thoigian_Start { get; set; }
        public virtual DateTime? Thoigian_End { get; set; }
        public virtual int? GiaovienId { get; set; }
        public virtual int? Gia { get; set; }
        public virtual int? DiaChiId { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? SoLuong { get; set; }
        public virtual int? SoLuong_Max { get; set; }
        public virtual DateTime? ThoiGian_BatDau { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbLop_HoatDong_Lon")]
    public class LopHoatDongLon : Entity<int>
    {
        [MaxLength(50)]
        public virtual string Ten { get; set; }
        [MaxLength(20)]
        public virtual string CodeId { get; set; }
        public virtual int? LopSo { get; set; }
        public virtual int? LoaiLopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? Thoigian_Start { get; set; }
        public virtual DateTime? Thoigian_End { get; set; }
        public virtual int? GiaovienId { get; set; }
        public virtual int? Gia { get; set; }
        public virtual int? DiaChiId { get; set; }
        public virtual string DiaChi { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? SoLuong { get; set; }
        public virtual int? SoLuong_Max { get; set; }
        public virtual DateTime? ThoiGian_BatDau { get; set; }
        public virtual string MoTa { get; set; }
    }

    [Table("DbLop_BuoiHoc")]
    public class LopBuoiHoc : Entity<int>
    {
        public virtual string Name { get; set; }
        public virtual int LopId { get; set; }
        public virtual int? BuoiSo { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual int? Ngay { get; set; }
        public virtual DateTime? Thoigian_Start { get; set; }
        public virtual DateTime? Thoigian_End { get; set; }
        public virtual string BTVN { get; set; }
        public virtual int? SoLuong_Hoc { get; set; }
        public virtual int? SoLuong_nghi { get; set; }
        public virtual string PhanMonId { get; set; }
        public virtual string SoSanh_KeHoach { get; set; }
        public virtual string MoTa { get; set; }
    }
    [Table("DbLop_KeHoach")]
    public class LopKeHoach : Entity<int>
    {
        public virtual int? LopId { get; set; }
        public virtual int? BaiSo { get; set; }
        public virtual int? PhanMonId { get; set; }
        public virtual string Mota { get; set; }
    }
    [Table("DbLop_HocSinh")]
    public class LopHocSinh : Entity<int>
    {
        public virtual int? LopId { get; set; }
        public virtual int? HocSinhId { get; set; }
        public virtual int? StatusId { get; set; }
    }

    [Table("DbBuoiHoc_HocSinh")]
    public class BuoiHocSinh : Entity<int>
    {
        public virtual int? BuoiHocId { get; set; }
        public virtual int? HocSinhId { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual string BTVH_Truoc { get; set; }
        public virtual bool PhuHuynh_DongY { get; set; }
        public virtual string LiDo_Nghi { get; set; }
        public virtual string Loai_NotesId_PH { get; set; }
        public virtual string Loai_NotesId_GV { get; set; }
        public virtual string GiaoVien_Notes { get; set; }
        public virtual string PhuHuynh_Notes { get; set; }
    }
    /*Assess*/
    [Table("DbSoLienLac")]
    public class SoLienLac : Entity<int>
    {
        public virtual int? HocSinhId { get; set; }
        public virtual int? LopId { get; set; }
        public virtual int? MonHocId { get; set; }
        public virtual int? Year { get; set; }
        public virtual int? Month { get; set; }
        public virtual DateTime? Ngay_Notes { get; set; }
        public virtual string Loai_NotesId_PH { get; set; }
        public virtual string Loai_NotesId_GV { get; set; }
        public virtual string GiaoVien_Notes { get; set; }
        public virtual string PhuHuynh_Notes { get; set; }
    }
}
