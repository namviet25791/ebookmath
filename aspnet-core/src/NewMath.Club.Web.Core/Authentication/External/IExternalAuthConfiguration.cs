﻿using System.Collections.Generic;

namespace NewMath.Club.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
