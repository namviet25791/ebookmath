﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using NewMath.Club.Authorization.Roles;
using NewMath.Club.Authorization.Users;
using NewMath.Club.MultiTenancy;
using NewMath.Club.DataTutor;

namespace NewMath.Club.EntityFrameworkCore
{
    public class ClubDbContext : AbpZeroDbContext<Tenant, Role, User, ClubDbContext>
    {
        #region GiaoVien
        public virtual DbSet<GiaoVien> GiaoViens { get; set; }
        public virtual DbSet<GiaoVienQuaKhu> GiaoVienQuaKhus { get; set; }
        public virtual DbSet<GiaoVienThanhTich> GiaoVienThanhTichs { get; set; }
        public virtual DbSet<GiaoVienBangCap> GiaoVienBangCaps { get; set; }
        public virtual DbSet<GiaoVienKiNang> GiaoVienKiNangs { get; set; }
        public virtual DbSet<GiaoVienSoLuong> GiaoVienSoLuongs { get; set; }
        public virtual DbSet<GiaoVienCoTheDay> GiaoVienCoTheDays { get; set; }
        public virtual DbSet<GiaoVienRanh> GiaoVienRanhs { get; set; }
        public virtual DbSet<GiaoVienLich> GiaoVienLichs { get; set; }
        #endregion
        public virtual DbSet<HocSinh> HocSinhs { get; set; }
        public virtual DbSet<HocSinhQuaKhu> HocSinhQuaKhus { get; set; }
        public virtual DbSet<HocSinhQuaKhuChiTiet> HocSinhQuaKhuChiTiets { get; set; }
        public virtual DbSet<HocSinhPhuHuynh> HocSinhPhuHuynhs { get; set; }
        public virtual DbSet<HocSinhLopHoc> HocSinhLopHocs { get; set; }
        public virtual DbSet<HocSinhDiem> HocSinhDiems { get; set; }
        public virtual DbSet<HocSinhLichHoc> HocSinhLichHocs { get; set; }
        public virtual DbSet<PhuHuynh> PhuHuynhs { get; set; }
        public virtual DbSet<PhuHuynhGiaoVien> PhuHuynhGiaoViens { get; set; }
        public virtual DbSet<PhuHuynhHocSinh> PhuHuynhHocSinhs { get; set; }
        #region LopHoc
        public virtual DbSet<LopYeuCau> LopYeuCaus { get; set; }
        public virtual DbSet<LopHoatDongGiaSu> LopHoatDongs { get; set; }
        public virtual DbSet<LopHoatDongNhom> LopHoatDongNhoms { get; set; }
        public virtual DbSet<LopHoatDongLon> LopHoatDongLons { get; set; }
        public virtual DbSet<LopBuoiHoc> LopBuoiHocs { get; set; }
        public virtual DbSet<LopKeHoach> LopKeHoachs { get; set; }
        public virtual DbSet<LopHocSinh> LopHocSinhs { get; set; }
        public virtual DbSet<BuoiHocSinh> BuoiHocSinhs { get; set; }
        public virtual DbSet<SoLienLac> SoLienLacs { get; set; }
        #endregion
        #region HoiThoai
        public virtual DbSet<TrungTamHoiThoai> TrungTamHoiThoais { get; set; }
        public virtual DbSet<TrungTamCuocGap> TrungTamCuocGaps { get; set; }
        #endregion
        public virtual DbSet<NhanVien> NhanViens { get; set; }
        public virtual DbSet<NhanVienLuong> NhanVienLuongs { get; set; }
        public virtual DbSet<NhanVienGiaoVienLopHocPhi> NhanVienGiaoVienLopHocs { get; set; }
        
        public virtual DbSet<TaiLieuBoSuuTap> TaiLieuBoSuuTaps { get; set; }
        public virtual DbSet<TaiLieuBoSuuTapChiTiet> TaiLieuBoSuuTapChiTiets { get; set; }
        public virtual DbSet<TaiLieu> TaiLieus { get; set; }
        public virtual DbSet<TaiLieuNguoiMua> TaiLieuNguoiMuas { get; set; }
        public virtual DbSet<TrungTamKeHoachDayHoc> TrungTamKeHoachDayHocs { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }
        public virtual DbSet<LoaiStatus> LoaiStatuses { get; set; }
        public virtual DbSet<LoaiNotes> LoaiNoteses { get; set; }
        public virtual DbSet<MonHoc> MonHocs { get; set; }
        public virtual DbSet<PhanMon> PhanMons { get; set; }
        public virtual DbSet<CapHoc> CapHocs { get; set; }
        public virtual DbSet<LoaiTruong> LoaiTruongs { get; set; }
        public virtual DbSet<Truong> Truongs { get; set; }
        public virtual DbSet<LoaiLop> LoaiLops { get; set; }
        public virtual DbSet<Anh> Anhs { get; set; }
        public virtual DbSet<DiaChi> DiaChis { get; set; }
        public virtual DbSet<Test> Tests { get; set; }
        public virtual DbSet<Mobile> Mobiles { get; set; }
        public virtual DbSet<DanhMucHuyenEntity> DanhMucHuyenEntitys { get; set; }
        public virtual DbSet<DanhMucTinhEntity> DanhMucTinhEntitys { get; set; }
        public virtual DbSet<DanhMucXaEntity> DanhMucXaEntitys { get; set; }
        public virtual DbSet<LoaiTaiLieu> LoaiTaiLieus { get; set; }
        //BaiViet
        public virtual DbSet<BaiViet> BaiViets { get; set; }
        public virtual DbSet<BoSuuTapBaiViet> BoSuuTapBaiViets { get; set; }
        public virtual DbSet<ChuDeBaiViet> ChuDeBaiViets { get; set; }

        public ClubDbContext(DbContextOptions<ClubDbContext> options)
            : base(options)
        {
        }
    }
}
