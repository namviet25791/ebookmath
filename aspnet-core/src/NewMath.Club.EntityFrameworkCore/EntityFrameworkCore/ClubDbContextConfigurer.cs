using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace NewMath.Club.EntityFrameworkCore
{
    public static class ClubDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ClubDbContext> builder, string connectionString)
        {
            builder.UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ClubDbContext> builder, DbConnection connection)
        {
            builder.UseMySql(connection);
        }
    }
}
