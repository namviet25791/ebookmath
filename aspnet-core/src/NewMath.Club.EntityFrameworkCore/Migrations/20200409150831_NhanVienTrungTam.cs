﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class NhanVienTrungTam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gia",
                table: "DbNhanVien_Luong");

            migrationBuilder.DropColumn(
                name: "SalaryId",
                table: "DbNhanVien_Luong");

            migrationBuilder.DropColumn(
                name: "Gia",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "LopId",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "Salary",
                table: "DbNhanVien");

            migrationBuilder.AlterColumn<int>(
                name: "NhanVienId",
                table: "DbTrungTam_HoiThoai",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GiaoVienId",
                table: "DbTrungTam_HoiThoai",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CodeZalo",
                table: "DbTrungTam_HoiThoai",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CodeId",
                table: "DbTrungTam_HoiThoai",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MoTaKetQua",
                table: "DbTrungTam_HoiThoai",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ResultStatusId",
                table: "DbTrungTam_HoiThoai",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MoTaKetQua",
                table: "DbTrungTam_CuocHen",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ResultStatusId",
                table: "DbTrungTam_CuocHen",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NhanVienId",
                table: "DbNhanVien_Luong",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CodeId",
                table: "DbNhanVien_Luong",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Luong",
                table: "DbNhanVien_Luong",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ThieuLuong",
                table: "DbNhanVien_Luong",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NhanVienId",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GiaoVienId",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CodeId",
                table: "DbNhanVien_GiaoVien_LopHoc",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LopGiaSu",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LopLon",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LopNhom",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NoPhi",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Phi",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ThoiGian_HenTra",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ThoiGian_KichHoat",
                table: "DbNhanVien_GiaoVien_LopHoc",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "StatusId",
                table: "DbNhanVien",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "DbNhanVien",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CodeId",
                table: "DbNhanVien",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Luong",
                table: "DbNhanVien",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ten",
                table: "DbNhanVien",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ThoiGian_End",
                table: "DbNhanVien",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "YeuCauGiaoVien",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100) CHARACTER SET utf8mb4",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "YeuCauChung",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100) CHARACTER SET utf8mb4",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTaHocSinh",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100) CHARACTER SET utf8mb4",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DanhGiaNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100) CHARACTER SET utf8mb4",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_Nhom",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100) CHARACTER SET utf8mb4",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_Lon",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(200) CHARACTER SET utf8mb4",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_GiaSu",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(200) CHARACTER SET utf8mb4",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ThanhTich",
                table: "DbGiaoVien_ThanhTich",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(200) CHARACTER SET utf8mb4",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbGiaoVien_ThanhTich",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(200) CHARACTER SET utf8mb4",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbGiaoVien_BangCap",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(200) CHARACTER SET utf8mb4",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Mota",
                table: "DbGiaoVien",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(200) CHARACTER SET utf8mb4",
                oldMaxLength: 200,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CodeId",
                table: "DbTrungTam_HoiThoai");

            migrationBuilder.DropColumn(
                name: "MoTaKetQua",
                table: "DbTrungTam_HoiThoai");

            migrationBuilder.DropColumn(
                name: "ResultStatusId",
                table: "DbTrungTam_HoiThoai");

            migrationBuilder.DropColumn(
                name: "MoTaKetQua",
                table: "DbTrungTam_CuocHen");

            migrationBuilder.DropColumn(
                name: "ResultStatusId",
                table: "DbTrungTam_CuocHen");

            migrationBuilder.DropColumn(
                name: "CodeId",
                table: "DbNhanVien_Luong");

            migrationBuilder.DropColumn(
                name: "Luong",
                table: "DbNhanVien_Luong");

            migrationBuilder.DropColumn(
                name: "ThieuLuong",
                table: "DbNhanVien_Luong");

            migrationBuilder.DropColumn(
                name: "CodeId",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "LopGiaSu",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "LopLon",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "LopNhom",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "NoPhi",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "Phi",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "ThoiGian_HenTra",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "ThoiGian_KichHoat",
                table: "DbNhanVien_GiaoVien_LopHoc");

            migrationBuilder.DropColumn(
                name: "CodeId",
                table: "DbNhanVien");

            migrationBuilder.DropColumn(
                name: "Luong",
                table: "DbNhanVien");

            migrationBuilder.DropColumn(
                name: "Ten",
                table: "DbNhanVien");

            migrationBuilder.DropColumn(
                name: "ThoiGian_End",
                table: "DbNhanVien");

            migrationBuilder.AlterColumn<int>(
                name: "NhanVienId",
                table: "DbTrungTam_HoiThoai",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GiaoVienId",
                table: "DbTrungTam_HoiThoai",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CodeZalo",
                table: "DbTrungTam_HoiThoai",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NhanVienId",
                table: "DbNhanVien_Luong",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(int),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<int>(
                name: "Gia",
                table: "DbNhanVien_Luong",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SalaryId",
                table: "DbNhanVien_Luong",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NhanVienId",
                table: "DbNhanVien_GiaoVien_LopHoc",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GiaoVienId",
                table: "DbNhanVien_GiaoVien_LopHoc",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Gia",
                table: "DbNhanVien_GiaoVien_LopHoc",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LopId",
                table: "DbNhanVien_GiaoVien_LopHoc",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StatusId",
                table: "DbNhanVien",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "DbNhanVien",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Salary",
                table: "DbNhanVien",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "YeuCauGiaoVien",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "varchar(100) CHARACTER SET utf8mb4",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "YeuCauChung",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "varchar(100) CHARACTER SET utf8mb4",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTaHocSinh",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "varchar(100) CHARACTER SET utf8mb4",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DanhGiaNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "varchar(100) CHARACTER SET utf8mb4",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_Nhom",
                type: "varchar(100) CHARACTER SET utf8mb4",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_Lon",
                type: "varchar(200) CHARACTER SET utf8mb4",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_GiaSu",
                type: "varchar(200) CHARACTER SET utf8mb4",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ThanhTich",
                table: "DbGiaoVien_ThanhTich",
                type: "varchar(200) CHARACTER SET utf8mb4",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbGiaoVien_ThanhTich",
                type: "varchar(200) CHARACTER SET utf8mb4",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbGiaoVien_BangCap",
                type: "varchar(200) CHARACTER SET utf8mb4",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Mota",
                table: "DbGiaoVien",
                type: "varchar(200) CHARACTER SET utf8mb4",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
