﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class Sua_Tailie_ThemBaiViet4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "DbTaiLieu_BoSuuTap",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Ten",
                table: "DbTaiLieu",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "Ten",
                table: "DbTaiLieu");
        }
    }
}
