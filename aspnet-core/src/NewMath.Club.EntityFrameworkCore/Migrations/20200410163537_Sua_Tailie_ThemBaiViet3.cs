﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class Sua_Tailie_ThemBaiViet3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Ten",
                table: "DbTaiLieu_BoSuuTap",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Ten",
                table: "DbTaiLieu_BoSuuTap");
        }
    }
}
