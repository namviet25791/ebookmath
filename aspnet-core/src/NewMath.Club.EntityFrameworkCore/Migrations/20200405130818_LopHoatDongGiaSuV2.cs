﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class LopHoatDongGiaSuV2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SoLuong_Max",
                table: "DbLop_HoatDong_GiaSu");

            migrationBuilder.AlterColumn<string>(
                name: "TenNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(100) CHARACTER SET utf8mb4",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenHocSinh",
                table: "DbLop_YeuCau_ChuaHoatDong",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Ten",
                table: "DbLop_HoatDong_Nhom",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_Nhom",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Ten",
                table: "DbLop_HoatDong_Lon",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_Lon",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Ten",
                table: "DbLop_HoatDong_GiaSu",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MoTa",
                table: "DbLop_HoatDong_GiaSu",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CodeId",
                table: "DbLop_HoatDong_GiaSu",
                maxLength: 20,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenHocSinh",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.DropColumn(
                name: "CodeId",
                table: "DbLop_HoatDong_GiaSu");

            migrationBuilder.AlterColumn<string>(
                name: "TenNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "varchar(100) CHARACTER SET utf8mb4",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Ten",
                table: "DbLop_HoatDong_Nhom",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MoTa",
                table: "DbLop_HoatDong_Nhom",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Ten",
                table: "DbLop_HoatDong_Lon",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MoTa",
                table: "DbLop_HoatDong_Lon",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Ten",
                table: "DbLop_HoatDong_GiaSu",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MoTa",
                table: "DbLop_HoatDong_GiaSu",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SoLuong_Max",
                table: "DbLop_HoatDong_GiaSu",
                type: "int",
                nullable: true);
        }
    }
}
