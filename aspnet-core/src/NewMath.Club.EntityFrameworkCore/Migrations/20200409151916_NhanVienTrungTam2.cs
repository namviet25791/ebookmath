﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class NhanVienTrungTam2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MonHocId",
                table: "DbNhanVien");

            migrationBuilder.AddColumn<int>(
                name: "CongViecId",
                table: "DbNhanVien",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DiaChi",
                table: "DbNhanVien",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TruongId",
                table: "DbNhanVien",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CongViecId",
                table: "DbNhanVien");

            migrationBuilder.DropColumn(
                name: "DiaChi",
                table: "DbNhanVien");

            migrationBuilder.DropColumn(
                name: "TruongId",
                table: "DbNhanVien");

            migrationBuilder.AddColumn<int>(
                name: "MonHocId",
                table: "DbNhanVien",
                type: "int",
                nullable: true);
        }
    }
}
