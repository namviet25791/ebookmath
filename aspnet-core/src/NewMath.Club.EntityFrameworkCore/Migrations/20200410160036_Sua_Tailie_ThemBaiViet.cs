﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class Sua_Tailie_ThemBaiViet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LoaiLop",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "LoaiSuuTap",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "LoaiLop",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "LoaiTaiLieu",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "MonHoc",
                table: "DbTaiLieu");

            migrationBuilder.AddColumn<int>(
                name: "LoaiLopId",
                table: "DbTaiLieu_BoSuuTap",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiSuuTapId",
                table: "DbTaiLieu_BoSuuTap",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MonHocId",
                table: "DbTaiLieu_BoSuuTap",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HavePro",
                table: "DbTaiLieu",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "LoaiLopId",
                table: "DbTaiLieu",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiTaiLieuId",
                table: "DbTaiLieu",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MonHocId",
                table: "DbTaiLieu",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MucDo",
                table: "DbTaiLieu",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LoaiLopId",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "LoaiSuuTapId",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "MonHocId",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "HavePro",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "LoaiLopId",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "LoaiTaiLieuId",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "MonHocId",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "MucDo",
                table: "DbTaiLieu");

            migrationBuilder.AddColumn<int>(
                name: "LoaiLop",
                table: "DbTaiLieu_BoSuuTap",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiSuuTap",
                table: "DbTaiLieu_BoSuuTap",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiLop",
                table: "DbTaiLieu",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiTaiLieu",
                table: "DbTaiLieu",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MonHoc",
                table: "DbTaiLieu",
                type: "int",
                nullable: true);
        }
    }
}
