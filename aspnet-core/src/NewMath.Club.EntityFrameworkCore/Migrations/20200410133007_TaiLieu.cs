﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class TaiLieu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NhanVienId",
                table: "DbTaiLieu_NguoiMua");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "DbTaiLieu_NguoiMua");

            migrationBuilder.DropColumn(
                name: "Link",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "Link",
                table: "DbTaiLieu");

            migrationBuilder.AddColumn<string>(
                name: "NguoiMua",
                table: "DbTaiLieu_NguoiMua",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NguoiMuaSoDT",
                table: "DbTaiLieu_NguoiMua",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TaiLieuId",
                table: "DbTaiLieu_BoSuuTap_Chitiet",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BoSuuTapId",
                table: "DbTaiLieu_BoSuuTap_Chitiet",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LoaiSuuTap",
                table: "DbTaiLieu_BoSuuTap",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DanhGia",
                table: "DbTaiLieu_BoSuuTap",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SoNguoiTai",
                table: "DbTaiLieu_BoSuuTap",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "DbTaiLieu_BoSuuTap",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "DbTaiLieu",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CodeId",
                table: "DbTaiLieu",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DanhGia",
                table: "DbTaiLieu",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LinkPdf",
                table: "DbTaiLieu",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LoaiFile",
                table: "DbTaiLieu",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SoLuongTai",
                table: "DbTaiLieu",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NguoiMua",
                table: "DbTaiLieu_NguoiMua");

            migrationBuilder.DropColumn(
                name: "NguoiMuaSoDT",
                table: "DbTaiLieu_NguoiMua");

            migrationBuilder.DropColumn(
                name: "DanhGia",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "SoNguoiTai",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "DbTaiLieu_BoSuuTap");

            migrationBuilder.DropColumn(
                name: "DanhGia",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "LinkPdf",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "LoaiFile",
                table: "DbTaiLieu");

            migrationBuilder.DropColumn(
                name: "SoLuongTai",
                table: "DbTaiLieu");

            migrationBuilder.AddColumn<int>(
                name: "NhanVienId",
                table: "DbTaiLieu_NguoiMua",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RoleId",
                table: "DbTaiLieu_NguoiMua",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TaiLieuId",
                table: "DbTaiLieu_BoSuuTap_Chitiet",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "BoSuuTapId",
                table: "DbTaiLieu_BoSuuTap_Chitiet",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "LoaiSuuTap",
                table: "DbTaiLieu_BoSuuTap",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Link",
                table: "DbTaiLieu_BoSuuTap",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "DbTaiLieu",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CodeId",
                table: "DbTaiLieu",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Link",
                table: "DbTaiLieu",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
