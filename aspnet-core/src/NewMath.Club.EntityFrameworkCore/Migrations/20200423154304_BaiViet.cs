﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class BaiViet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbBaiViet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TieuDe = table.Column<string>(nullable: true),
                    TomTat = table.Column<string>(nullable: true),
                    NoiDung = table.Column<string>(nullable: true),
                    KetLuan = table.Column<string>(nullable: true),
                    BaiThu = table.Column<int>(nullable: false),
                    NgayViet = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbBaiViet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbBoSuuTapBaiViet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ChuDeId = table.Column<int>(nullable: true),
                    Ten = table.Column<string>(maxLength: 30, nullable: true),
                    TomTat = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbBoSuuTapBaiViet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbChuDe_BaiViet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 30, nullable: true),
                    Ten = table.Column<string>(maxLength: 100, nullable: true),
                    MoTa = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbChuDe_BaiViet", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbBaiViet");

            migrationBuilder.DropTable(
                name: "DbBoSuuTapBaiViet");

            migrationBuilder.DropTable(
                name: "DbChuDe_BaiViet");
        }
    }
}
