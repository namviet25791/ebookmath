﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class Edit_collumn_Class : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CodeId",
                table: "DbLop_HoatDong_Nhom",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CodeId",
                table: "DbLop_HoatDong_Lon",
                maxLength: 20,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CodeId",
                table: "DbLop_HoatDong_Nhom");

            migrationBuilder.DropColumn(
                name: "CodeId",
                table: "DbLop_HoatDong_Lon");
        }
    }
}
