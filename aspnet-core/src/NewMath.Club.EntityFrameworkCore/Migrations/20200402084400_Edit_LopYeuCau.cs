﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewMath.Club.Migrations
{
    public partial class Edit_LopYeuCau : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GiaoVienId",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.DropColumn(
                name: "HocSinhId",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.DropColumn(
                name: "PhuHuynhId",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.AlterColumn<int>(
                name: "MonHocId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "DanhGiaNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DanhSachGiaoVienId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GioiTinhNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TenNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DanhGiaNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.DropColumn(
                name: "DanhSachGiaoVienId",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.DropColumn(
                name: "GioiTinhNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.DropColumn(
                name: "TenNguoiYeuCau",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "DbLop_YeuCau_ChuaHoatDong");

            migrationBuilder.AlterColumn<int>(
                name: "MonHocId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GiaoVienId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HocSinhId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PhuHuynhId",
                table: "DbLop_YeuCau_ChuaHoatDong",
                type: "int",
                nullable: true);
        }
    }
}
